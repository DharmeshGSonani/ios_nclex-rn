//
//  SwipeCardView.swift
//  TinderStack
//
//  Created by Osama Naeem on 16/03/2019.
//  Copyright © 2019 NexThings. All rights reserved.
//

import UIKit

class SwipeCardView : UIView {
   
    //MARK: - Properties
    var swipeView : UIView!
    var shadowView : UIView!
    var imageView: UIImageView!
  
    var label = UILabel()
    var label1 = UILabel()
    var moreButton = UIButton()
    
    var delegate : SwipeCardsDelegate?

    var divisor : CGFloat = 0
    let baseView = UIView()
    
    var dataSource : CardsDataModel? {
        didSet {
            //swipeView.backgroundColor = dataSource?.bgColor
            label.text = dataSource?.text
            guard let image = dataSource?.image else { return }
            imageView.image = UIImage(named: image)
        }
    }
    
    //MARK: - Init
     override init(frame: CGRect) {
        super.init(frame: .zero)
        configureShadowView()
        configureSwipeView()
        //configureLabelView()
        //configureImageView()
        configureButton()
        addPanGestureOnCards()
        configureTapGesture()
        attributedString()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    //MARK: - Configuration
    
    func attributedString()
    {
        let attributedString1 = NSMutableAttributedString(string: "01", attributes: [
            NSAttributedString.Key.font:UIFont(name: "Montserrat-SemiBold", size: 18.0)!,
            .foregroundColor: UIColor.init(red: 238/255, green: 58/255, blue: 58/255, alpha: 1)
            //            .kern: -0.29
        ])
        
        let attributedString2 = NSMutableAttributedString(string: " /68", attributes: [
            NSAttributedString.Key.font:UIFont(name: "Montserrat-SemiBold", size: 12.0)!,
            .foregroundColor: UIColor.init(red: 17/255, green: 50/255, blue: 71/255, alpha: 1)
            //.kern: -0.29
        ])
        
        let priceNSMutableAttributedString = NSMutableAttributedString()
        priceNSMutableAttributedString.append(attributedString1)
        priceNSMutableAttributedString.append(attributedString2)
        
        self.label1.attributedText = priceNSMutableAttributedString
    }
    
    func configureShadowView() {
        shadowView = UIView()
        shadowView.backgroundColor = .clear
        shadowView.layer.shadowColor = UIColor.black.cgColor
        shadowView.layer.shadowOffset = CGSize(width: 0, height: 0)
        shadowView.layer.shadowOpacity = 0.8
        shadowView.layer.shadowRadius = 4.0
        addSubview(shadowView)
        
        shadowView.translatesAutoresizingMaskIntoConstraints = false
        shadowView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        shadowView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        shadowView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        shadowView.topAnchor.constraint(equalTo: topAnchor).isActive = true
    }
    
    func configureSwipeView() {
        
        swipeView = UIView()
        swipeView.layer.cornerRadius = 15
        swipeView.clipsToBounds = true
        swipeView.backgroundColor = UIColor.white
        
        imageView = UIImageView()
        self.swipeView.addSubview(imageView)

        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false

        label.textColor = UIColor(red: 17/255, green: 50/255, blue: 71/255, alpha: 1.0)
        label.textAlignment = .left
        label.font = UIFont(name: "SFProText-Heavy", size: 14)
        
        self.shadowView.addSubview(swipeView)
        self.swipeView.addSubview(label)
        self.swipeView.addSubview(label1)
        
        swipeView.translatesAutoresizingMaskIntoConstraints = false
        swipeView.leftAnchor.constraint(equalTo: shadowView.leftAnchor).isActive = true
        swipeView.rightAnchor.constraint(equalTo: shadowView.rightAnchor).isActive = true
        swipeView.bottomAnchor.constraint(equalTo: shadowView.bottomAnchor).isActive = true
        swipeView.topAnchor.constraint(equalTo: shadowView.topAnchor).isActive = true
        
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        label.leftAnchor.constraint(equalTo: swipeView.leftAnchor, constant: 20).isActive = true
        label.rightAnchor.constraint(equalTo: swipeView.rightAnchor).isActive = true
        label.topAnchor.constraint(equalTo: swipeView.topAnchor, constant: 30).isActive = true
        label.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        label1.translatesAutoresizingMaskIntoConstraints = false
        label1.rightAnchor.constraint(equalTo: swipeView.rightAnchor).isActive = true
        label1.topAnchor.constraint(equalTo: swipeView.topAnchor, constant: 0).isActive = true
        label1.widthAnchor.constraint(equalToConstant: 50).isActive = true
        label1.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        imageView.centerXAnchor.constraint(equalTo: swipeView.centerXAnchor).isActive = true
        imageView.centerYAnchor.constraint(equalTo: swipeView.centerYAnchor, constant: -30).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: 400).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 400).isActive = true
    }
    
//    func configureLabelView() {
//        swipeView.addSubview(label)
//        label.backgroundColor = .white
//        label.textColor = .black
//        label.textAlignment = .center
//        label.font = UIFont.systemFont(ofSize: 18)
//        label.translatesAutoresizingMaskIntoConstraints = false
//        label.leftAnchor.constraint(equalTo: swipeView.leftAnchor).isActive = true
//        label.rightAnchor.constraint(equalTo: swipeView.rightAnchor).isActive = true
//        label.bottomAnchor.constraint(equalTo: swipeView.bottomAnchor).isActive = true
//        label.heightAnchor.constraint(equalToConstant: 85).isActive = true
//    }
    
//    func configureImageView() {
//        imageView = UIImageView()
//        swipeView.addSubview(imageView)
//        imageView.contentMode = .scaleAspectFill
//        imageView.translatesAutoresizingMaskIntoConstraints = false
//
//        imageView.centerXAnchor.constraint(equalTo: swipeView.centerXAnchor).isActive = true
//        imageView.centerYAnchor.constraint(equalTo: swipeView.centerYAnchor, constant: -30).isActive = true
//        imageView.widthAnchor.constraint(equalToConstant: 300).isActive = true
//        imageView.heightAnchor.constraint(equalToConstant: 300).isActive = true
//
//    }
    
    func configureButton() {
    
        moreButton.translatesAutoresizingMaskIntoConstraints = false
//        let image = UIImage(named: "ic_plus")?.withRenderingMode(.alwaysTemplate)
//        moreButton.setImage(image, for: .normal)
//        moreButton.tintColor = UIColor.red

        let moreButton   = UIButton(type: UIButton.ButtonType.system) as UIButton
        moreButton.frame = CGRect(x: 35, y: 300, width: 200, height: 50)
        moreButton.setTitleColor(.red, for: .normal)
        moreButton.setTitle("Continue to answer", for: .normal)
        moreButton.addTarget(self, action: #selector(buttonAction(sender:)), for: .touchUpInside)
        self.label.addSubview(moreButton)
        
//        moreButton.rightAnchor.constraint(equalTo: label.rightAnchor, constant: 15).isActive = true
//        moreButton.centerXAnchor.constraint(equalTo: label.centerXAnchor).isActive = true
//        moreButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
//        moreButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }

    func configureTapGesture() {
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTapGesture)))
    }
    
    func addPanGestureOnCards() {
        self.isUserInteractionEnabled = true
        addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture)))
    }
    
    @objc func buttonAction(sender: UIButton!) {
      print("Button tapped")
    }
    
    //MARK: - Handlers
    @objc func handlePanGesture(sender: UIPanGestureRecognizer){
        let card = sender.view as! SwipeCardView
        let point = sender.translation(in: self)
        let centerOfParentContainer = CGPoint(x: self.frame.width / 2, y: self.frame.height / 2)
        card.center = CGPoint(x: centerOfParentContainer.x + point.x, y: centerOfParentContainer.y + point.y)
        
        let distanceFromCenter = ((UIScreen.main.bounds.width / 2) - card.center.x)
        divisor = ((UIScreen.main.bounds.width / 2) / 0.61)
       
        switch sender.state {
        case .ended:
            if (card.center.x) > 400 {
                delegate?.swipeDidEnd(on: card)
                UIView.animate(withDuration: 0.2) {
                    card.center = CGPoint(x: centerOfParentContainer.x + point.x + 200, y: centerOfParentContainer.y + point.y + 75)
                    card.alpha = 0
                    self.layoutIfNeeded()
                }
                return
            }else if card.center.x < -65 {
                delegate?.swipeDidEnd(on: card)
                UIView.animate(withDuration: 0.2) {
                    card.center = CGPoint(x: centerOfParentContainer.x + point.x - 200, y: centerOfParentContainer.y + point.y + 75)
                    card.alpha = 0
                    self.layoutIfNeeded()
                }
                return
            }
            UIView.animate(withDuration: 0.2) {
                card.transform = .identity
                card.center = CGPoint(x: self.frame.width / 2, y: self.frame.height / 2)
                self.layoutIfNeeded()
            }
        case .changed:
            let rotation = tan(point.x / (self.frame.width * 2.0))
            card.transform = CGAffineTransform(rotationAngle: rotation)
            
        default:
            break
        }
    }
    
    @objc func handleTapGesture(sender: UITapGestureRecognizer){
    }

  
}
