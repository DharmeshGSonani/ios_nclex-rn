//
//  DBHandler.m
//  BioZiInic
//
//  Created by iweenggs on 12/17/15.
//  Copyright © 2015 iweenggs. All rights reserved.

//
#import "DBHandler.h"
#import <sqlite3.h>

@implementation DBHandler

+(NSString *) getDatabasePath:(NSString *)dbName
{
    // Get the path to the documents directory and append the databaseName
    
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [documentPaths objectAtIndex:0];
    
   // NSLog(@"%@",documentsDir);
    
    return [documentsDir stringByAppendingPathComponent:dbName];
}

+(void)checkAndCreateDB:(NSString *)dbName dbPath:(NSString *)dbPath
{
    BOOL success;
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    success = [fileManager fileExistsAtPath:dbPath];
    
    if(success) return;
    
    NSString *databasePathFromApp = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:dbName];
    
    [fileManager copyItemAtPath:databasePathFromApp toPath:dbPath error:nil];
}



+(NSMutableArray *) getDataFromTable :(NSString *)query
{
    NSMutableArray *mainArray = [[NSMutableArray alloc] init];
    sqlite3 *database;
    
    NSString *path = [self getDatabasePath:@"rn NEW.db"];
    [self checkAndCreateDB:@"rn NEW.db" dbPath:path];
    
    if (sqlite3_open([path UTF8String], &database) == SQLITE_OK) {
        // delete all records
        sqlite3_stmt *statement;
        NSString *temp_sql = query;
        NSLog(@"%@", temp_sql);
        const char *sql = [temp_sql UTF8String] ;
        if (sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
            
            int columnCounter=sqlite3_column_count(statement);
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                NSMutableDictionary *subDict  = [[NSMutableDictionary alloc]init];
               
                for(int i=0;i<columnCounter;i++)
                {
                    NSString * test =[self NullsafeValue:(char *) sqlite3_column_text(statement,i)];
                    
                    const char *_columnName=sqlite3_column_name(statement, i);
                    NSString *columnName=[[NSString alloc] initWithUTF8String:_columnName];
                    
                    if(i != columnCounter )
                    {
                        [subDict setValue:test forKey:columnName];
                    }
                }
                
                  [mainArray addObject:subDict];
            }
            
            sqlite3_finalize(statement);
        }
        sqlite3_close(database);
    }
    return mainArray;
}
//catid:(NSString*)catid

+(BOOL)addReadQue:(NSString*)topicid totalPages:(NSInteger)totalPages viewCount:(NSInteger)viewCount partid:(NSString*)partid
{
    NSString * insertQuery = @"";
    NSString * tempQuery = @"INSERT INTO read_question (topic_id,total_pages,viewcount,part_id) VALUES";
    insertQuery = [tempQuery stringByAppendingFormat:@"(\"%@\",\"%ld\",\"%ld\",\"%@\")",topicid,totalPages,viewCount,partid];
    
    return  [DBHandler genericQuery:insertQuery];
}

//+(BOOL)addPartQue:(NSString*)topicid partid:(NSString*)partid queid:(NSString*)queid question:(NSString*)question readcount:(NSString*)readcount
//{
//    NSString * insertQuery = @"";
//    NSString * tempQuery = @"INSERT INTO part_question (topic_id,part_id,que_id,question,read_count) VALUES";
//    insertQuery = [tempQuery stringByAppendingFormat:@"(\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")",topicid,partid,queid,question,readcount];
//    
//    return  [DBHandler genericQuery:insertQuery];
//}

//+(BOOL)addChallanges:(NSString*)strId result:(NSString*)result time:(NSString*)time title:(NSString *)title
// {
//     NSString * insertQuery = @"";
//     NSString * tempQuery = @"INSERT INTO challenges_que (id,result,lastTime,title) VALUES";
//     insertQuery = [tempQuery stringByAppendingFormat:@"(\"%@\",\"%@\",\"%@\",\"%@\")",strId,result,time,title];
//     
//     return  [DBHandler genericQuery:insertQuery];
//}


//+(BOOL)addProduct:(NSString *)productName desc:(NSString *)desc company:(NSString *)company type:(NSString *)type mrp:(NSString *)mrp rate:(NSString *)rate key:(NSString *)key packing:(NSString *)packing lut:(NSString*)lut scheme:(NSString *)scheme
//{
//
//    NSString * insertQuery = @"";
//
//    NSString *tempQuery = @"INSERT INTO Products (name,description,company,type,mrp,rate,key,packing,lut,scheme) VALUES ";
//    insertQuery = [tempQuery stringByAppendingFormat:@"(\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%ld\",\"%@\")",productName,desc,company,type,mrp,rate,key,packing,lut.integerValue,scheme];
//
//    return [DBHandler genericQuery:insertQuery];
//}
//
//+(BOOL)addTopProduct:(NSString *)sortOrder CarouselID:(NSString *)CarouselID CarouselName:(NSString *)CarouselName codeKey:(NSString *)codeKey CreatedAt:(NSString *)CreatedAt ID:(NSString *)ID ImageURL:(NSString *)ImageURL IsDeleted:(NSString *)IsDeleted ItemName:(NSString *)ItemName ProductsortOrder:(NSString *)ProductsortOrder isDeleted:(NSString *)isDeleted
//{
//    NSString * insertQuery = @"";
//
//    NSString *tempQuery = @"INSERT INTO TopProducts (CarouselSortOrder,CarouselID,CarouselName,CodeKey,CreatedAt,ID,ImageURL,ItemName,ProductSortOrder,IsDeleted) VALUES ";
//    insertQuery = [tempQuery stringByAppendingFormat:@"(\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")",sortOrder,CarouselID,CarouselName,codeKey,CreatedAt,ID,ImageURL,ItemName,ProductsortOrder,isDeleted];
//
//    return [DBHandler genericQuery:insertQuery];
//}
//
//+(BOOL)addTopBanners:(NSString *)sortOrder CarouselID:(NSString *)CarouselID CarouselName:(NSString *)CarouselName CreatedAt:(NSString *)CreatedAt ID:(NSString *)ID ImageURL:(NSString *)ImageURL IsDeleted:(NSString *)IsDeleted ItemName:(NSString *)ItemName ProductsortOrder:(NSString *)ProductsortOrder isDeleted:(NSString *)isDeleted
//{
//    NSString * insertQuery = @"";
//
//    NSString *tempQuery = @"INSERT INTO TopBanners (CarouselSortOrder,CarouselID,CarouselName,CreatedAt,ID,ImageURL,ItemName,ProductSortOrder,IsDeleted) VALUES ";
//    insertQuery = [tempQuery stringByAppendingFormat:@"(\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")",sortOrder,CarouselID,CarouselName,CreatedAt,ID,ImageURL,ItemName,ProductsortOrder,isDeleted];
//
//    return [DBHandler genericQuery:insertQuery];
//}
//
//+(BOOL)addCompanies:(NSString *)company selected:(NSInteger)selected
//{
//    NSString * insertQuery = @"";
//
//    NSString *tempQuery = @"INSERT INTO companies (company,selected) VALUES ";
//    insertQuery = [tempQuery stringByAppendingFormat:@"(\"%@\",\"%ld\")",company,(long)selected];
//
//    return [DBHandler genericQuery:insertQuery];
//}
//
//+(BOOL)addType:(NSString *)type selected:(NSInteger)selected
//{
//    NSString * insertQuery = @"";
//
//    NSString *tempQuery = @"INSERT INTO types (type,selected) VALUES ";
//    insertQuery = [tempQuery stringByAppendingFormat:@"(\"%@\",\"%ld\")",type,(long)selected];
//
//    return [DBHandler genericQuery:insertQuery];
//}
//
//+(BOOL)addOrder:(NSString *)key quantity:(NSString *)quantity orderCases:(NSString *)orderCases remarks:(NSString *)remarks orderStatus:(NSInteger)status lut:(NSInteger)lut
//{
//    NSString * insertQuery = @"";
//
//    NSString *tempQuery = @"INSERT INTO orders (key,units,cases,remarks,status,lut) VALUES ";
//    insertQuery = [tempQuery stringByAppendingFormat:@"(\"%@\",\"%@\",\"%@\",\"%@\",\"%ld\",\"%ld\")",key,quantity,orderCases,remarks,(long)status,lut];
//
//    return [DBHandler genericQuery:insertQuery];
//}

+(BOOL)addQuickTest:(NSString*)que_id is_wrong:(NSString*)is_wrong correct_option:(NSString*)correct_option wrong_option:(NSString*)wrong_option
{
    NSString * insertQuery = @"";
    NSString * tempQuery = @"INSERT INTO quick_test (que_id,is_wrong,correct_option,wrong_option) VALUES";
    insertQuery = [tempQuery stringByAppendingFormat:@"(\"%@\",\"%@\",\"%@\",\"%@\")",que_id,is_wrong,correct_option,wrong_option];
    
    return  [DBHandler genericQuery:insertQuery];
}

+(BOOL)addWrongQue:(NSString*)que_id is_wrong:(NSString*)is_wrong correct_option:(NSString*)correct_option wrong_option:(NSString*)wrong_option
{
    NSString * insertQuery = @"";
    NSString * tempQuery = @"INSERT INTO wrong_test (que_id,is_wrong,correct_option,wrong_option) VALUES";
    insertQuery = [tempQuery stringByAppendingFormat:@"(\"%@\",\"%@\",\"%@\",\"%@\")",que_id,is_wrong,correct_option,wrong_option];
    
    return  [DBHandler genericQuery:insertQuery];
}

+(BOOL)addCustomized:(NSString*)que_id is_wrong:(NSString*)is_wrong correct_option:(NSString*)correct_option wrong_option:(NSString*)wrong_option
{
    NSString * insertQuery = @"";
    NSString * tempQuery = @"INSERT INTO customized_test (que_id,is_wrong,correct_option,wrong_option) VALUES";
    insertQuery = [tempQuery stringByAppendingFormat:@"(\"%@\",\"%@\",\"%@\",\"%@\")",que_id,is_wrong,correct_option,wrong_option];
    
    return  [DBHandler genericQuery:insertQuery];
}

+(BOOL)addDaily:(NSString*)que_id is_wrong:(NSString*)is_wrong correct_option:(NSString*)correct_option wrong_option:(NSString*)wrong_option
{
    NSString * insertQuery = @"";
    NSString * tempQuery = @"INSERT INTO daily_test (que_id,is_wrong,correct_option,wrong_option) VALUES";
    insertQuery = [tempQuery stringByAppendingFormat:@"(\"%@\",\"%@\",\"%@\",\"%@\")",que_id,is_wrong,correct_option,wrong_option];
    
    return  [DBHandler genericQuery:insertQuery];
}

+(BOOL)addDailyCheckin:(NSString*)que_id is_wrong:(NSString*)is_wrong correct_option:(NSString*)correct_option wrong_option:(NSString*)wrong_option
{
    NSString * insertQuery = @"";
    NSString * tempQuery = @"INSERT INTO daily_checkin (que_id,is_wrong,correct_option,wrong_option) VALUES";
    insertQuery = [tempQuery stringByAppendingFormat:@"(\"%@\",\"%@\",\"%@\",\"%@\")",que_id,is_wrong,correct_option,wrong_option];
    
    return  [DBHandler genericQuery:insertQuery];
}



+(BOOL)deleteData:(NSString *)strQuery
{
   return  [DBHandler genericQuery:strQuery];
}

+(BOOL) genericQuery:(NSString *)temp_sql{
   
    NSString *path = [self getDatabasePath:@"rn NEW.db"];
    [self checkAndCreateDB:@"rn NEW.db" dbPath:path];
    sqlite3 *database;
    
    if (sqlite3_open([path UTF8String], &database) == SQLITE_OK) {
        
        //NSLog(@"%@",temp_sql);
        const char *sql = [temp_sql UTF8String];
        char *err;
        if ( sqlite3_exec(database,sql,NULL,NULL,&err)!=SQLITE_OK) {
            NSLog(@"ERROR: %@ - %s", temp_sql, err);
            sqlite3_close(database);
            return FALSE;
        }else {
            //NSLog(@"%@ - performed OK.", temp_sql);
            sqlite3_close(database);
            return TRUE;
            
        }
    }
    
    return FALSE;
}

+(NSString *)NullsafeValue:(char *)value{
    if (value == nil) {
        return @"";
    }
    return [NSString stringWithUTF8String:value];
}

-(id)init {
    
    //alert view
    self = [super init];
    
    
    return self;
}

@end
