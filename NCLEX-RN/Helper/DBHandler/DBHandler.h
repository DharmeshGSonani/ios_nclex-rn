//
//  DBHandler.h
//  BioZiInic
//
//  Created by iweenggs on 12/17/15.
//  Copyright © 2015 iweenggs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DBHandler : NSObject{
    BOOL isChecked;
    NSString *databasePath;

}

+(NSString *)NullsafeValue:(char *)value;

+(BOOL) genericQuery:(NSString *)temp_sql;

+(NSMutableArray *) getDataFromTable:(NSString *)tableNameWithAnyCondition user_id:(NSString *)userId;

//+(BOOL)addProduct:(NSString *)productName desc:(NSString *)desc company:(NSString *)company type:(NSString *)type mrp:(NSString *)mrp rate:(NSString *)rate key:(NSString *)key packing:(NSString *)packing lut:(NSString*)lut scheme:(NSString *)scheme;
//
//+(BOOL)addTopProduct:(NSString *)sortOrder CarouselID:(NSString *)CarouselID CarouselName:(NSString *)CarouselName codeKey:(NSString *)codeKey CreatedAt:(NSString *)CreatedAt ID:(NSString *)ID ImageURL:(NSString *)ImageURL IsDeleted:(NSString *)IsDeleted ItemName:(NSString*)ItemName ProductsortOrder:(NSString *)ProductsortOrder isDeleted:(NSString *)isDeleted;
//
//+(BOOL)addTopBanners:(NSString *)sortOrder CarouselID:(NSString *)CarouselID CarouselName:(NSString *)CarouselName CreatedAt:(NSString *)CreatedAt ID:(NSString *)ID ImageURL:(NSString *)ImageURL IsDeleted:(NSString *)IsDeleted ItemName:(NSString*)ItemName ProductsortOrder:(NSString *)ProductsortOrder isDeleted:(NSString *)isDeleted;
//
//+(BOOL)addCompanies:(NSString *)company selected:(NSInteger)selected;
//
//+(BOOL)addType:(NSString *)type selected:(NSInteger)selected;
//
//+(BOOL)addOrder:(NSString *)key quantity:(NSString *)quantity orderCases:(NSString *)orderCases remarks:(NSString *)remarks orderStatus:(NSInteger)status lut:(NSInteger)lut;

+(NSMutableArray *) getDataFromTable:(NSString *)tableNameWithAnyCondition product_id:(NSString *)product_id;

+(NSMutableArray *) getDataFromTable:(NSString *)query;

+(BOOL)addReadQue:(NSString*)topicid totalPages:(NSInteger)totalPages viewCount:(NSInteger)viewCount partid:(NSString*)partid;

+(BOOL)addQuickTest:(NSString*)que_id is_wrong:(NSString*)is_wrong correct_option:(NSString*)correct_option wrong_option:(NSString*)wrong_option;

+(BOOL)addWrongQue:(NSString*)que_id is_wrong:(NSString*)is_wrong correct_option:(NSString*)correct_option wrong_option:(NSString*)wrong_option;

+(BOOL)addCustomized:(NSString*)que_id is_wrong:(NSString*)is_wrong correct_option:(NSString*)correct_option wrong_option:(NSString*)wrong_option;

+(BOOL)addDaily:(NSString*)que_id is_wrong:(NSString*)is_wrong correct_option:(NSString*)correct_option wrong_option:(NSString*)wrong_option;

+(BOOL)addDailyCheckin:(NSString*)que_id is_wrong:(NSString*)is_wrong correct_option:(NSString*)correct_option wrong_option:(NSString*)wrong_option;

//+(BOOL)addPartQue:(NSString*)topicid partid:(NSString*)partid queid:(NSString*)queid question:(NSString*)question readcount:(NSString*)readcount;

//+(BOOL)addChallanges:(NSString*)strId result:(NSString*)result time:(NSString*)time title:(NSString *)title;

+(BOOL)deleteData:(NSString *)strQuery;

@end
