//
//  AppDelegate.swift
//  NCLEX-RN
//
//  Created by Kevin M1 on 04/03/22.
//
//https://app.mockplus.cn/app/Wdom0YGRsYa/design

import UIKit
import StoreKit
import Adjust
import AppTrackingTransparency
import SVProgressHUD
import AdSupport
import AMTabView
import Firebase
import FirebaseCore
import FirebaseStorage

extension Notification.Name {
    public static let fillData = Notification.Name(rawValue: "fillData")
}

@main
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate ,AdjustDelegate {

    var window: UIWindow?

    var totalTimeAppOpenCount : Int = 0
    var totalQuestionShow : Int = 0
    
    var isOpenCheckInPopUP : Bool = false
    var isFromCheckIn : Bool = false
    var isComeFirstTime : Bool = false
    
    var time : Int = 0
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        InAppPurchaseManager.sharedManager.start(withHandler: DemoTransactionHandler())
        
        for family in UIFont.familyNames {
            print("\(family)")

            for name in UIFont.fontNames(forFamilyName: family) {
                print("\(name)")
            }
        }
        setupNotification()
        requestTrackingPermission()
        
        installationDate()
        
        let yourAppToken = "j46vtoplophc"
        let environment = ADJEnvironmentProduction
        let adjustConfig = ADJConfig(
            appToken: yourAppToken,
            environment: environment)
        
        adjustConfig!.delegate = self
        Adjust.appDidLaunch(adjustConfig)
        
        AMTabView.settings.ballColor = UIColor.init(red: 88/255, green: 92/255, blue: 229/255, alpha: 1.0)
        AMTabView.settings.tabColor = UIColor.init(red: 236/255, green: 242/255, blue: 255/255, alpha: 1.0)
        AMTabView.settings.selectedTabTintColor = .white
        AMTabView.settings.unSelectedTabTintColor = UIColor.init(red: 128/255, green: 135/255, blue: 168/255, alpha: 1.0)

        // Chnage the animation duration
        AMTabView.settings.animationDuration = 1.0

        FirebaseApp.configure()
        getStorage()
        return true
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        
        requestTrackingPermission()
        
    }
    
    func adjustAttributionChanged(_ attribution: ADJAttribution?) {
        
    }
    
    func getStorage(){
        
        // Create local filesystem URL
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let localURL = documentsURL.appendingPathComponent("rn NEW.db")
        
        if !FileManager.default.fileExists(atPath: localURL.path)
        {
            let storage = Storage.storage()
            let storageRef = storage.reference()
            let pathReference = storage.reference(withPath: "nclex-rn-88528.appspot.com/rn NEW.db")
            let gsReference = storage.reference(forURL: "gs://nclex-rn-88528.appspot.com/rn NEW.db")
            //let httpsReference = storage.reference(forURL: "https://firebasestorage.googleapis.com/b/bucket/o/emt-3-in-mk.appspot.com/EMT.zip")
            
            let islandRef = storageRef.child("rn NEW.db")
            
            // Download to the local filesystem
            
            let downloadTask = islandRef.write(toFile: localURL) { url, error in
                if let error = error {
                    // Uh-oh, an error occurred!
                } else {
                    // Local file URL for "images/island.jpg" is returned
                    print(url)
                    NotificationCenter.default.post(name: Notification.Name.fillData, object: "", userInfo: [:])
                }
            }
        }
    }
    
    func setupNotification()
    {
        UNUserNotificationCenter.current().delegate = self
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { (granted, error) in
            if granted {
                print("User gave permissions for local notifications")
                DispatchQueue.main.async {
                    UIApplication.shared.registerForRemoteNotifications()
                    
                }
            }
        }
    }
    
    func requestTrackingPermission() {
        if #available(iOS 14, *) {
            // ATTrackingManager.requestTrackingAuthorization { status in
            ATTrackingManager.requestTrackingAuthorization(completionHandler: { status in
                
                switch status {
                case .authorized:
                    // Tracking authorization dialog was shown
                    // and we are authorized
                    print("Authorized")
                    
                    // Now that we are authorized we can get the IDFA
                    print(ASIdentifierManager.shared().advertisingIdentifier)
                    
                case .denied:
                    // Tracking authorization dialog was
                    // shown and permission is denied
                    print("Denied")
                    
                case .notDetermined:
                    // Tracking authorization dialog has not been shown
                    print("Not Determined")
                case .restricted:
                    print("Restricted")
                @unknown default:
                    print("Unknown")
                }
            }
        )}
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        SVProgressHUD.dismiss()
        completionHandler([.alert, .badge, .sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("Usrinfo associated with notification == \(response.notification.request.content.userInfo)")
        SVProgressHUD.dismiss()
        completionHandler()
    }
    
    //MARK: - Installation Get Date  Method
    
    func installationDate()
    {
        let prefs = UserDefaults.standard
        
        if prefs.object(forKey: "installation_date") == nil {
           
            totalTimeAppOpenCount = 0
            
            let todayDate = Date()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            dateFormatter.timeZone = TimeZone.current
            
            let getDate = dateFormatter.string(from: todayDate)
            print("todayDate:-", getDate)
            
            let currentDate = dateFormatter.date(from: getDate)
            
            prefs.set(currentDate, forKey: "installation_date")
            UserDefaults.standard.set(0, forKey: "AppOpenCount")
            
            UserDefaults.standard.removeObject(forKey: "daysShow")
            UserDefaults.standard.removeObject(forKey: "showProgress")
            UserDefaults.standard.removeObject(forKey: "showQuestion")
            UserDefaults.standard.synchronize()
                        
            isOpenCheckInPopUP = true
            isComeFirstTime = true
            
        }
        else
        {
            let todayDate = Date()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            dateFormatter.timeZone = TimeZone.current
            let getCurrentDate = dateFormatter.string(from: todayDate)
            
            let currentDate = dateFormatter.date(from: getCurrentDate)
            let getInstallationDate = prefs.object(forKey: "installation_date") as! Date
            
            print("getInstallationDate:-",getInstallationDate)
            print("todayDate:-", todayDate)
            
            let count = UserDefaults.standard.object(forKey: "AppOpenCount") as! Int
            totalTimeAppOpenCount = count 
            
            UserDefaults.standard.synchronize()
            
            if currentDate! > getInstallationDate
            {
                self.isOpenCheckInPopUP = true
                self.isComeFirstTime = true
                totalTimeAppOpenCount = totalTimeAppOpenCount + 0
                UserDefaults.standard.set(totalTimeAppOpenCount, forKey: "AppOpenCount")
                prefs.set(todayDate, forKey: "installation_date")
            } else {
                isOpenCheckInPopUP = false
                isComeFirstTime = false
            }
        }
    }
}

