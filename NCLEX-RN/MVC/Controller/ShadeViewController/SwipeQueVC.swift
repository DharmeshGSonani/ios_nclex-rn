//
//  SwipeQueVC.swift
//  CanadianCityzenShip
//
//  Created by Rutvik Moradiya on 28/12/21.
//

import UIKit
import AMTabView

class SwipeQueVC: UIViewController, TabItem {
    
    var tabImage: UIImage?{
        return UIImage(named: "ic_second")
    }
    
    var cards = [ImageCard]()
    var arrQuestions = NSMutableArray()
    var arrAnswer = NSArray()
    var currentIndex : Int = 0
    var arrOptions = NSMutableArray()
    
    var dicSelectedCategory = NSDictionary()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        arrQuestions = DBHandler.getDataFromTable("SELECT * FROM question ORDER BY random() ")
        print(arrQuestions.count)
        
        arrAnswer = DBHandler.getDataFromTable("SELECT answers FROM question")
        print(arrAnswer.count)
        
        for index in 0..<arrQuestions.count {
            
            let dicQuestion = arrQuestions.object(at: index) as! NSDictionary
            let strQuestionTitle = dicQuestion.value(forKey: "question") as! String
            let strExaplanation = dicQuestion.value(forKey: "explanation") as! String
            let strTopicName = dicQuestion.value(forKey: "topicName") as! String
            
            let card = ImageCard(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 60, height: self.view.frame.height * 0.7))
            card.btnNext.tag = index
            card.txtQueView.text = strQuestionTitle
            card.lblSubject.text = strTopicName
            card.btnNext.addTarget(self, action: #selector(self.buttonClicked(sender:)), for: .touchUpInside)
            card.btnNext.isHidden = false
            card.explanationView.isHidden = true
            card.txtExplainView.isHidden = true
       
            let strAnswer = dicQuestion.value(forKey: "answers") as! String
            let strFinalAnswer = strAnswer.replacingOccurrences(of: "[\"", with: "").replacingOccurrences(of: "\"]", with: "")
            
            let lbl = card.answerView.viewWithTag(10) as! UILabel
            lbl.isHidden = true
            lbl.text = strFinalAnswer
            
            card.txtExplainView.text = strExaplanation
            cards.append(card)
        }
        
        dynamicAnimator = UIDynamicAnimator(referenceView: self.view)
        
        //        setUpDummyUI()
        
        layoutCards()
        
        // Do any additional setup after loading the view.
    }
    
    /// Scale and alpha of successive cards visible to the user
    let cardAttributes: [(downscale: CGFloat, alpha: CGFloat)] = [(1, 1), (0.92, 0.8), (0.84, 0.6), (0.76, 0.4)]
    let cardInteritemSpacing: CGFloat = 15
    
    /// Set up the frames, alphas, and transforms of the first 4 cards on the screen
    func layoutCards() {
        // frontmost card (first card of the deck)
        let firstCard = cards[0]
        self.view.addSubview(firstCard)
        firstCard.layer.zPosition = CGFloat(cards.count)
        firstCard.center = self.view.center
        firstCard.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(handleCardPan)))
        
        
        // the next 3 cards in the deck
        for i in 1...3 {
            if i > (cards.count - 1)
            { continue }
            
            let card = cards[i]
            
            card.layer.zPosition = CGFloat(cards.count - i)
            
            // here we're just getting some hand-picked vales from cardAttributes (an array of tuples)
            // which will tell us the attributes of each card in the 4 cards visible to the user
            let downscale = cardAttributes[i].downscale
            let alpha = cardAttributes[i].alpha
            card.transform = CGAffineTransform(scaleX: downscale, y: downscale)
            card.alpha = alpha
            
            // position each card so there's a set space (cardInteritemSpacing) between each card, to give it a fanned out look
            card.center.x = self.view.center.x
            card.frame.origin.y = cards[0].frame.origin.y - (CGFloat(i) * cardInteritemSpacing)
            // workaround: scale causes heights to skew so compensate for it with some tweaking
            if i == 3 {
                card.frame.origin.y += 1.5
            }
            self.view.addSubview(card)
        }
        
        // make sure that the first card in the deck is at the front
        self.view.bringSubviewToFront(cards[0])
    }
    
    /// This is called whenever the front card is swiped off the screen or is animating away from its initial position.
    /// showNextCard() just adds the next card to the 4 visible cards and animates each card to move forward.
    func showNextCard() {
        let animationDuration: TimeInterval = 0.2
        // 1. animate each card to move forward one by one
        for i in 1...3 {
            if i > (cards.count - 1) { continue }
            let card = cards[i]
            let newDownscale = cardAttributes[i - 1].downscale
            let newAlpha = cardAttributes[i - 1].alpha
            UIView.animate(withDuration: animationDuration, delay: (TimeInterval(i - 1) * (animationDuration / 2)), usingSpringWithDamping: 0.8, initialSpringVelocity: 0.0, options: [], animations: {
                card.transform = CGAffineTransform(scaleX: newDownscale, y: newDownscale)
                card.alpha = newAlpha
                if i == 1 {
                    card.center = self.view.center
                } else {
                    card.center.x = self.view.center.x
                    card.frame.origin.y = self.cards[1].frame.origin.y - (CGFloat(i - 1) * self.cardInteritemSpacing)
                }
            }, completion: { (_) in
                if i == 1 {
                    card.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(self.handleCardPan)))
                    card.btnNext.isHidden = false
                    
                     let lbl = card.answerView.viewWithTag(10) as! UILabel
                    lbl.isHidden = true
                    
                }
            })
        }
        
        // 2. add a new card (now the 4th card in the deck) to the very back
        if 4 > (cards.count - 1) {
            if cards.count != 1 {
                self.view.bringSubviewToFront(cards[1])
            }
            return
        }
        
        let newCard = cards[4]
        newCard.layer.zPosition = CGFloat(cards.count - 4)
        let downscale = cardAttributes[3].downscale
        let alpha = cardAttributes[3].alpha
         
        // initial state of new card
        newCard.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        newCard.alpha = 0
        newCard.center.x = self.view.center.x
        newCard.frame.origin.y = cards[1].frame.origin.y - (4 * cardInteritemSpacing)
        self.view.addSubview(newCard)
        
        // animate to end state of new card
        UIView.animate(withDuration: animationDuration, delay: (3 * (animationDuration / 2)), usingSpringWithDamping: 0.8, initialSpringVelocity: 0.0, options: [], animations: {
            newCard.transform = CGAffineTransform(scaleX: downscale, y: downscale)
            newCard.alpha = alpha
            newCard.center.x = self.view.center.x
            newCard.frame.origin.y = self.cards[1].frame.origin.y - (3 * self.cardInteritemSpacing) + 1.5
        }, completion: { (_) in
            
        })
        
        // first card needs to be in the front for proper interactivity
        self.view.bringSubviewToFront(self.cards[1])
        
        //Progress Bar
        
        self.currentIndex = self.currentIndex + 1
        
        if self.currentIndex == 10{
            self.currentIndex = 0
        }
    }
    
    func removeOldFrontCard() {
        cards[0].removeFromSuperview()
        cards.remove(at: 0)
    }
    
    var dynamicAnimator: UIDynamicAnimator!
    var cardAttachmentBehavior: UIAttachmentBehavior!
    
    /// This method handles the swiping gesture on each card and shows the appropriate emoji based on the card's center.
    @objc func handleCardPan(sender: UIPanGestureRecognizer) {
        // if we're in the process of hiding a card, don't let the user interace with the cards yet
        if cardIsHiding { return }
        // change this to your discretion - it represents how far the user must pan up or down to change the option
        let optionLength: CGFloat = 60
        // distance user must pan right or left to trigger an option
        let requiredOffsetFromCenter: CGFloat = 15
        
        let panLocationInView = sender.location(in: view)
        let panLocationInCard = sender.location(in: cards[0])
        switch sender.state {
        case .began:
            dynamicAnimator.removeAllBehaviors()
            let offset = UIOffset(horizontal: panLocationInCard.x - cards[0].bounds.midX, vertical: panLocationInCard.y - cards[0].bounds.midY);
            // card is attached to center
            cardAttachmentBehavior = UIAttachmentBehavior(item: cards[0], offsetFromCenter: offset, attachedToAnchor: panLocationInView)
            dynamicAnimator.addBehavior(cardAttachmentBehavior)
        case .changed:
            cardAttachmentBehavior.anchorPoint = panLocationInView
            if cards[0].center.x > (self.view.center.x + requiredOffsetFromCenter)
            {
                if cards[0].center.y < (self.view.center.y - optionLength)
                {
                    if cards[0].center.y < (self.view.center.y - optionLength - optionLength)
                    {
                        
                    }
                    else
                    {
                        
                    }
                    
                }
                else if cards[0].center.y > (self.view.center.y + optionLength)
                {
                    
                }
                else
                {
                    
                }
            }
            else if cards[0].center.x < (self.view.center.x - requiredOffsetFromCenter)
            {
                
                if cards[0].center.y < (self.view.center.y - optionLength)
                {
                    
                }
                else if cards[0].center.y > (self.view.center.y + optionLength)
                {
                    
                }
                else
                {
                    
                }
            }
            else
            {
                
            }
            
        case .ended:
            
            dynamicAnimator.removeAllBehaviors()
            if !(cards[0].center.x > (self.view.center.x + requiredOffsetFromCenter) || cards[0].center.x < (self.view.center.x - requiredOffsetFromCenter)) {
                // snap to center
                let snapBehavior = UISnapBehavior(item: cards[0], snapTo: self.view.center)
                dynamicAnimator.addBehavior(snapBehavior)
            } else {
                
                let velocity = sender.velocity(in: self.view)
                let pushBehavior = UIPushBehavior(items: [cards[0]], mode: .instantaneous)
                pushBehavior.pushDirection = CGVector(dx: velocity.x/10, dy: velocity.y/10)
                pushBehavior.magnitude = 175
                dynamicAnimator.addBehavior(pushBehavior)
                // spin after throwing
                var angular = CGFloat.pi / 2 // angular velocity of spin
                
                let currentAngle: Double = atan2(Double(cards[0].transform.b), Double(cards[0].transform.a))
                
                if currentAngle > 0 {
                    angular = angular * 1
                } else {
                    angular = angular * -1
                }
                let itemBehavior = UIDynamicItemBehavior(items: [cards[0]])
                itemBehavior.friction = 0.2
                itemBehavior.allowsRotation = true
                itemBehavior.addAngularVelocity(CGFloat(angular), for: cards[0])
                dynamicAnimator.addBehavior(itemBehavior)
                
                showNextCard()
                
                hideFrontCard()
                
            }
        default:
            break
            
        }
    }
    
    /// This function continuously checks to see if the card's center is on the screen anymore. If it finds that the card's center is not on screen, then it triggers removeOldFrontCard() which removes the front card from the data structure and from the view.
    
    var cardIsHiding = false
    func hideFrontCard() {
        if #available(iOS 10.0, *) {
            var cardRemoveTimer: Timer? = nil
            cardRemoveTimer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true, block: { [weak self] (_) in
                guard self != nil else { return }
                if !(self!.view.bounds.contains(self!.cards[0].center)) {
                    cardRemoveTimer!.invalidate()
                    self?.cardIsHiding = true
                    UIView.animate(withDuration: 0.2, delay: 0, options: [.curveEaseIn], animations: {
                        self?.cards[0].alpha = 0.0
                    }, completion: { (_) in
                        self?.removeOldFrontCard()
                        self?.cardIsHiding = false
                    })
                }
            })
        } else {
            // fallback for earlier versions
            UIView.animate(withDuration: 0.2, delay: 1.5, options: [.curveEaseIn], animations: {
                self.cards[0].alpha = 0.0
            }, completion: { (_) in
                self.removeOldFrontCard()
            })
        }
    }
    
    
    @objc func buttonClicked(sender: UIButton){
        
        let card = self.cards[0]
        
        card.btnNext.isHidden = true
        card.txtExplainView.isHidden = false
        card.explanationView.isHidden = false
      
        let lbl = card.answerView.viewWithTag(10) as! UILabel
        lbl.isHidden = false
    
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        self.navigationController?.popViewController(animated: true)
    }
}

// MARK: - Unrelated to cards logic code

extension SwipeQueVC {
    /// Hide status bar
    override var prefersStatusBarHidden: Bool {
        get {
            return true
        }
    }
    
}


