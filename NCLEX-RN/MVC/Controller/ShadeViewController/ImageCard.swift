//
//  ImageCard.swift
//  CardSlider
//
//  Created by Saoud Rizwan on 2/27/17.
//  Copyright © 2017 Saoud Rizwan. All rights reserved.
//

import UIKit

class ImageCard: CardView {
    
    var answerView = UIView()
    var explanationView = UIView()
    let lblAns = UILabel()
    var btnNext = UIButton(type: UIButton.ButtonType.system) as UIButton
    var lblQues = UILabel()
    var txtQueView = UITextView()
    var txtExplainView = UITextView()
    let lblSubject = UILabel()
    var lblExplain = UILabel()
    var lblExplanation = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        //MARK: - blur
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
        blurEffectView.layer.cornerRadius = 10
        blurEffectView.clipsToBounds = true
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(blurEffectView)
        
        //MARK: - Lable Subject
        lblSubject.frame = CGRect(x: 20, y: 37, width: self.layer.frame.width - 40, height: 20)
        lblSubject.textColor = UIColor(red: 88/255, green: 92/255, blue: 229/255, alpha: 1.0)
        lblSubject.font = UIFont(name:"Montserrat-SemiBold",size:13)
        self.addSubview(lblSubject)
        
        
        //MARK: - Lable Question
        txtQueView.frame = CGRect(x: 24, y: lblSubject.frame.maxY + 12, width: self.layer.frame.width - 48, height: 90)
        txtQueView.showsVerticalScrollIndicator = false
        txtQueView.isEditable = false
        txtQueView.isSelectable = false
        txtQueView.bounces = false
        txtQueView.textColor = UIColor(red: 52/255, green: 57/255, blue: 101/255, alpha: 1)
        txtQueView.font = UIFont(name:"Montserrat-Bold",size:16)
        self.addSubview(txtQueView)
        
        
        //MARK: - answerView
        answerView.frame = CGRect(x: 24, y: txtQueView.frame.maxY + txtQueView.layer.frame.height + 24, width: self.frame.width - 48, height: 95)
        answerView.backgroundColor = .clear
        answerView.layer.cornerRadius = 10
        answerView.clipsToBounds = true
        self.addSubview(answerView)

        let lbl = UILabel()
        lbl.frame = CGRect.init(x: 20, y: 0, width: answerView.frame.width - 40, height: answerView.frame.height)
        lbl.numberOfLines = 0
        lbl.font = UIFont(name: "Montserrat-SemiBold", size: 12)
        lbl.textColor = UIColor(red: 8/255, green: 201/255, blue: 189/255, alpha: 1.0)
        lbl.tag = 10
        lbl.textAlignment = .center
        answerView.addSubview(lbl)
        answerView.addDashedBorder(strokeColor: UIColor.init(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.2), lineWidth: 2.0)

        
        //MARK: - ExplanationView
        explanationView.frame = CGRect(x: 24, y: answerView.frame.maxY + 20, width: self.frame.width -  48, height: 20)
        self.addSubview(explanationView)
        
        let label = UILabel()
        label.frame = CGRect.init(x: 0, y: 0, width: 100, height: explanationView.frame.height)
        label.text = "Explanation:"
        label.font = UIFont(name: "Montserrat-SemiBold", size: 12)
        label.textColor = UIColor(red: 8/255, green: 201/255, blue: 189/255, alpha: 1.0)
        explanationView.addSubview(label)
        
        txtExplainView.frame = CGRect(x: 24, y: explanationView.frame.maxY + 12, width: self.layer.frame.width - 48, height: 100)
        txtExplainView.showsVerticalScrollIndicator = false
        txtExplainView.isEditable = false
        txtExplainView.isSelectable = false
        txtExplainView.bounces = false
        txtExplainView.textColor = UIColor(red: 52/255, green: 57/255, blue: 101/255, alpha: 1)
        txtExplainView.font = UIFont(name:"Montserrat-Bold",size:12)
        self.addSubview(txtExplainView)
       

        //MARK: - Button
        btnNext.frame = CGRect(x: 50, y: txtExplainView.frame.maxY + 5, width: 250, height: 50)
        btnNext.backgroundColor = UIColor(red: 88/255, green: 92/255, blue: 229/255, alpha: 1.0)
        btnNext.setTitleColor(.white, for: .normal)
        btnNext.layer.cornerRadius = btnNext.frame.height / 2
        btnNext.setTitle("Show Answer", for: .normal)
        btnNext.titleLabel?.font =  UIFont(name: "Montserrat-Regular", size: 12)
        self.addSubview(btnNext)
        
        deviceType()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func deviceType(){
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136, 1334, 1920, 2208: // iPhone 5 or 5S or 5C
                
                lblSubject.frame = CGRect(x: 20, y: 18, width: self.layer.frame.width - 40, height: 15)
                txtQueView.frame = CGRect(x: 24, y: lblSubject.frame.maxY + 10, width: self.layer.frame.width - 48, height: 65)
                answerView.frame = CGRect(x: 24, y: txtQueView.frame.maxY + 21, width: self.frame.width - 48, height: 100)
                explanationView.frame = CGRect(x: 24, y: answerView.frame.maxY + 10, width: self.frame.width -  48, height: 17)
                txtExplainView.frame = CGRect(x: 24, y: explanationView.frame.maxY + 5, width: self.layer.frame.width - 50, height: 80)
                btnNext.frame = CGRect(x: 24, y: txtExplainView.frame.maxY + 5, width: self.frame.width -  45, height: 50)
              
                break
                
            case 1792:
                
                lblSubject.frame = CGRect(x: 20, y: 37, width: self.layer.frame.width - 40, height: 15)
                txtQueView.frame = CGRect(x: 24, y: lblSubject.frame.maxY + 12, width: self.layer.frame.width - 48, height: 120)
                answerView.frame = CGRect(x: 24, y: txtQueView.frame.maxY + 21, width: self.frame.width - 48, height: 95)
                explanationView.frame = CGRect(x: 24, y: answerView.frame.maxY + 20, width: self.frame.width -  48, height: 20)
                txtExplainView.frame = CGRect(x: 24, y: explanationView.frame.maxY + 12, width: self.layer.frame.width - 50, height: 190)
                btnNext.frame = CGRect(x: 50, y: txtExplainView.frame.maxY + 5, width: 250, height: 50)
                
            default:
                
                lblSubject.frame = CGRect(x: 20, y: 37, width: self.layer.frame.width - 40, height: 15)
                txtQueView.frame = CGRect(x: 24, y: lblSubject.frame.maxY + 12, width: self.layer.frame.width - 48, height: 120)
                answerView.frame = CGRect(x: 24, y: txtQueView.frame.maxY + 21, width: self.frame.width - 48, height: 95)
                explanationView.frame = CGRect(x: 24, y: answerView.frame.maxY + 20, width: self.frame.width -  48, height: 20)
                txtExplainView.frame = CGRect(x: 24, y: explanationView.frame.maxY + 12, width: self.layer.frame.width - 50, height: 175)
                btnNext.frame = CGRect(x: 24, y: 400, width: self.frame.width -  45, height: 50)
            }
        }
    }
}

extension UIView {

    func addDashedBorder(strokeColor: UIColor, lineWidth: CGFloat) {
        self.layoutIfNeeded()
        let strokeColor = strokeColor.cgColor

        let shapeLayer:CAShapeLayer = CAShapeLayer()
        let frameSize = self.frame.size
        let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width - 2, height: frameSize.height - 2)

        shapeLayer.bounds = shapeRect
        shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = strokeColor
        shapeLayer.lineWidth = lineWidth
        shapeLayer.lineJoin = CAShapeLayerLineJoin.round

        shapeLayer.lineDashPattern = [9,9] // adjust to your liking
        shapeLayer.path = UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: shapeRect.width, height: shapeRect.height), cornerRadius: self.layer.cornerRadius).cgPath

        self.layer.addSublayer(shapeLayer)
    }

}

