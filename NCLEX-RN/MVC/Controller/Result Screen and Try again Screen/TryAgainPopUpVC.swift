//
//  TryAgainPopUpVC.swift
//  NCLEX-RN
//
//  Created by Kevin M1 on 12/03/22.
//

import UIKit

class TryAgainPopUpVC: UIViewController {

    //MARK: - Variables
    var dicResult = NSMutableDictionary()
    
    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    //MARK: - IBAction
    @IBAction func btnSureTapped(_ sender: Any) {
        
        if isProductPurchased == false{
            
            let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let subVC = mainStoryboard.instantiateViewController(withIdentifier: "ExamTestVC") as! ExamTestVC
            
            subVC.hidesBottomBarWhenPushed = true
            
            if presentingViewController?.presentingViewController != nil{
                self.present(subVC, animated: true, completion: nil)

            } else{
                self.navigationController?.pushViewController(subVC, animated: true)
            }
            
        } else {
         
            
            let examTestVC = self.storyboard?.instantiateViewController(withIdentifier: "ExamTestVC") as! ExamTestVC
//            resultVC.dicResult = self.dicResult
            self.navigationController?.pushViewController(examTestVC, animated: true)
        }
        
    }
    
    @IBAction func btnCancelTapped(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
}
