//
//  ResultTopVC.swift
//  NCLEX-RN
//
//  Created by Kevin M1 on 27/04/22.
//

import UIKit
import MBCircularProgressBar
import AMTabView

class ResultTopVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
  
    //MARK: - IBOutltes
    @IBOutlet weak var progressView: MBCircularProgressBarView!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblCorrect: UILabel!
    @IBOutlet weak var lblError: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblExcellent: UILabel!
    @IBOutlet weak var viewWrongQue: UIView!
    @IBOutlet weak var btnTryAgain: UIButton!
    @IBOutlet weak var btnTryHeightConstraint: NSLayoutConstraint!
    
    //MARK: - Variables

    var isFromQuickTest = false
    var isFromWrong = false
    var isFromCustomized = false
    var isFromDaily = false
    var isFromDailyCheckin = false
    var time = ""

    var dicResult = NSMutableDictionary()
    
    var arrQuestions = NSArray()
    var arrOptions = NSMutableArray()
    
    //MARK: - View LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tblView.delegate = self
        tblView.dataSource = self
        
        if isFromCustomized == true{
            
            let time = UserDefaults.standard.value(forKey: "selectedTime")
            
            if time != nil {
                self.lblTime.text = "\(time ?? ""):00"
            } else {
                self.lblTime.text = "00:00"
            }
        } else {
            self.lblTime.text = time
        }
        
        UserDefaults.standard.synchronize()
        
        tblView.register(UINib(nibName: "ResultWrongQueHeaderCell", bundle: nil), forHeaderFooterViewReuseIdentifier: "ResultWrongQueHeaderCell")
        
        viewWrongQue.isHidden = true
        
        self.lblCorrect.text = dicResult.value(forKey: "correctAnswer") as? String
        self.lblError.text = dicResult.value(forKey: "IncorrectAnswer") as? String
        
        let correctAns = (Int)((dicResult.value(forKey: "correctAnswer") as? String) ?? "")
        let totalQuestion = (Int)((dicResult.value(forKey: "totalQuestion") as? String) ?? "")
        
        if correctAns != nil{
            
            let correctCount = (CGFloat)(correctAns!)
            let percentage = (correctCount * 100)/(CGFloat)(totalQuestion!)
            
            progressView.value = (CGFloat)(percentage)
            
            if percentage >= 80{
                lblExcellent.text = "Excellent"
            } else if percentage >= 50  {
                lblExcellent.text = "Good"
            } else if percentage == 0{
                lblExcellent.text = "Very Bad"
            }
            
        } else {
            progressView.value = 0
            self.lblExcellent.text = "Very Bad"
            self.lblCorrect.text = "0"
            self.lblError.text = (dicResult.value(forKey: "IncorrectAnswer") as? String)
        }
        
        if isProductPurchased == true{
            self.btnTryAgain.isHidden = true
            btnTryHeightConstraint.constant = 0
        } else {
            self.btnTryAgain.isHidden = false
            btnTryHeightConstraint.constant = 50
        }
        
        if isFromQuickTest == true{
            arrQuestions = DBHandler.getDataFromTable("SELECT * FROM quick_test WHERE is_wrong = '1'")
            print(arrQuestions.count)
            
        } else if isFromCustomized == true{
            arrQuestions = DBHandler.getDataFromTable("SELECT * FROM customized_test WHERE is_wrong = '1'")
            print(arrQuestions.count)
            
        } else if isFromWrong == true{
            arrQuestions = DBHandler.getDataFromTable("SELECT * FROM question WHERE is_wrong = '1'")
            print(arrQuestions.count)
            
        } else if isFromDaily == true{
            arrQuestions = DBHandler.getDataFromTable("SELECT * FROM daily_test WHERE is_wrong = '1'")
            print(arrQuestions.count)
            
        } else if isFromDailyCheckin == true{
            arrQuestions = DBHandler.getDataFromTable("SELECT * FROM daily_checkin WHERE is_wrong = '1'")
            print(arrQuestions.count)
        }
          
    }
    
    //MARK: - TableView Delegate & Datasource

    func numberOfSections(in tableView: UITableView) -> Int {
        return arrQuestions.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let dicQuestion = arrQuestions.object(at: section) as! NSDictionary
       
        let strFinalAnswer = dicQuestion.value(forKey: "correct_option") as! String
        
        let question_id = dicQuestion.value(forKey: "que_id") as! String
        let array = DBHandler.getDataFromTable("SELECT * FROM question WHERE id = '\(question_id)'")
        
        let dicObject = array?.firstObject as! NSDictionary
        
        let strOption = dicObject.value(forKey: "choices") as! String
        let strFinalOption = strOption.replacingOccurrences(of: "[\"", with: "").replacingOccurrences(of: "\"]", with: "")
        let strFinal = strFinalOption.replacingOccurrences(of: "\"", with: "").replacingOccurrences(of: "\"", with: "")
        
        var arrOption : [String] = []
        
        arrOption = strFinal.components(separatedBy: "$")
        let count1 = arrOption.count
        
            arrOption.insert(strFinalAnswer, at: count1)

        
        self.arrOptions.removeAllObjects()
        
        for index in 0..<arrOption.count
        {
            let id = arrOption[index]
            self.arrOptions.add(id)
        }
        
        return arrOptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblView.dequeueReusableCell(withIdentifier: "ResultWrongQueCell", for: indexPath) as! ResultWrongQueCell
        
        let dicQuestion = arrQuestions.object(at: indexPath.section) as! NSDictionary
       
        var strFinalAnswer = dicQuestion.value(forKey: "correct_option") as! String
        strFinalAnswer = strFinalAnswer.getReplaceValue()
        var strWrongAnswer = dicQuestion.value(forKey: "wrong_option") as! String
        strWrongAnswer = strWrongAnswer.getReplaceValue()
        
        let question_id = dicQuestion.value(forKey: "que_id") as! String
        let array = DBHandler.getDataFromTable("SELECT * FROM question WHERE id = '\(question_id)'")
        
        let dicObject = array?.firstObject as! NSDictionary
        let strOption = dicObject.value(forKey: "choices") as! String
        
        let strFinalOption = strOption.replacingOccurrences(of: "[\"", with: "").replacingOccurrences(of: "\"]", with: "")
        let strFinal = strFinalOption.replacingOccurrences(of: "\"", with: "").replacingOccurrences(of: "\"", with: "")
        var arrOption : [String] = []
        
        arrOption = strFinal.components(separatedBy: "$")
        let count1 = arrOption.count
        
        arrOption.insert(strFinalAnswer, at: count1)

        self.arrOptions.removeAllObjects()
        
        for index in 0..<arrOption.count
        {
            let id = arrOption[index]
            self.arrOptions.add(id)
        }
        
        cell.lblOption.text = arrOptions[indexPath.row] as? String
        
        if cell.lblOption.text == strFinalAnswer{
            cell.imgView.image = UIImage(named: "ic_right")
            cell.imgView.isHidden = false
            
        } else if cell.lblOption.text == strWrongAnswer{
            cell.imgView.image = UIImage(named: "ic_wrong1")
            cell.imgView.isHidden = false
            
        } else {
            cell.imgView.isHidden = true
        }
       
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let view = tblView.dequeueReusableHeaderFooterView(withIdentifier: "ResultWrongQueHeaderCell") as! ResultWrongQueHeaderCell
        
        let dicQuestion = arrQuestions.object(at: section) as! NSDictionary
        let question_id = dicQuestion.value(forKey: "que_id") as! String
    
        let array = DBHandler.getDataFromTable("SELECT * FROM question WHERE id = '\(question_id)'")
        
        let dicObject = array?.firstObject as! NSDictionary
        let strQuestion = dicObject.value(forKey: "question") as! String
        
        view.lblQues.text = strQuestion
       
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    //MARK: - IBAction
    
    @IBAction func clickedOnClose(_ sender: Any) {
        
        let deleteQuickTestSuccess = DBHandler.genericQuery("DELETE FROM quick_test")
        print(deleteQuickTestSuccess)
        let deleteCustomizedTestSuccess = DBHandler.genericQuery("DELETE FROM customized_test")
        print(deleteCustomizedTestSuccess)
        let deleteWrongTestSuccess = DBHandler.genericQuery("DELETE FROM wrong_test")
        print(deleteWrongTestSuccess)
        let deleteDailyTestSuccess = DBHandler.genericQuery("DELETE FROM daily_test")
        print(deleteDailyTestSuccess)
        
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! AMTabsViewController
        let navigationController = UINavigationController(rootViewController: nextViewController)
        navigationController.isNavigationBarHidden = true
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = navigationController
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func clickedOnTry(_ sender: Any) {
        
        if isProductPurchased == true{
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "TryAgainPopUpVC") as! TryAgainPopUpVC
            let vc = UINavigationController(rootViewController: nextViewController)
            vc.isNavigationBarHidden = true

            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle   = .crossDissolve
            self.present(vc, animated: true, completion: nil)
            
        } else  {
            
            let PurchaseVC = self.storyboard?.instantiateViewController(withIdentifier: "SubscriptionVC") as! SubscriptionVC
            self.navigationController?.pushViewController(PurchaseVC, animated: true)
            
        }
        
    }
    
}
