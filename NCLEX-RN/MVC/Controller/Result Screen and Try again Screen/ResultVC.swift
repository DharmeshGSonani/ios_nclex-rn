//
//  ResultVC.swift
//  NCLEX-RN
//
//  Created by Kevin M1 on 09/03/22.
//

import UIKit
import MBCircularProgressBar
import AMTabView
import SwiftUI

class ResultVC: UIViewController, UITableViewDelegate, UITableViewDataSource{
  
    //MARK: - IBOutlets
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var progressView: MBCircularProgressBarView!
    @IBOutlet weak var lblCorrect: UILabel!
    @IBOutlet weak var lblError: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var imgCard: UIImageView!
    @IBOutlet weak var btnTryAgain: UIButton!
    @IBOutlet weak var viewWrongQue: UIView!
    @IBOutlet weak var lblExcellent: UILabel!
    @IBOutlet weak var imgAllRightQue: UIImageView!
    @IBOutlet weak var lblGreat: UILabel!
    @IBOutlet weak var btnTryHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var lblWrongQuestion: UILabel!
    
    //MARK: - Variables
    var dicResult = NSMutableDictionary()
    
    var arrQuestions = NSArray()
    var arrOptions = NSMutableArray()
    var part_id = ""
    
    var isFromTopicVC = false
    var isFromWrong = false
    
    var time = ""
    
    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()

        tblView.delegate = self
        tblView.dataSource = self

        UserDefaults.standard.synchronize()
        
        tblView.register(UINib(nibName: "ResultWrongQueHeaderCell", bundle: nil), forHeaderFooterViewReuseIdentifier: "ResultWrongQueHeaderCell")
        
        viewWrongQue.isHidden = true
        
        self.lblCorrect.text = dicResult.value(forKey: "correctAnswer") as? String
        self.lblError.text = dicResult.value(forKey: "IncorrectAnswer") as? String
        
        let correctAns = (Int)((dicResult.value(forKey: "correctAnswer") as? String) ?? "")
        let totalQuestion = (Int)((dicResult.value(forKey: "totalQuestion") as? String) ?? "")
        
        if correctAns != nil{
            
            let correctCount = (CGFloat)(correctAns!)
            let percentage = (correctCount * 100)/(CGFloat)(totalQuestion!)
            
            progressView.value = (CGFloat)(percentage)
            
            if percentage >= 80{
                lblExcellent.text = "Excellent"
            } else if percentage >= 50  {
                lblExcellent.text = "Good"
            } else if percentage == 0{
                lblExcellent.text = "Very Bad"
            }
            
            if correctAns == totalQuestion{
                self.lblError.text = "0"
            }
            
        } else {
            self.progressView.value = 0
            self.lblExcellent.text = "Very Bad"
            self.lblCorrect.text = "0"
            self.lblError.text = (dicResult.value(forKey: "IncorrectAnswer") as? String)
        }
        
        if isProductPurchased == true{
            self.btnTryAgain.isHidden = true
            btnTryHeightConstraint.constant = 0
        } else {
            self.btnTryAgain.isHidden = false
            btnTryHeightConstraint.constant = 50
        }
       
        let categoryTest = AppConstant.shared.CategoryTest
        
        if isFromTopicVC == true{
            
            let time = UserDefaults.standard.value(forKey: "selectedTime")
    
            if time != nil {
                self.lblTime.text = "\(time ?? ""):00"
            } else {
                self.lblTime.text = "00:00"
            }
            
            arrQuestions = DBHandler.getDataFromTable("SELECT * FROM question WHERE is_wrong = '1' AND topicName = '\(categoryTest)' AND part_id = '\(part_id)'")
            print(arrQuestions.count)
            
        } else if isFromWrong == true{
            
            lblTime.text = time

            arrQuestions = DBHandler.getDataFromTable("SELECT * FROM question WHERE is_wrong = '1'")
            print(arrQuestions.count)
            
        }
                    
        if arrQuestions.count != 0{
            self.lblGreat.isHidden = true
            self.imgAllRightQue.isHidden = true
        } else {
            self.lblGreat.isHidden = false
            self.imgAllRightQue.isHidden = false
        }
       
        
    }
    
    //MARK: - TableView Delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrQuestions.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let dicQuestion = arrQuestions.object(at: section) as! NSDictionary
        
        let strFinalAnswer = dicQuestion.value(forKey: "correct_option") as! String
        
//        let strAnswer = dicQuestion.value(forKey: "answers") as! String
//        let strFinalAnswer = strAnswer.replacingOccurrences(of: "[\"", with: "").replacingOccurrences(of: "\"]", with: "")
        
        let strOption = dicQuestion.value(forKey: "choices") as! String
        let strFinalOption = strOption.replacingOccurrences(of: "[\"", with: "").replacingOccurrences(of: "\"]", with: "")
        let strFinal = strFinalOption.replacingOccurrences(of: "\"", with: "").replacingOccurrences(of: "\"", with: "")
        
        var arrOption : [String] = []
        
        arrOption = strFinal.components(separatedBy: "$")
        let count1 = arrOption.count
        arrOption.insert(strFinalAnswer, at: count1)
        
        self.arrOptions.removeAllObjects()
        
        for index in 0..<arrOption.count
        {
            let id = arrOption[index]
            self.arrOptions.add(id)
        }
        
//        self.arrOptions = (arrOption.shuffled() as NSArray).mutableCopy() as! NSMutableArray
        
        return arrOptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblView.dequeueReusableCell(withIdentifier: "ResultWrongQueCell", for: indexPath) as! ResultWrongQueCell
        
        let dicQuestion = arrQuestions.object(at: indexPath.section) as! NSDictionary
        
        let strFinalAnswer = dicQuestion.value(forKey: "correct_option") as! String
        let strWrongAnswer = dicQuestion.value(forKey: "wrong_option") as! String
        
//        let strAnswer = dicQuestion.value(forKey: "answers") as! String
//        let strFinalAnswer = strAnswer.replacingOccurrences(of: "[\"", with: "").replacingOccurrences(of: "\"]", with: "")
        
        let strOption = dicQuestion.value(forKey: "choices") as! String
        let strFinalOption = strOption.replacingOccurrences(of: "[\"", with: "").replacingOccurrences(of: "\"]", with: "")
        let strFinal = strFinalOption.replacingOccurrences(of: "\"", with: "").replacingOccurrences(of: "\"", with: "")
        
        var arrOption : [String] = []
        
        arrOption = strFinal.components(separatedBy: "$")
        let count1 = arrOption.count
        arrOption.insert(strFinalAnswer, at: count1)
        
        self.arrOptions.removeAllObjects()
        
        for index in 0..<arrOption.count
        {
            let id = arrOption[index]
            self.arrOptions.add(id)
        }
        
//        self.arrOptions = (arrOption.shuffled() as NSArray).mutableCopy() as! NSMutableArray
        
        cell.lblOption.text = arrOptions[indexPath.row] as? String
        
        if cell.lblOption.text == strFinalAnswer{
            cell.imgView.image = UIImage(named: "ic_right")
            cell.imgView.isHidden = false
            
        } else if cell.lblOption.text == strWrongAnswer{
            cell.imgView.image = UIImage(named: "ic_wrong1")
            cell.imgView.isHidden = false
            
        } else {
            cell.imgView.isHidden = true
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let view = tblView.dequeueReusableHeaderFooterView(withIdentifier: "ResultWrongQueHeaderCell") as! ResultWrongQueHeaderCell
        
        let dicData = arrQuestions.value(forKey: "question") as! NSArray
        view.lblQues.text = dicData[section] as? String
       
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    //MARK: - IBAction
    @IBAction func btnCloseClicked(_ sender: Any) {
        
        let updateQuestionStatus = DBHandler.genericQuery("UPDATE * FROM question WHERE is_wrong = ''")
        print(updateQuestionStatus)
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! AMTabsViewController
        let navigationController = UINavigationController(rootViewController: nextViewController)
        navigationController.isNavigationBarHidden = true
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = navigationController
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func BtnTryAgainTapped(_ sender: Any) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "TryAgainPopUpVC") as! TryAgainPopUpVC
        let vc = UINavigationController(rootViewController: nextViewController)
        vc.isNavigationBarHidden = true

        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle   = .crossDissolve
        self.present(vc, animated: true, completion: nil)
        
    }
    
}
