//
//  FirstVC.swift
//  NCLEX-RN
//
//  Created by Kevin M1 on 07/03/22.
//

import UIKit
import CryptoKit
import AMTabView

class FirstVC: UIViewController, UITableViewDelegate,UITableViewDataSource, TabItem{
    
    var tabImage: UIImage?{
        return UIImage(named: "ic_home")
    }
    
    //MARK: - IBOutlets
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblDays: UILabel!
    @IBOutlet weak var lblQuestion: UILabel!
    @IBOutlet weak var lblDailyQue: UILabel!
    @IBOutlet weak var lblQuickTest: UILabel!
    @IBOutlet weak var lblMissedTest: UILabel!
    @IBOutlet weak var lblCustomizedTest: UILabel!
    @IBOutlet weak var imgLock: UIImageView!
    @IBOutlet weak var imgLock1: UIImageView!
    @IBOutlet weak var btnCheckIn: UIButton!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var lblCheckIn: UILabel!
    
    //MARK: - Variables
    var arrImg = ["ic_cell6", "ic_cell1", "ic_cell3", "ic_cell4", "ic_cell8", "ic_cell2", "ic_cell5", "ic_cell7"]
    //    var arrTitle = ["Management of Care", "Safety and Infection Control", "Health Promotion and Maintenance", "Psychosocial Integrity", "Basic Care and Comfort", "Pharmacological and Parenteral Therapies", "Reduction of Risk Potential", "Physiological Adaptation" ]
    
    var arrQuestions = NSMutableArray()
    var arrWrongQuestion = NSMutableArray()
    var arrCatIds : [String] = []
    
    var arrQuick = NSMutableArray()
    var arrCustom = NSMutableArray()
    var arrDaily = NSMutableArray()
    var arrTopic = NSMutableArray()
    
    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblView.delegate = self
        tblView.dataSource = self
        
        if isProductPurchased == false{
            imgLock.isHidden = false
            imgLock1.isHidden = false
        } else {
            imgLock.isHidden = true
            imgLock1.isHidden = true
        }
                
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
            
        NotificationCenter.default.addObserver(self, selector: #selector(self.callingData(noti:)), name: Notification.Name.fillData, object: nil)
        
        self.setupQuestions()
                
        if appDelegate.isFromCheckIn == true{
            
            appDelegate.isFromCheckIn = false
            
            let showQuestionTotal = appDelegate.totalQuestionShow
            
            if showQuestionTotal >= 1 {
                
                let attributedString1 = NSMutableAttributedString(string: "\(appDelegate.totalQuestionShow)", attributes: [
                    NSAttributedString.Key.font: UIFont(name: "Montserrat-Regular", size: 10)!,
                    .foregroundColor: UIColor.init(red: 93/255, green: 97/255, blue: 132/255, alpha: 1)])
                
                let attributedString2 = NSMutableAttributedString(string: "/3 questions", attributes: [
                    NSAttributedString.Key.font: UIFont(name: "Montserrat-Regular", size: 10)!,
                    .foregroundColor: UIColor.init(red: 93/255, green: 97/255, blue: 132/255, alpha: 1)])
                
                let priceNSMutableAttributedString = NSMutableAttributedString()
                priceNSMutableAttributedString.append(attributedString1)
                priceNSMutableAttributedString.append(attributedString2)
                
                self.lblQuestion.attributedText = priceNSMutableAttributedString
                
                let totalQuestion : Int = 3
                let totalViewQue = appDelegate.totalQuestionShow
                
                let percentage = ((CGFloat)(totalViewQue * 100)/(CGFloat)(totalQuestion))
                self.progressView.progress = Float(percentage/100)
                
                appDelegate.totalTimeAppOpenCount  = appDelegate.totalTimeAppOpenCount + 1
                
                UserDefaults.standard.setValue(self.progressView.progress, forKey: "showProgress")
                UserDefaults.standard.setValue("\(appDelegate.totalTimeAppOpenCount)", forKey: "daysShow")
                UserDefaults.standard.setValue("\(totalViewQue)", forKey: "showQuestion")
                
            }
        }
        
        let days = UserDefaults.standard.value(forKey: "daysShow") ?? ""
        if days as! String != "" {
            self.lblDays.text = "0\(days)"
        } else {
            self.lblDays.text = "00"
        }
        
        let showQuestion = UserDefaults.standard.value(forKey: "showQuestion") ?? ""
        
        if showQuestion as! String >= "1" {
            self.btnCheckIn.setBackgroundImage(UIImage(named: "ic_complete"), for: .normal)
            self.lblCheckIn.isHidden = true
            self.lblQuestion.text = "\(showQuestion)/3 questions"
        } else {
//            self.btnCheckIn.setBackgroundImage(UIImage(named: "ic_complete"), for: .normal)
            self.lblQuestion.text = "0/3 questions"
        }
        
        let showProgress = UserDefaults.standard.value(forKey: "showProgress")
        
        if showProgress != nil{
            
            self.progressView.progress = showProgress as! Float
            
        } else {
            self.progressView.progress = 0
        }
        
        
        if appDelegate.isOpenCheckInPopUP == true{

            appDelegate.isOpenCheckInPopUP = false
            let vc = self.storyboard?.instantiateViewController(withIdentifier:"QuickTestPopUpVC")  as! QuickTestPopUpVC
            vc.mainTitle = self.lblQuickTest.text!
            vc.modalTransitionStyle  = .crossDissolve;
            vc.modalPresentationStyle = .overCurrentContext
            vc.hidesBottomBarWhenPushed = true

            self.present(vc, animated: true, completion: nil)
        }
       
        
    }
    
    @objc func callingData(noti:Notification) {
        self.setupQuestions()
        self.tblView.reloadData()
    }
    
    func setupQuestions()
    {
        arrQuestions = DBHandler.getDataFromTable("SELECT * FROM question")
        print(arrQuestions)
        
        let arrCategory = arrQuestions.value(forKey: "topicName") as! [String]
        self.arrCatIds = arrCategory.uniqued()
        
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        self.tblView.reloadData()
//    }
    
    //MARK: - TableView Delegate
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCatIds.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblView.dequeueReusableCell(withIdentifier: "AllSubjectTableViewCell", for: indexPath) as! AllSubjectTableViewCell
        
        cell.imgView.image = UIImage(named: arrImg[indexPath.row])
        
        //        let dicData = arrQue.object(at: indexPath.row) as! NSDictionary
        //        let name = dicData.value(forKey: "topicName") as? String
        
        cell.lblTitle.text = arrCatIds[indexPath.row]
        
        let predicate = NSPredicate.init(format: "topicName = %@", self.arrCatIds[indexPath.row])
        let filteredArray = self.arrQuestions.filtered(using: predicate) as NSArray
        
        let count = filteredArray.count
        
        let attributedString1 = NSMutableAttributedString(string: "\(count)", attributes: [
            NSAttributedString.Key.font: UIFont(name: "Montserrat-Regular", size: 10)!,
            .foregroundColor: UIColor(named: "textColorLight") as Any])
        
        let attributedString2 = NSMutableAttributedString(string: " Exam Questions", attributes: [
            NSAttributedString.Key.font: UIFont(name: "Montserrat-Regular", size: 10)!,
            .foregroundColor: UIColor(named: "textColorLight") as Any])
        
        let priceNSMutableAttributedString = NSMutableAttributedString()
        priceNSMutableAttributedString.append(attributedString1)
        priceNSMutableAttributedString.append(attributedString2)
        
        cell.lblTotalque.attributedText = priceNSMutableAttributedString
        
        if isProductPurchased == false {
            cell.lblCount.isHidden = true
            cell.imgView1.isHidden = false
            cell.imgView1.image = UIImage(named: "ic_lock")
        }
        
        var totalQuestions : Int = 0
        var totalViewCount : Int = 0
        
        if filteredArray.count > 0{
            
            let topic_id = (filteredArray.firstObject as! NSDictionary).value(forKey: "topicId") as! String
            
            totalQuestions =  filteredArray.count
            
            let allReadRecord = DBHandler.getDataFromTable("SELECT * FROM read_question WHERE topic_id = '\(topic_id)'")!
            let predicate1 = NSPredicate.init(format: "topic_id = %@", topic_id)
            let filteredArray1 = allReadRecord.filtered(using: predicate1)
            
            if filteredArray1.count > 0{
                
                var viewCardCount = 0
                for i in 0..<filteredArray1.count {
                    
                    let dicObject = filteredArray1[i] as! NSDictionary
                    viewCardCount = (Int)(dicObject.value(forKey: "viewcount") as! String)!
                    totalViewCount = totalViewCount + viewCardCount
                }
                
                let percentage = ((CGFloat)(totalViewCount * 100)/(CGFloat)(totalQuestions))
                cell.progressView.progress = Float(percentage/100)
                
                let per  = (CGFloat(percentage))
                let toFloat = (String(format:"%.f", per))
                cell.lblCount.text = "\(toFloat)%"
                
            } else {
                
                cell.progressView.progress = 0
                cell.lblCount.text = "0%"
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if arrQuestions.count != 0{
            
            let QueCategory = arrQuestions.object(at: indexPath.row) as! NSDictionary
            
            let predicate = NSPredicate.init(format: "topicName = %@", self.arrCatIds[indexPath.row])
            let filteredArray = self.arrQuestions.filtered(using: predicate) as NSArray
            
            if isProductPurchased == false{
                
                let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let subVC = mainStoryboard.instantiateViewController(withIdentifier: "SubscriptionVC") as! SubscriptionVC
                subVC.hidesBottomBarWhenPushed = true
                
                if self.presentingViewController?.presentedViewController != nil{
                    self.navigationController?.pushViewController(subVC, animated: true)
                } else {
                    self.present(subVC, animated: true, completion: nil)
                }
                
            } else {
                
                if indexPath.row == 0{
                    
                    let finalArray = NSMutableArray()
                    finalArray.addObjects(from: filteredArray as! [Any])
                    
                    let topic_id = (finalArray.firstObject as! NSDictionary).value(forKey: "topicId") as! String
                    
                    let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let SubWiseVC = mainStoryboard.instantiateViewController(withIdentifier: "SubjectWiseVC") as! SubjectWiseVC
                    SubWiseVC.imageData = UIImage(named: "ic_cell6")!
                    AppConstant.shared.CategoryTest = "Pharmacological and Parenteral Therapies"
                    SubWiseVC.arrPharma = finalArray
                    SubWiseVC.mainTitle = arrCatIds[indexPath.row]
                    SubWiseVC.lblMainTopic = arrCatIds[indexPath.row]
                    SubWiseVC.topic_id = topic_id
                    SubWiseVC.hidesBottomBarWhenPushed = true
                    SubWiseVC.dicSelectedCategory = QueCategory
                    
                    self.navigationController?.pushViewController(SubWiseVC, animated: true)
                    
                }
                else if indexPath.row == 1
                {
                    let finalArray = NSMutableArray()
                    finalArray.addObjects(from: filteredArray as! [Any])
                    
                    let topic_id = (finalArray.firstObject as! NSDictionary).value(forKey: "topicId") as! String
                    
                    let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let SubWiseVC = mainStoryboard.instantiateViewController(withIdentifier: "SubjectWiseVC") as! SubjectWiseVC
                    SubWiseVC.imageData = UIImage(named: "ic_cell1")!
                    AppConstant.shared.CategoryTest = "Management of Care"
                    SubWiseVC.arrManagement = finalArray
                    SubWiseVC.lblMainTopic = arrCatIds[indexPath.row]
                    SubWiseVC.mainTitle = arrCatIds[indexPath.row]
                    SubWiseVC.topic_id = topic_id
                    SubWiseVC.hidesBottomBarWhenPushed = true
                    SubWiseVC.dicSelectedCategory = QueCategory
                    self.navigationController?.pushViewController(SubWiseVC, animated: true)
                }
                else if indexPath.row == 2
                {
                    let finalArray = NSMutableArray()
                    finalArray.addObjects(from: filteredArray as! [Any])
                    
                    let topic_id = (finalArray.firstObject as! NSDictionary).value(forKey: "topicId") as! String
                    
                    let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let SubWiseVC = mainStoryboard.instantiateViewController(withIdentifier: "SubjectWiseVC") as! SubjectWiseVC
                    SubWiseVC.imageData = UIImage(named: "ic_cell3")!
                    AppConstant.shared.CategoryTest = "Health Promotion and Maintenance"
                    SubWiseVC.arrHealth = finalArray
                    SubWiseVC.mainTitle = arrCatIds[indexPath.row]
                    SubWiseVC.lblMainTopic = arrCatIds[indexPath.row]
                    SubWiseVC.topic_id = topic_id
                    SubWiseVC.hidesBottomBarWhenPushed = true
                    SubWiseVC.dicSelectedCategory = QueCategory
                    self.navigationController?.pushViewController(SubWiseVC, animated: true)
                }
                else if indexPath.row == 3
                {
                    let finalArray = NSMutableArray()
                    finalArray.addObjects(from: filteredArray as! [Any])
                    
                    let topic_id = (finalArray.firstObject as! NSDictionary).value(forKey: "topicId") as! String
                    
                    let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let SubWiseVC = mainStoryboard.instantiateViewController(withIdentifier: "SubjectWiseVC") as! SubjectWiseVC
                    SubWiseVC.imageData = UIImage(named: "ic_cell4")!
                    AppConstant.shared.CategoryTest = "Psychosocial Integrity"
                    SubWiseVC.arrIntegrity = finalArray
                    SubWiseVC.mainTitle = arrCatIds[indexPath.row]
                    SubWiseVC.lblMainTopic = arrCatIds[indexPath.row]
                    SubWiseVC.topic_id = topic_id
                    SubWiseVC.hidesBottomBarWhenPushed = true
                    SubWiseVC.dicSelectedCategory = QueCategory
                    self.navigationController?.pushViewController(SubWiseVC, animated: true)
                }
                else if indexPath.row == 4
                {
                    let finalArray = NSMutableArray()
                    finalArray.addObjects(from: filteredArray as! [Any])
                    
                    let topic_id = (finalArray.firstObject as! NSDictionary).value(forKey: "topicId") as! String
                    
                    let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let SubWiseVC = mainStoryboard.instantiateViewController(withIdentifier: "SubjectWiseVC") as! SubjectWiseVC
                    SubWiseVC.imageData = UIImage(named: "ic_cell8")!
                    AppConstant.shared.CategoryTest = "Physiological Adaptation"
                    SubWiseVC.arrAdoption = finalArray
                    SubWiseVC.mainTitle = arrCatIds[indexPath.row]
                    SubWiseVC.lblMainTopic = arrCatIds[indexPath.row]
                    SubWiseVC.topic_id = topic_id
                    SubWiseVC.hidesBottomBarWhenPushed = true
                    SubWiseVC.dicSelectedCategory = QueCategory
                    self.navigationController?.pushViewController(SubWiseVC, animated: true)
                }
                else if indexPath.row == 5
                {
                    let finalArray = NSMutableArray()
                    finalArray.addObjects(from: filteredArray as! [Any])
                    
                    let topic_id = (finalArray.firstObject as! NSDictionary).value(forKey: "topicId") as! String
                    
                    let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let SubWiseVC = mainStoryboard.instantiateViewController(withIdentifier: "SubjectWiseVC") as! SubjectWiseVC
                    SubWiseVC.imageData = UIImage(named: "ic_cell2")!
                    SubWiseVC.arrSafety = finalArray
                    AppConstant.shared.CategoryTest = "Safety and Infection Control"
                    SubWiseVC.mainTitle = arrCatIds[indexPath.row]
                    SubWiseVC.lblMainTopic = arrCatIds[indexPath.row]
                    SubWiseVC.topic_id = topic_id
                    SubWiseVC.hidesBottomBarWhenPushed = true
                    SubWiseVC.dicSelectedCategory = QueCategory
                    self.navigationController?.pushViewController(SubWiseVC, animated: true)
                }
                else if indexPath.row == 6
                {
                    let finalArray = NSMutableArray()
                    finalArray.addObjects(from: filteredArray as! [Any])
                    
                    let topic_id = (finalArray.firstObject as! NSDictionary).value(forKey: "topicId") as! String
                    
                    let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let SubWiseVC = mainStoryboard.instantiateViewController(withIdentifier: "SubjectWiseVC") as! SubjectWiseVC
                    SubWiseVC.imageData = UIImage(named: "ic_cell5")!
                    SubWiseVC.arrBasicAndCare = finalArray
                    AppConstant.shared.CategoryTest = "Basic Care and Comfort"
                    SubWiseVC.mainTitle = arrCatIds[indexPath.row]
                    SubWiseVC.lblMainTopic = arrCatIds[indexPath.row]
                    SubWiseVC.topic_id = topic_id
                    SubWiseVC.hidesBottomBarWhenPushed = true
                    SubWiseVC.dicSelectedCategory = QueCategory
                    self.navigationController?.pushViewController(SubWiseVC, animated: true)
                }
                else if indexPath.row == 7
                {
                    let finalArray = NSMutableArray()
                    finalArray.addObjects(from: filteredArray as! [Any])
                    
                    let topic_id = (finalArray.firstObject as! NSDictionary).value(forKey: "topicId") as! String
                    
                    let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let SubWiseVC = mainStoryboard.instantiateViewController(withIdentifier: "SubjectWiseVC") as! SubjectWiseVC
                    SubWiseVC.imageData = UIImage(named: "ic_cell7")!
                    SubWiseVC.arrReduction = finalArray
                    AppConstant.shared.CategoryTest = "Reduction of Risk Potential"
                    SubWiseVC.mainTitle = arrCatIds[indexPath.row]
                    SubWiseVC.lblMainTopic = arrCatIds[indexPath.row]
                    SubWiseVC.topic_id = topic_id
                    SubWiseVC.hidesBottomBarWhenPushed = true
                    SubWiseVC.dicSelectedCategory = QueCategory
                    self.navigationController?.pushViewController(SubWiseVC, animated: true)
                }
            }
        }
    }
    
    //MARK: - IBAction
    @IBAction func btnDailyQuestion(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier:"DailyQuePopUpVC") as! DailyQuePopUpVC
        vc.mainTitle = lblDailyQue.text!
        vc.isFromDailyQuestion = true
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle   = .crossDissolve
        vc.hidesBottomBarWhenPushed = true
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func btnQuickTest(_ sender: Any) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "QuickTestPopUpVC") as! QuickTestPopUpVC
        nextViewController.mainTitle = lblQuickTest.text!
        let vc = UINavigationController(rootViewController: nextViewController)
        vc.isNavigationBarHidden = true
        
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle   = .crossDissolve
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func btnMissedTest(_ sender: Any) {
        
        if isProductPurchased == false{
            
            let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let SubVC = mainStoryboard.instantiateViewController(withIdentifier: "SubscriptionVC") as! SubscriptionVC
            
            SubVC.hidesBottomBarWhenPushed = true
            
            if self.presentingViewController?.presentedViewController != nil{
                self.navigationController?.pushViewController(SubVC, animated: true)
            }
            else{
                self.present(SubVC, animated: true, completion: nil)
            }
            
        } else {
            
            arrQuestions = DBHandler.getDataFromTable("SELECT * FROM question WHERE is_wrong = '1'")
            
//            arrQuick = DBHandler.getDataFromTable("SELECT * FROM quick_test WHERE is_wrong = '1'")
//            arrDaily = DBHandler.getDataFromTable("SELECT * FROM daily_test WHERE is_wrong = '1'")
//            arrCustom = DBHandler.getDataFromTable("SELECT * FROM customized_test WHERE is_wrong = '1'")
//
//            arrWrongQuestion.add(arrQuick)
//            arrWrongQuestion.add(arrDaily)
//            arrWrongQuestion.add(arrCustom)
//
//            print(arrWrongQuestion.count)
            
            if arrQuestions.count != 0{
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "WrongQuePopUpVC") as! WrongQuePopUpVC
                nextViewController.mainTitle = lblMissedTest.text!
                let vc = UINavigationController(rootViewController: nextViewController)
                vc.isNavigationBarHidden = true
                
                vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle   = .crossDissolve
                self.present(vc, animated: true, completion: nil)
                
            } else {
                
                let alert = UIAlertController(title: "No Wrong Question Available", message: "", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    self.dismiss(animated: true, completion: nil)
                }))
                self.present(alert, animated: true, completion: nil)
            }
            
        }
        
    }
    
    @IBAction func btnCustomizedTest(_ sender: Any) {
        
        if isProductPurchased == false{
            
            imgLock.isHidden = false
            let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let SubVC = mainStoryboard.instantiateViewController(withIdentifier: "SubscriptionVC") as! SubscriptionVC
            SubVC.hidesBottomBarWhenPushed = true
            
            if self.presentingViewController?.presentedViewController != nil{
                self.navigationController?.pushViewController(SubVC, animated: true)
            }
            else{
                self.present(SubVC, animated: true, completion: nil)
            }
            
        } else {
            
            imgLock.isHidden = true
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "CustomizedTestPopUpVC") as! CustomizedTestPopUpVC
            nextViewController.mainTitle = lblCustomizedTest.text!
            let vc = UINavigationController(rootViewController: nextViewController)
            vc.isNavigationBarHidden = true
            
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle   = .crossDissolve
            self.present(vc, animated: true, completion: nil)
            
        }
    }
    
    @IBAction func btnCheckIn(_ sender: Any) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate

        if appDelegate.isComeFirstTime == true{
            
            appDelegate.isComeFirstTime = false
            let vc = self.storyboard?.instantiateViewController(withIdentifier:"DailyQuePopUpVC") as! DailyQuePopUpVC
            vc.mainTitle = lblCheckIn.text!
            vc.isFromDailyCheckIn = true
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle   = .crossDissolve
            vc.hidesBottomBarWhenPushed = true
            self.present(vc, animated: true, completion: nil)
            self.btnCheckIn.isUserInteractionEnabled = true
            
        } else {
            
            self.btnCheckIn.isUserInteractionEnabled = false
        }
        
//        if UserDefaults.standard.object(forKey: "isFirstClick") == nil{
//
//            UserDefaults.standard.set(true, forKey: "isFirstClick")
//            UserDefaults.standard.synchronize()
//
//            let vc = self.storyboard?.instantiateViewController(withIdentifier:"DailyQuePopUpVC") as! DailyQuePopUpVC
//            vc.mainTitle = lblCheckIn.text!
//            vc.isFromDailyCheckIn = true
//            vc.modalTransitionStyle   = .crossDissolve;
//            vc.modalPresentationStyle = .overCurrentContext
//            self.present(vc, animated: true, completion: nil)
//            self.btnCheckIn.isUserInteractionEnabled = true
//
//        }
        
    }
        
    
}
extension Sequence where Element: Hashable {
    func uniqued() -> [Element] {
        var set = Set<Element>()
        return filter { set.insert($0).inserted }
    }
}
