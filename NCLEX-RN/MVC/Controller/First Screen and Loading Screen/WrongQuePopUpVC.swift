//
//  WrongQuePopUpVC.swift
//  NCLEX-RN
//
//  Created by Kevin M1 on 08/03/22.
//

import UIKit

class WrongQuePopUpVC: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
       
    //MARK: - IBOutlets
    @IBOutlet weak var pickerView: UIPickerView!
    
    //MARK: - Variables
    var mainTitle = ""
    var count = 0
    
    var arrQuestions = NSMutableArray()
    
    var arrQuick = NSMutableArray()
    var arrCustom = NSMutableArray()
    var arrDaily = NSMutableArray()
    var arrTopic = NSMutableArray()

    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()

        pickerView.delegate = self
        pickerView.dataSource = self
        
//        arrQuestions = DBHandler.getDataFromTable("SELECT * FROM question WHERE is_wrong = '1'")
//        print(arrQuestions.count)
        
        let arrQuick = DBHandler.getDataFromTable("SELECT * FROM quick_test WHERE is_wrong = '1'")
        let arrDaily = DBHandler.getDataFromTable("SELECT * FROM daily_test WHERE is_wrong = '1'")
        let arrCustom = DBHandler.getDataFromTable("SELECT * FROM customized_test WHERE is_wrong = '1'")

        for index in 0..<arrQuick!.count{

            let dicData = arrQuick?.object(at: index)  as! NSDictionary

            let que_id = dicData.value(forKey: "que_id") as! String

            let array = DBHandler.getDataFromTable("SELECT * FROM question WHERE id = '\(que_id)'")

            let dic = array?.firstObject as! NSDictionary

            let topicName = dic.value(forKey: "topicName") as! String
            let question = dic.value(forKey: "question") as! String
            let fav = dic.value(forKey: "is_fav") as! String
            let choices = dic.value(forKey: "choices") as! String
            let strExplanation = dic.value(forKey: "explanation") as! String
            let strAnswer = dic.value(forKey: "answers") as! String
            let id = dic.value(forKey: "id") as! String

            let newDic = NSMutableDictionary.init(dictionary: dicData)
            newDic.setValue(topicName, forKey: "topicName")
            newDic.setValue(question, forKey: "question")
            newDic.setValue(fav, forKey: "is_fav")
            newDic.setValue(choices, forKey: "choices")
            newDic.setValue(strExplanation, forKey: "explanation")
            newDic.setValue(strAnswer, forKey: "answers")
            newDic.setValue(id, forKey: "id")

            self.arrQuestions.add(newDic)
        }

        for index in 0..<arrDaily!.count{

            let dicData = arrDaily?.object(at: index)  as! NSDictionary

            let que_id = dicData.value(forKey: "que_id") as! String

            let array = DBHandler.getDataFromTable("SELECT * FROM question WHERE id = '\(que_id)'")

            let dic = array?.firstObject as! NSDictionary

            let topicName = dic.value(forKey: "topicName") as! String
            let question = dic.value(forKey: "question") as! String
            let fav = dic.value(forKey: "is_fav") as! String
            let choices = dic.value(forKey: "choices") as! String
            let strExplanation = dic.value(forKey: "explanation") as! String
            let strAnswer = dic.value(forKey: "answers") as! String
            let id = dic.value(forKey: "id") as! String

            let newDic = NSMutableDictionary.init(dictionary: dicData)
            newDic.setValue(topicName, forKey: "topicName")
            newDic.setValue(question, forKey: "question")
            newDic.setValue(fav, forKey: "is_fav")
            newDic.setValue(choices, forKey: "choices")
            newDic.setValue(strExplanation, forKey: "explantion")
            newDic.setValue(strAnswer, forKey: "answers")
            newDic.setValue(id, forKey: "id")

            self.arrQuestions.add(newDic)
        }

        for index in 0..<arrCustom!.count{

            let dicData = arrCustom?.object(at: index)  as! NSDictionary

            let que_id = dicData.value(forKey: "que_id") as! String

            let array = DBHandler.getDataFromTable("SELECT * FROM question WHERE id = '\(que_id)'")

            let dic = array?.firstObject as! NSDictionary

            let topicName = dic.value(forKey: "topicName") as! String
            let question = dic.value(forKey: "question") as! String
            let fav = dic.value(forKey: "is_fav") as! String
            let choices = dic.value(forKey: "choices") as! String
            let strExplanation = dic.value(forKey: "explanation") as! String
            let strAnswer = dic.value(forKey: "answers") as! String
            let id = dic.value(forKey: "id") as! String

            let newDic = NSMutableDictionary.init(dictionary: dicData)
            newDic.setValue(topicName, forKey: "topicName")
            newDic.setValue(question, forKey: "question")
            newDic.setValue(fav, forKey: "is_fav")
            newDic.setValue(choices, forKey: "choices")
            newDic.setValue(strExplanation, forKey: "explantion")
            newDic.setValue(strAnswer, forKey: "answers")
            newDic.setValue(id, forKey: "id")

            self.arrQuestions.add(newDic)
        }

        let arrTopicWrong = DBHandler.getDataFromTable("SELECT * FROM question WHERE is_wrong = '1'")
        self.arrQuestions.addObjects(from: arrTopicWrong as! [Any])
        
        print(arrQuestions.count)
        
    }

    
    //MARK: - PickerView Delegate
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
//        let total = arrQuick.count + arrDaily.count + arrCustom.count
//        return total
        return arrQuestions.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return "\(row+1)"
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        count = row + 1
        self.pickerView.reloadAllComponents()
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let pickerLabel = UILabel()
        pickerLabel.textColor = (row == pickerView.selectedRow(inComponent: component)) ? UIColor.init(red: 88/255, green: 92/255, blue: 229/255, alpha: 1.0) : UIColor.lightGray
        pickerLabel.font = UIFont(name: "Montserrat-Bold", size: 30)
        pickerLabel.backgroundColor = UIColor.clear
        pickerLabel.text = "\(row+1)"
        pickerLabel.textAlignment = NSTextAlignment.center
        return pickerLabel
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 50
    }
    
    //MARK: - IBAction
    @IBAction func btnCloseTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func tbnStartNowTapped(_ sender: Any) {
        
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextViewController = storyboard.instantiateViewController(withIdentifier: "WrongTestVC") as! WrongTestVC
        nextViewController.mainTitle = mainTitle
        let navigationController = UINavigationController(rootViewController: nextViewController)
        navigationController.isNavigationBarHidden = true
        
        if arrQuestions.count != 0{
            
            let arr = (self.arrQuestions.shuffled() as NSArray).mutableCopy() as! NSMutableArray
            let finalArray = NSMutableArray()

            for index in 0..<self.count{

                let dic = arr.object(at: index) as! NSDictionary
                finalArray.add(dic)
            }
            
            nextViewController.arrQuestions = finalArray
            nextViewController.isFromWrongQue = true
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = navigationController
        }
    }
}
