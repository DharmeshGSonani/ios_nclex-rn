//
//  QuickTestPopUpVC.swift
//  NCLEX-RN
//
//  Created by Kevin M1 on 08/03/22.
//

import UIKit

class QuickTestPopUpVC: UIViewController {

    var mainTitle = ""
    
    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    //MARK: - IBAction
    @IBAction func btnReady(_ sender: Any) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "QuickTestLoadVC") as! QuickTestLoadVC
        nextViewController.mainTitle = self.mainTitle
        let vc = UINavigationController(rootViewController: nextViewController)
        vc.isNavigationBarHidden = true
        
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle   = .crossDissolve
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func btnMayBeLater(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
