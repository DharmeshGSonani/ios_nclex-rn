//
//  CustomizedTestPopUpVC.swift
//  NCLEX-RN
//
//  Created by Kevin M1 on 08/03/22.
//

import UIKit

class CustomizedTestPopUpVC: UIViewController, TagListViewDelegate {

    //MARK: - IBOutlets
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var tagListView: TagListView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var lblMinutes: UILabel!
    @IBOutlet weak var lblQuestion: UILabel!
    @IBOutlet weak var btnStart: UIButton!
    @IBOutlet weak var slider1: UISlider!
    @IBOutlet weak var slider2: UISlider!
    
    @IBOutlet weak var lblTotalQuestion: UILabel!
    @IBOutlet weak var lblTotalMinutes: UILabel!
    
    //MARK: - Variables

    var arrQuestions = NSMutableArray()
    var arrCategory = NSMutableArray()
    var arrCatIds : [String] = []
    var arrQue = NSMutableArray()
    
    var mainTitle = ""
    
    var timer = Timer()
    var second = 00
    
    //MARK: - View LifeCycle 
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
        
        slider1.setThumbImage(UIImage(named: "ic_thumb"), for: .normal)
        slider2.setThumbImage(UIImage(named: "ic_thumb"), for: .normal)
        
        tagListView.clipsToBounds = true
        
        self.tagListView.delegate = self
        
        self.knowDeviceType()
        
        self.slider1.minimumValue = 10
        self.slider2.minimumValue = 10
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        mainView.addGestureRecognizer(tap)
        mainView.isUserInteractionEnabled = true
        
        arrQuestions = DBHandler.getDataFromTable("SELECT * FROM question")
        print(arrQuestions)
        
        let arrCategory = arrQuestions.value(forKey: "topicName") as! [String]
        self.arrCatIds = arrCategory.uniqued()
        
        for index in 0..<self.arrCatIds.count{
            
            let name = self.arrCatIds[index]
            tagListView.addTag(name).cornerRadius = 20
        }
        
        self.lblTotalMinutes.text = "\(60)"
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer){
        self.dismiss(animated: true, completion: nil)
    }
        func knowDeviceType(){
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136, 1334, 1920, 2208: // iPhone 5 or 5S or 5C
                
                tblView.isScrollEnabled = true
                break
                
            case 1792, 2340, 2436, 2532, 2688, 2778: // iPhone XR/ 11 iphone 12 mini iPhone X/XS/11 Pro iphone 12 pro iPhone XS Max/11 Pro Max iphone 12 pro max
                
                tblView.isScrollEnabled = false
                break
                
            default:
                print("Unknown")
            }
        }
    }
    
    // MARK: TagListViewDelegate
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) {
        print("Tag pressed: \(title)")
        
        tagView.isSelected = !tagView.isSelected
        
        arrQue = DBHandler.getDataFromTable("SELECT * FROM question WHERE topicName = '\(title)'")
        print(arrQue.count)
        
        tagView.backgroundColor = UIColor(red: 88/255, green: 92/255, blue: 229/255, alpha: 1.0)
        tagView.clipsToBounds = true
    }
    
    func tagRemoveButtonPressed(_ title: String, tagView: TagView, sender: TagListView) {
        print("Tag Remove pressed: \(title), \(sender)")
        sender.removeTagView(tagView)
    }
    
    //MARK: - IBAction
    @IBAction func slider1Progress(_ sender: UISlider) {
        
        let countmin = Int(Double(sender.value) * 0.6)

        let hours:Int = countmin/60

        let minutes = countmin - (hours*60)

        print("\(minutes)min")
        
        self.lblMinutes.text = "\(minutes)"
        
    }
    
    @IBAction func slider2Progress(_ sender: UISlider) {
        
        lblQuestion.text = "\(Int(sender.value))"
        
    }
    
    @IBAction func btnStartTapped(_ sender: Any) {
        
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextViewController = storyboard.instantiateViewController(withIdentifier: "ExamTestVC") as! ExamTestVC
        nextViewController.mainTitle = mainTitle
        let navigationController = UINavigationController(rootViewController: nextViewController)
        navigationController.isNavigationBarHidden = true
        
        if arrQue.count != 0{
            
            let array = (self.arrQue.shuffled() as NSArray).mutableCopy() as! NSMutableArray
            let count = (Int)(slider2.value)
            let time = Int(Double(slider1.value) * 0.6)
            let finalArray = NSMutableArray()
            
            for index in 0..<count{
                let dic = array.object(at: index) as! NSDictionary
                
                finalArray.add(dic)
            }
            
            nextViewController.arrQuestions = finalArray
            nextViewController.isFromCustomized = true
            
            UserDefaults.standard.setValue("\(time)", forKey: "selectedTime")
            UserDefaults.standard.synchronize()
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = navigationController
        } else {
            
            self.view.makeToast("Please Select Atleast one Topic.")
        }
        
    }
    
    @IBAction func btnClosedTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
