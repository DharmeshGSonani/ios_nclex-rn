//
//  DailyQuePopUpVC.swift
//  NCLEX-RN
//
//  Created by Kevin M1 on 08/03/22.
//

import UIKit

class DailyQuePopUpVC: UIViewController {

    //MARK: - Variables
    var isSelected = false
    var mainTitle = ""
    
    var isFromDailyCheckIn = false
    var isFromDailyQuestion = false
    
    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    //MARK: - IBAction
    @IBAction func btnGetStarted(_ sender: Any) {
        
        if isFromDailyQuestion == true{
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ExamTestVC") as! ExamTestVC
            nextViewController.mainTitle = mainTitle
            nextViewController.isFromDailyQuestion = true
            let navigationController = UINavigationController(rootViewController: nextViewController)
            navigationController.isNavigationBarHidden = true

            let appDelegate = UIApplication.shared.delegate as! AppDelegate
    //        appDelegate.isFromCheckIn = true
            appDelegate.window?.rootViewController = navigationController
            
        }
        else if isFromDailyCheckIn == true{
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ExamTestVC") as! ExamTestVC
            nextViewController.mainTitle = mainTitle
            nextViewController.isFromDailyCheckIn = true
            let navigationController = UINavigationController(rootViewController: nextViewController)
            navigationController.isNavigationBarHidden = true

            let appDelegate = UIApplication.shared.delegate as! AppDelegate
    //        appDelegate.isFromCheckIn = true
            appDelegate.window?.rootViewController = navigationController
            
        }
        
    }
    
    @IBAction func btnCancel(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
}
