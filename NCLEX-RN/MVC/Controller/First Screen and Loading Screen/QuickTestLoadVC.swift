//
//  QuickTestLoadVC.swift
//  NCLEX-RN
//
//  Created by Kevin M1 on 08/03/22.
//

import UIKit

class QuickTestLoadVC: UIViewController {

    //MARK: - Variables
    var mainTitle = ""
    
    //MARK: - View LifeCycle 
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ExamTestVC") as! ExamTestVC
            nextViewController.isFromQuickTest = true
            nextViewController.mainTitle = self.mainTitle
            let navigationController = UINavigationController(rootViewController: nextViewController)
            navigationController.isNavigationBarHidden = true

            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = navigationController
            
        }
    }
    
    //MARK: - IBAction
    @IBAction func btnCloseTapped(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
}
