//
//  TermsAndPolicyVC.swift
//  NCLEX-RN
//
//  Created by Kevin M1 on 11/03/22.
//

import UIKit

class TermsAndPolicyVC: UIViewController {

    //MARK: - IBOutltes
    @IBOutlet weak var viewTerms: UIView!
    @IBOutlet weak var viewPolicy: UIView!
    @IBOutlet weak var lblTerms: UILabel!
    @IBOutlet weak var lblPolicy: UILabel!
    
    //MARK: - Variables
    var isFromTerms = false
    var isFromPolicy = false
    
    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isFromTerms == true{
            viewPolicy.isHidden = true
            lblPolicy.isHidden = true
            
        } else {
            viewTerms.isHidden = true
            lblTerms.isHidden = true
            
        }
    }
    
    //MARK: - IBAction
    @IBAction func btnBackTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
}
