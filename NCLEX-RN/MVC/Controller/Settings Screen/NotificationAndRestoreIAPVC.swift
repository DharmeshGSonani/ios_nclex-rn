//
//  NotificationAndRestoreIAPVC.swift
//  NCLEX-RN
//
//  Created by Kevin M1 on 10/03/22.
//

import UIKit

protocol clickNotification{
    func didSelectSwitch(str: Bool)
}

class NotificationAndRestoreIAPVC: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblExplanation: UILabel!
    @IBOutlet weak var btnSure: UIButton!
    
    //MARK: - Variables
    var isFromNotification = false
    var isFromRestore = false
    var isFromResetAllProgress = false
    
    var delegateSwitch : clickNotification!
    
    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()

        if isFromNotification == true {
            lblTitle.text = "Turn on Notification?"
            lblExplanation.text = "Allow notification permissions on the settings to get timely practice alerts."
            
        } else if isFromRestore == true {
            
            lblTitle.text = "Would you like to restore your in-app purchases?"
            lblExplanation.text = ""
            
        } else {
            
            lblTitle.text = "Sure You Want to Reset All Progress?"
            lblExplanation.text = ""
            btnSure.setTitle("Reset Now", for: .normal)
        }

    }
    
    //MARK: - IBAction
    @IBAction func btnSureClicked(_ sender: Any) {
        
        if isFromRestore == true{
            
            InAppPurchaseManager.sharedManager.restoringPurchases()
                        
        } else if isFromNotification == true {
            self.delegateSwitch.didSelectSwitch(str: true)
            self.dismiss(animated: true, completion: nil)
            
        } else{
            
            let alert = UIAlertController(title: "Alert", message: "Are you sure you want reset all Progress?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                
                let deleteRecordSuccess = DBHandler.genericQuery("DELETE FROM read_question")
                print(deleteRecordSuccess)
                
                let updateWrongQueSuccess = DBHandler.genericQuery("UPDATE question SET is_wrong = ''")
                print(updateWrongQueSuccess)
                
                let updateReadQueSuccess = DBHandler.genericQuery("UPDATE question SET read_count = ''")
                print(updateReadQueSuccess)
                
                let updateWrongSuccess = DBHandler.genericQuery("UPDATE question SET wrong_option = ''")
                print(updateWrongSuccess)

                let updateRightSuccess = DBHandler.genericQuery("UPDATE question SET correct_option = ''")
                print(updateRightSuccess)
                
                let updatePartSuccess = DBHandler.genericQuery("UPDATE question SET part_id = ''")
                print(updatePartSuccess)
                
                let updateFavSuccess = DBHandler.genericQuery("UPDATE question SET is_fav = ''")
                print(updateFavSuccess)
                
                let deleteQuickSuccess = DBHandler.genericQuery("DELETE FROM quick_test")
                print(deleteQuickSuccess)
                
                let deleteWrongTestSuccess = DBHandler.genericQuery("DELETE FROM wrong_test")
                print(deleteWrongTestSuccess)
                
                let deleteCustomTestSuccess = DBHandler.genericQuery("DELETE FROM customized_test")
                print(deleteCustomTestSuccess)
                
                let deleteDailyTestSuccess = DBHandler.genericQuery("DELETE FROM daily_test")
                print(deleteDailyTestSuccess)
                
                
                
                UserDefaults.standard.removeObject(forKey: "daysShow")
                UserDefaults.standard.removeObject(forKey: "showProgress")
                UserDefaults.standard.removeObject(forKey: "showQuestion")
                UserDefaults.standard.synchronize()
                
                self.dismiss(animated: true, completion: nil)
                let appDelegate = UIApplication.shared.delegate as! AppDelegate

//                appDelegate.window?.rootViewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController")
            }))
            
            alert.addAction(UIAlertAction(title: "CANCEL", style: .default, handler: { action in
                self.dismiss(animated: true, completion: nil)
            }))
            
            self.present(alert, animated: true, completion: nil)
            
        }
        
    }
    
    @IBAction func btnCancelClicked(_ sender: Any) {
        
        if isFromNotification == true{
            self.delegateSwitch.didSelectSwitch(str: false)
        }
        self.dismiss(animated: true, completion: nil)
        
    }
    
}
