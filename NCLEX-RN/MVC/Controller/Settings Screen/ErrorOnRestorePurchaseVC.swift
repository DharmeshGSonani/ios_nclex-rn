//
//  ErrorOnRestorePurchaseVC.swift
//  NCLEX-RN
//
//  Created by Kevin M1 on 11/03/22.
//

import UIKit

class ErrorOnRestorePurchaseVC: UIViewController {
   
    //MARK: - IBOutlets
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblExplanation: UILabel!
    
    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    //MARK: - IBAction
    @IBAction func closeBtnTapped(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
}
