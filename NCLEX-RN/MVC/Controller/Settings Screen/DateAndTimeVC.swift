//
//  DateAndTimeVC.swift
//  NCLEX-RN
//
//  Created by Kevin M1 on 11/03/22.
//

import UIKit

protocol timeSelection{
    func didSelectTime(str:String)
}

class DateAndTimeVC: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
   
    //MARK: - IBOutltes
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var lblTime: UILabel!
    
    //MARK: - Variables
    var delegateTime : timeSelection!
    
    let arrHr : [String] = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24"]
    
    let arrMinutes : [String] = ["00","01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12","13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59"]
    
    var selectedHour : Int = 10
    var selectedMin : Int = 0

    //MARK: - IBOutltes
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    //MARK: - PickerView Delegate
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0 {
            return arrHr.count
        } else {
            return arrMinutes.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        let pickerLabel = UILabel()
        
        if component == 0 {
            
            if pickerView.selectedRow(inComponent: component) == row {
                
                pickerLabel.attributedText = NSAttributedString(string: arrHr[row], attributes: [NSAttributedString.Key.font:UIFont(name: "Montserrat-SemiBold", size: 30)!, NSAttributedString.Key.foregroundColor: UIColor(red: 88/255, green: 92/255, blue: 229/255, alpha: 1)])
            } else {
                
                pickerLabel.attributedText = NSAttributedString(string: arrHr[row], attributes: [NSAttributedString.Key.font:UIFont(name: "Montserrat-SemiBold", size: 18.0)!,NSAttributedString.Key.foregroundColor: UIColor(red: 126/255, green: 130/255, blue: 159/255, alpha: 0.5)])
                
            }
        } else {
            
            if pickerView.selectedRow(inComponent: component) == row {
            
            pickerLabel.attributedText = NSAttributedString(string: arrMinutes[row], attributes: [NSAttributedString.Key.font:UIFont(name: "Montserrat-SemiBold", size: 30)!, NSAttributedString.Key.foregroundColor: UIColor(red: 88/255, green: 92/255, blue: 229/255, alpha: 1)])
                
            } else {
                
                pickerLabel.attributedText = NSAttributedString(string: arrMinutes[row], attributes: [NSAttributedString.Key.font:UIFont(name: "Montserrat-SemiBold", size: 18.0)!,NSAttributedString.Key.foregroundColor: UIColor(red: 126/255, green: 130/255, blue: 159/255, alpha: 0.5)])
            }
        }
        pickerLabel.textAlignment = .center
        return pickerLabel
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        pickerView.reloadAllComponents()
        
        if component == 0{
            
            let hour = arrHr[row]
            let min = arrMinutes[pickerView.selectedRow(inComponent: 1)]
            
            lblTime.text = "\(hour):\(min)"
            selectedHour = (Int)(hour)!
            selectedMin = (Int)(min)!
            
        } else {
            
            let min = arrMinutes[row]
            let hour = arrHr[pickerView.selectedRow(inComponent: 0)]
            
            lblTime.text = "\(hour):\(min)"
            selectedHour = (Int)(hour)!
            selectedMin = (Int)(min)!
            
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        
        if component == 0{
            return 60
        } else {
            return 40
        }
    }
    
    //MARK: - IBAction
    
    @IBAction func btnSave(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        
        self.delegateTime.didSelectTime(str: "\(lblTime.text ?? "")")
        UserDefaults.standard.setValue("\(lblTime.text ?? "")", forKey: "selectedTimeForPush")
        UserDefaults.standard.synchronize()
        
        UserDefaults.standard.set(selectedHour, forKey: "selectedHour")
        UserDefaults.standard.synchronize()
        
        UserDefaults.standard.set(selectedMin, forKey:  "selectedMin")
        UserDefaults.standard.synchronize()
        
        let content = UNMutableNotificationContent()
        content.title = "NCLEX-RN"
        content.body = "The study time you set is up, come and learn!"
        content.sound = .default
        
        var datCamp = DateComponents()
        datCamp.hour = selectedHour
        datCamp.second = selectedMin
        let trigger = UNCalendarNotificationTrigger(dateMatching: datCamp, repeats: true)
        
        let request = UNNotificationRequest(identifier: "ID", content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request) { (error : Error?) in
            if let theError = error {
                print(theError.localizedDescription)
            }
        }
    }
    
    @IBAction func btnCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
