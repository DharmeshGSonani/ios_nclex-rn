//
//  SettingVC.swift
//  NCLEX-RN
//
//  Created by Kevin M1 on 10/03/22.
//

import UIKit
import StoreKit
import AMTabView

class SettingVC: UIViewController, timeSelection, clickNotification, TabItem{

    //MARK: - IBOutlets
    @IBOutlet weak var btnTime: UIButton!
    @IBOutlet weak var pushSwitch: UISwitch!
    
    //MARK: - Variables
    var tabImage: UIImage?{
        return UIImage(named: "ic_fourth")
    }
    
    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UserDefaults.standard.object(forKey: "selectedTimeForPush") != nil{
            
            let str = UserDefaults.standard.object(forKey: "selectedTimeForPush")
            btnTime.setTitle((str as! String), for: .normal)
        }
        else{
            btnTime.setTitle("10:00", for: .normal)
        }
        
        if UserDefaults.standard.object(forKey: "isOnNotification") != nil{
            if UserDefaults.standard.bool(forKey: "isOnNotification"){
                pushSwitch.setOn(true, animated: true)
            }
            else{
                pushSwitch.setOn(false, animated: true)
            }
        }
        else{
            pushSwitch.setOn(true, animated: true)
        }
        
    }
    
    //MARK: - Function
    func didSelectSwitch(str: Bool) {
        if str == true{
            pushSwitch.setOn(true, animated: true)
        } else {
            pushSwitch.setOn(false, animated: true)
        }
    }
    
    func didSelectTime(str: String) {
        btnTime.setTitle(str, for: .normal)
    }
    
    //MARK: - IBAction
    @IBAction func btnShareApp(_ sender: Any) {
        AppSupport.shareApp(inController: self)
    }
    
    @IBAction func btnRateApp(_ sender: Any) {
        AppSupport.rateApp()
    }
    
    @IBAction func clickOnTimeBtn(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DateAndTimeVC") as! DateAndTimeVC
        vc.modalTransitionStyle   = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        vc.hidesBottomBarWhenPushed = true
        vc.delegateTime = self
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func switchNotification(_ sender: UISwitch) {
        
        if sender.isOn{
            UserDefaults.standard.set(true, forKey: "isOnNotification")
            UserDefaults.standard.synchronize()
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.setupNotification()
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationAndRestoreIAPVC")  as! NotificationAndRestoreIAPVC
            vc.delegateSwitch = self
            vc.modalTransitionStyle   = .crossDissolve
            vc.modalPresentationStyle = .overFullScreen
            vc.hidesBottomBarWhenPushed = true
            vc.isFromNotification = true
            self.present(vc, animated: true, completion: nil)
            
        } else {
            
            UserDefaults.standard.set(false, forKey: "isOnNotification")
            UserDefaults.standard.synchronize()

            UIApplication.shared.unregisterForRemoteNotifications()
        }

    }
    
//    @IBAction func btnResetAllProgress(_ sender: Any) {
//        
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationAndRestoreIAPVC")  as! NotificationAndRestoreIAPVC
//        vc.modalTransitionStyle   = .crossDissolve
//        vc.modalPresentationStyle = .overFullScreen
//        vc.hidesBottomBarWhenPushed = true
//        vc.isFromResetAllProgress = true
//        self.present(vc, animated: true, completion: nil)
//        
//    }
//    
//    @IBAction func btnRestore(_ sender: Any) {
//        
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationAndRestoreIAPVC")  as! NotificationAndRestoreIAPVC
//        vc.modalTransitionStyle   = .crossDissolve
//        vc.modalPresentationStyle = .overFullScreen
//        vc.hidesBottomBarWhenPushed = true
//        vc.isFromRestore = true
//        self.present(vc, animated: true, completion: nil)
//        
//    }
    
    @IBAction func btnTermsOfService(_ sender: Any) {

        let url = URL(string: "https://sites.google.com/view/termlikny")!
        if UIApplication.shared.canOpenURL(url)
        {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
            //If you want handle the completion block than
            UIApplication.shared.open(url, options: [:], completionHandler: { (success) in
                print("Open url : \(success)")
            })
        }
        
    }
        
    @IBAction func btnPrivacyPolicy(_ sender: Any) {
        
        let url = URL(string: "https://sites.google.com/view/likny")!
        if UIApplication.shared.canOpenURL(url)
        {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
            //If you want handle the completion block than
            UIApplication.shared.open(url, options: [:], completionHandler: { (success) in
                print("Open url : \(success)")
            })
        }
    }
}

struct AppSupport {
    
    static let appID = "1619048479"
    
    static func rateApp(){
        if let url = URL(string:"itms-apps://itunes.apple.com/app/\(appID)") {
            AppSupport.openURL(url)
        }
    }
    
    static func openURL(_ url: URL){
        if UIApplication.shared.canOpenURL(url){
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    static let shareText = "NCLEX-RN"
    
    static func shareApp(inController controller:UIViewController){
        let textToShare = "\(AppSupport.shareText) \n itms-apps://itunes.apple.com/app/\(appID)"
        AppSupport.itemShare(inController: controller, items: textToShare)
    }
    
    static func itemShare(inController controller:UIViewController, items:Any){
        let objectsToShare = [items]
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        activityVC.popoverPresentationController?.sourceView = controller.view
        activityVC.popoverPresentationController?.sourceRect = CGRect(x: 100, y: 200, width: 300, height: 300)
        controller.present(activityVC, animated: true, completion: nil)
    }
}
