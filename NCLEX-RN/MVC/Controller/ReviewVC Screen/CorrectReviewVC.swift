//
//  CorrectReviewVC.swift
//  NCLEX-RN
//
//  Created by Kevin M1 on 10/03/22.
//

import UIKit

class CorrectReviewVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    //MARK: - IBOutlets
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var dataView: UIView!
    
    @IBOutlet weak var noDataView: UIView!
    
    var arrQuestions = NSMutableArray()
    
    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()

        tblView.delegate = self
        tblView.dataSource = self
        
        tblView.register(UINib(nibName: "ReviewTableViewCell", bundle: nil), forCellReuseIdentifier: "ReviewTableViewCell")
        
        let arrQuickCorrect = DBHandler.getDataFromTable("SELECT * FROM quick_test WHERE is_wrong = '0'")
        let arrCustomCorrect = DBHandler.getDataFromTable("SELECT * FROM customized_test WHERE is_wrong = '0'")
        let arrWrongCorrect = DBHandler.getDataFromTable("SELECT * FROM wrong_test WHERE is_wrong = '0'")
        
        for index in 0..<arrQuickCorrect!.count{
            
            let dicData = arrQuickCorrect?.object(at: index)  as! NSDictionary
            
            let que_id = dicData.value(forKey: "que_id") as! String
            
            let array = DBHandler.getDataFromTable("SELECT * FROM question WHERE id = '\(que_id)'")
            
            let dic = array?.firstObject as! NSDictionary
            
            let topicName = dic.value(forKey: "topicName") as! String
            let question = dic.value(forKey: "question") as! String
            let fav = dic.value(forKey: "is_fav") as! String

            let newDic = NSMutableDictionary.init(dictionary: dicData)
            newDic.setValue(topicName, forKey: "topicName")
            newDic.setValue(question, forKey: "question")
            newDic.setValue(fav, forKey: "is_fav")

            self.arrQuestions.add(newDic)
        }
        for index in 0..<arrCustomCorrect!.count{
            
            let dicData = arrCustomCorrect?.object(at: index)  as! NSDictionary
            
            let que_id = dicData.value(forKey: "que_id") as! String
            
            let array = DBHandler.getDataFromTable("SELECT * FROM question WHERE id = '\(que_id)'")
            
            let dic = array?.firstObject as! NSDictionary
            
            let topicName = dic.value(forKey: "topicName") as! String
            let question = dic.value(forKey: "question") as! String
            let fav = dic.value(forKey: "is_fav") as! String

            let newDic = NSMutableDictionary.init(dictionary: dicData)
            newDic.setValue(topicName, forKey: "topicName")
            newDic.setValue(question, forKey: "question")
            newDic.setValue(fav, forKey: "is_fav")

            self.arrQuestions.add(newDic)
        }
        for index in 0..<arrWrongCorrect!.count{
            
            let dicData = arrWrongCorrect?.object(at: index)  as! NSDictionary
            
            let que_id = dicData.value(forKey: "que_id") as! String
            
            let array = DBHandler.getDataFromTable("SELECT * FROM question WHERE id = '\(que_id)'")
            
            let dic = array?.firstObject as! NSDictionary
            
            let topicName = dic.value(forKey: "topicName") as! String
            let question = dic.value(forKey: "question") as! String
            let fav = dic.value(forKey: "is_fav") as! String

            let newDic = NSMutableDictionary.init(dictionary: dicData)
            newDic.setValue(topicName, forKey: "topicName")
            newDic.setValue(question, forKey: "question")
            newDic.setValue(fav, forKey: "is_fav")

            self.arrQuestions.add(newDic)
        }
        
        let arrAllCorrectQue = DBHandler.getDataFromTable("SELECT * FROM question WHERE is_wrong = '0'")
        
        self.arrQuestions.addObjects(from: arrAllCorrectQue as! [Any])

        if arrAllCorrectQue!.count > 0 || arrCustomCorrect!.count > 0 || arrWrongCorrect!.count > 0 || arrQuickCorrect!.count > 0{
            noDataView.isHidden = true
            dataView.isHidden = false
            
        } else {
            
            noDataView.isHidden = false
            dataView.isHidden = true
           
        }
    }

    //MARK: - Tableview Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrQuestions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblView.dequeueReusableCell(withIdentifier: "ReviewTableViewCell", for: indexPath) as! ReviewTableViewCell
        
        let dicData = arrQuestions.object(at: indexPath.row) as! NSDictionary
        
        if dicData.value(forKey: "is_fav") as? String == "0"{
            cell.imgView2.image = UIImage(named: "ic_favourite")
            cell.imgView2.isHidden = false
        } else {
            cell.imgView2.isHidden = true
        }
        
        cell.lblQue.text = dicData.value(forKey: "question") as? String
        cell.lblTitle.text = dicData.value(forKey: "topicName") as? String
        cell.imgView1.image = UIImage(named: "ic_mark")
 
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    
}
