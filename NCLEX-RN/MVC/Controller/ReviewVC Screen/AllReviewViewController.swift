//
//  AllReviewViewController.swift
//  NCLEX-RN
//
//  Created by Kevin M1 on 28/03/22.
//

import UIKit

class AllReviewViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
   
    //MARK: - IBOutlets
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var dataView: UIView!
    @IBOutlet weak var noDataView: UIView!
    
    var arrQuestions = NSMutableArray()
    var arrFavQue = NSMutableArray()
    
    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tblView.delegate = self
        self.tblView.dataSource = self
        tblView.register(UINib(nibName: "ReviewTableViewCell", bundle: nil), forCellReuseIdentifier: "ReviewTableViewCell")
        
        let arrQuickAll = DBHandler.getDataFromTable("SELECT * FROM quick_test WHERE is_wrong = '0' OR is_wrong = '1'")
        let arrCustomAll = DBHandler.getDataFromTable("SELECT * FROM customized_test WHERE is_wrong = '0' OR is_wrong = '1'")
        let arrWrongAll = DBHandler.getDataFromTable("SELECT * FROM wrong_test WHERE is_wrong = '0' OR is_wrong = '1'")
        
        for index in 0..<arrQuickAll!.count{
            
            let dicData = arrQuickAll?.object(at: index)  as! NSDictionary
            
            let que_id = dicData.value(forKey: "que_id") as! String
            
            let array = DBHandler.getDataFromTable("SELECT * FROM question WHERE id = '\(que_id)'")
            
            let dic = array?.firstObject as! NSDictionary
            
            let topicName = dic.value(forKey: "topicName") as! String
            let question = dic.value(forKey: "question") as! String
            let fav = dic.value(forKey: "is_fav") as! String

            let newDic = NSMutableDictionary.init(dictionary: dicData)
            newDic.setValue(topicName, forKey: "topicName")
            newDic.setValue(question, forKey: "question")
            newDic.setValue(fav, forKey: "is_fav")

            self.arrQuestions.add(newDic)
        }
        
        for index in 0..<arrCustomAll!.count{
            
            let dicData = arrQuickAll?.object(at: index)  as! NSDictionary
            
            let que_id = dicData.value(forKey: "que_id") as! String
            
            let array = DBHandler.getDataFromTable("SELECT * FROM question WHERE id = '\(que_id)'")
            
            let dic = array?.firstObject as! NSDictionary
            
            let topicName = dic.value(forKey: "topicName") as! String
            let question = dic.value(forKey: "question") as! String
            let fav = dic.value(forKey: "is_fav") as! String

            let newDic = NSMutableDictionary.init(dictionary: dicData)
            newDic.setValue(topicName, forKey: "topicName")
            newDic.setValue(question, forKey: "question")
            newDic.setValue(fav, forKey: "is_fav")

            self.arrQuestions.add(newDic)
        }
        
        for index in 0..<arrWrongAll!.count{
            
            let dicData = arrQuickAll?.object(at: index)  as! NSDictionary
            
            let que_id = dicData.value(forKey: "que_id") as! String
            
            let array = DBHandler.getDataFromTable("SELECT * FROM question WHERE id = '\(que_id)'")
            
            let dic = array?.firstObject as! NSDictionary
            
            let topicName = dic.value(forKey: "topicName") as! String
            let question = dic.value(forKey: "question") as! String
            let fav = dic.value(forKey: "is_fav") as! String

            let newDic = NSMutableDictionary.init(dictionary: dicData)
            newDic.setValue(topicName, forKey: "topicName")
            newDic.setValue(question, forKey: "question")
            newDic.setValue(fav, forKey: "is_fav")

            self.arrQuestions.add(newDic)
        }
  
        let arrAllQuestions = DBHandler.getDataFromTable("SELECT * FROM question WHERE is_wrong = '1' OR is_wrong = '0'")
        
        self.arrQuestions.addObjects(from: arrAllQuestions as! [Any])
        
//        arrAllQuestions = DBHandler.getDataFromTable("SELECT * FROM question WHERE is_wrong = '1' OR is_wrong = '0' OR is_fav = '0'")
//        print(arrAllQuestions.count)
//
        if arrAllQuestions!.count > 0 || arrCustomAll!.count > 0 || arrWrongAll!.count > 0 || arrQuickAll!.count > 0{
            
            tblView.isHidden = false
            noDataView.isHidden = true
        }
        else
        {
            tblView.isHidden = true
            noDataView.isHidden = false
        }
        
    }
    
    //MARK: - TableView Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrQuestions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblView.dequeueReusableCell(withIdentifier: "ReviewTableViewCell", for: indexPath) as! ReviewTableViewCell
        let dicData = arrQuestions.object(at: indexPath.row) as! NSDictionary
        
        if dicData.value(forKey: "is_wrong") as? String == "1"{
            cell.imgView1.image = UIImage(named: "ic_cross")
            cell.imgView2.isHidden = false
            
        } else if dicData.value(forKey: "is_wrong") as? String == "0"{
            cell.imgView1.image = UIImage(named: "ic_mark")
            cell.imgView2.isHidden = false
            
        } else {
            cell.imgView1.isHidden = true
        }
        
        if dicData.value(forKey: "is_fav") as? String == "0"{
            cell.imgView2.image = UIImage(named: "ic_favourite")
        } else {
            cell.imgView2.isHidden = true
        }
        
        cell.lblTitle.text = dicData.value(forKey: "topicName") as? String
        cell.lblQue.text = dicData.value(forKey: "question") as? String
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    
}
