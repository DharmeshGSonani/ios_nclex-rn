//
//  FlaggedReviewVC.swift
//  NCLEX-RN
//
//  Created by Kevin M1 on 10/03/22.
//

import UIKit

class FlaggedReviewVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //MARK: - IBOutlets
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var dataView: UIView!
    
    @IBOutlet weak var noDataView: UIView!
    
    var currentQuestionIndex : Int = 0
    var arrFavQue = NSMutableArray()
    
    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()

        tblView.delegate = self
        tblView.dataSource = self
        
        tblView.register(UINib(nibName: "ReviewTableViewCell", bundle: nil), forCellReuseIdentifier: "ReviewTableViewCell")
        
        arrFavQue = DBHandler.getDataFromTable("SELECT * FROM question WHERE is_fav = '0'")
        print(arrFavQue.count)
        
        if arrFavQue.count == 0{
            noDataView.isHidden = false
            dataView.isHidden = true

        } else {
            noDataView.isHidden = true
            dataView.isHidden = false

        }
    }
    
    //MARK: - TableView Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrFavQue.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblView.dequeueReusableCell(withIdentifier: "ReviewTableViewCell", for: indexPath) as! ReviewTableViewCell
        
        cell.imgView1.isHidden = true
        
        let dicData = arrFavQue.object(at: indexPath.row) as! NSDictionary
        
        cell.lblTitle.text = dicData.value(forKey: "topicName") as? String
        
        cell.lblQue.text = dicData.value(forKey: "question") as? String
       
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    
}
