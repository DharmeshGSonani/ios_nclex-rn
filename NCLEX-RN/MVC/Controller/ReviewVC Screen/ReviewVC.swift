//
//  ReviewVC.swift
//  NCLEX-RN
//
//  Created by Kevin M1 on 10/03/22.
//

import UIKit
import AMTabView

class ReviewVC: UIViewController, TabItem {
    
    //MARK: - IBOutlets
    @IBOutlet weak var lblAll: UILabel!
    @IBOutlet weak var lblAllCount: UILabel!
    
    @IBOutlet weak var lblCorrect: UILabel!
    @IBOutlet weak var lblCorrectCount: UILabel!
    
    @IBOutlet weak var lblInCorrect: UILabel!
    @IBOutlet weak var lblInCorrectCount: UILabel!
    
    @IBOutlet weak var lblFlagged: UILabel!
    @IBOutlet weak var lblFlaggedCount: UILabel!
    
    @IBOutlet weak var dataView: UIView!
    @IBOutlet weak var imgReview: UIImageView!
    
    @IBOutlet weak var noDataView: UIView!
    
    //MARK: - Variables
    var tabImage: UIImage?{
        return UIImage(named: "ic_third")
    }
    
    var arrAllQuestions = NSMutableArray()
    var arrCorrectQue = NSMutableArray()
    var arrInCorrectQue = NSMutableArray()
    var arrFavQue = NSMutableArray()
    
    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var totalCount : Int = 0
        var totalCorrectCount : Int = 0
        var totalInCorrectCount : Int = 0
        
        // all count
        
        let arrQuickAll = DBHandler.getDataFromTable("SELECT * FROM quick_test WHERE is_wrong = '0' OR is_wrong = '1'")
        totalCount = totalCount + arrQuickAll!.count
        
        let arrCustomAll = DBHandler.getDataFromTable("SELECT * FROM customized_test WHERE is_wrong = '0' OR is_wrong = '1'")
        totalCount = totalCount + arrCustomAll!.count
        
        let arrWrongAll = DBHandler.getDataFromTable("SELECT * FROM wrong_test WHERE is_wrong = '0' OR is_wrong = '1'")
        totalCount = totalCount + arrWrongAll!.count
  
        arrAllQuestions = DBHandler.getDataFromTable("SELECT * FROM question WHERE is_wrong = '1' OR is_wrong = '0'")
        totalCount = totalCount + arrAllQuestions.count

        self.lblAllCount.text = "\(totalCount)"
        
        // correct count
        
        let arrQuickCorrect = DBHandler.getDataFromTable("SELECT * FROM quick_test WHERE is_wrong = '0'")
        totalCorrectCount = totalCorrectCount + arrQuickCorrect!.count
        
        let arrCustomCorrect = DBHandler.getDataFromTable("SELECT * FROM customized_test WHERE is_wrong = '0'")
        totalCorrectCount = totalCorrectCount + arrCustomCorrect!.count
        
        let arrWrongCorrect = DBHandler.getDataFromTable("SELECT * FROM wrong_test WHERE is_wrong = '0'")
        totalCorrectCount = totalCorrectCount + arrWrongCorrect!.count
        
        arrCorrectQue = DBHandler.getDataFromTable("SELECT * FROM question WHERE is_wrong = '0'")
        totalCorrectCount = totalCorrectCount + arrCorrectQue.count

        self.lblCorrectCount.text = "\(totalCorrectCount)"
        
        // Incorrect count
        
        let arrQuickInCorrect = DBHandler.getDataFromTable("SELECT * FROM quick_test WHERE is_wrong = '1'")
        totalInCorrectCount = totalInCorrectCount + arrQuickInCorrect!.count
        
        let arrCustomInCorrect = DBHandler.getDataFromTable("SELECT * FROM customized_test WHERE is_wrong = '1'")
        totalInCorrectCount = totalInCorrectCount + arrCustomInCorrect!.count
        
        let arrWrongInCorrect = DBHandler.getDataFromTable("SELECT * FROM wrong_test WHERE is_wrong = '1'")
        totalInCorrectCount = totalInCorrectCount + arrWrongInCorrect!.count
        
        arrInCorrectQue = DBHandler.getDataFromTable("SELECT * FROM question WHERE is_wrong = '1'")
        totalInCorrectCount = totalInCorrectCount + arrInCorrectQue.count

        self.lblInCorrectCount.text = "\(totalInCorrectCount)"
        
        arrFavQue = DBHandler.getDataFromTable("SELECT * FROM question WHERE is_fav = '0' ")
        let count3 = String(arrFavQue.count)
        self.lblFlaggedCount.text = count3
        
        self.lblAll.textColor = UIColor(named: "blue")
        self.lblAllCount.textColor = UIColor(named: "blue")
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "AllReviewViewController") as! AllReviewViewController
        
        for subview in self.dataView.subviews{

            subview.removeFromSuperview()
        }

        imgReview.image = UIImage(named: "ic_allReview")
        vc.view.frame = CGRect.init(x: 0,y: 0, width: self.dataView.frame.size.width, height: self.dataView.frame.size.height)
        
        self.dataView.addSubview(vc.view)
        self.addChild(vc)
        vc.didMove(toParent: self)
    }

    //MARK: - IBAction
    @IBAction func btnAllReview(_ sender: Any) {
        
        self.lblAll.textColor = UIColor(named: "blue")
        self.lblAllCount.textColor = UIColor(named: "blue")
        
        self.lblCorrect.textColor = .white
        self.lblCorrectCount.textColor = .white
        self.lblInCorrect.textColor = .white
        self.lblInCorrectCount.textColor = .white
        self.lblFlagged.textColor = .white
        self.lblFlaggedCount.textColor = .white
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "AllReviewViewController") as! AllReviewViewController
        
        for subview in self.dataView.subviews{

            subview.removeFromSuperview()
        }

        imgReview.image = UIImage(named: "ic_allReview")
        vc.view.frame = CGRect.init(x: 0,y: 0, width: self.dataView.frame.size.width, height: self.dataView.frame.size.height)
        
        self.dataView.addSubview(vc.view)
        self.addChild(vc)
        vc.didMove(toParent: self)
    }
    
    @IBAction func btnCorrectReview(_ sender: Any) {
        
        self.lblCorrect.textColor = UIColor(named: "blue")
        self.lblCorrectCount.textColor = UIColor(named: "blue")
        
        self.lblAll.textColor = .white
        self.lblAllCount.textColor = .white
        self.lblInCorrect.textColor = .white
        self.lblInCorrectCount.textColor = .white
        self.lblFlagged.textColor = .white
        self.lblFlaggedCount.textColor = .white
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "CorrectReviewVC") as! CorrectReviewVC
        
        for subview in self.dataView.subviews{

            subview.removeFromSuperview()
        }

        imgReview.image = UIImage(named: "ic_correctReview")
        vc.view.frame = CGRect.init(x: 0,y: 0, width: self.dataView.frame.size.width, height: self.dataView.frame.size.height)
        
        self.dataView.addSubview(vc.view)
        self.addChild(vc)
        vc.didMove(toParent: self)
        
    }
    
    @IBAction func btnInCorrectReview(_ sender: Any) {
        
        self.lblInCorrect.textColor = UIColor(named: "blue")
        self.lblInCorrectCount.textColor = UIColor(named: "blue")
        
        self.lblAll.textColor = .white
        self.lblAllCount.textColor = .white
        self.lblCorrect.textColor = .white
        self.lblCorrectCount.textColor = .white
        self.lblFlagged.textColor = .white
        self.lblFlaggedCount.textColor = .white
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "InCorrectReviewVC") as! InCorrectReviewVC
        
        for subview in self.dataView.subviews{

            subview.removeFromSuperview()
        }

        imgReview.image = UIImage(named: "ic_inCorrectReview")
        vc.view.frame = CGRect.init(x: 0,y: 0, width: self.dataView.frame.size.width, height: self.dataView.frame.size.height)
        
        self.dataView.addSubview(vc.view)
        self.addChild(vc)
        vc.didMove(toParent: self)
        
    }
    
    @IBAction func btnFlaggedReview(_ sender: Any) {
        
        self.lblFlagged.textColor = UIColor(named: "blue")
        self.lblFlaggedCount.textColor = UIColor(named: "blue")
        
        self.lblAll.textColor = .white
        self.lblAllCount.textColor = .white
        self.lblCorrect.textColor = .white
        self.lblCorrectCount.textColor = .white
        self.lblInCorrect.textColor = .white
        self.lblInCorrectCount.textColor = .white
                
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "FlaggedReviewVC") as! FlaggedReviewVC
        
        for subview in self.dataView.subviews{
            
            subview.removeFromSuperview()
        }

        imgReview.image = UIImage(named: "ic_flaggedReview")
        vc.view.frame = CGRect.init(x: 0, y: 0, width: self.dataView.frame.size.width, height: self.dataView.frame.size.height)

        self.dataView.addSubview(vc.view)
        self.addChild(vc)
        vc.didMove(toParent: self)
        
    }
}
