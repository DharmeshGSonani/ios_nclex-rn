//
//  InCorrectReviewVC.swift
//  NCLEX-RN
//
//  Created by Kevin M1 on 10/03/22.
//

import UIKit

class InCorrectReviewVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //MARK: - IBOutlets
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var dataView: UIView!
    
    @IBOutlet weak var noDataView: UIView!
    
    var currentQuestionIndex : Int = 0
    var arrQuestions = NSMutableArray()
    var arrFavQuestion = NSMutableArray()
    
    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblView.delegate = self
        tblView.dataSource = self
        
        tblView.register(UINib(nibName: "ReviewTableViewCell", bundle: nil), forCellReuseIdentifier: "ReviewTableViewCell")
        
        let arrQuickInCorrect = DBHandler.getDataFromTable("SELECT * FROM quick_test WHERE is_wrong = '1'")
        let arrCustomInCorrect = DBHandler.getDataFromTable("SELECT * FROM customized_test WHERE is_wrong = '1'")
        let arrWrongInCorrect = DBHandler.getDataFromTable("SELECT * FROM wrong_test WHERE is_wrong = '1'")
        
        for index in 0..<arrQuickInCorrect!.count{
            
            let dicData = arrQuickInCorrect?.object(at: index)  as! NSDictionary
            
            let que_id = dicData.value(forKey: "que_id") as! String
            
            let array = DBHandler.getDataFromTable("SELECT * FROM question WHERE id = '\(que_id)'")
            
            let dic = array?.firstObject as! NSDictionary
            
            let topicName = dic.value(forKey: "topicName") as! String
            let question = dic.value(forKey: "question") as! String
            let fav = dic.value(forKey: "is_fav") as! String
            
            let newDic = NSMutableDictionary.init(dictionary: dicData)
            newDic.setValue(topicName, forKey: "topicName")
            newDic.setValue(question, forKey: "question")
            newDic.setValue(fav, forKey: "is_fav")
            
            self.arrQuestions.add(newDic)
        }
        for index in 0..<arrCustomInCorrect!.count{
            
            let dicData = arrCustomInCorrect?.object(at: index)  as! NSDictionary
            
            let que_id = dicData.value(forKey: "que_id") as! String
            
            let array = DBHandler.getDataFromTable("SELECT * FROM question WHERE id = '\(que_id)'")
            
            let dic = array?.firstObject as! NSDictionary
            
            let topicName = dic.value(forKey: "topicName") as! String
            let question = dic.value(forKey: "question") as! String
            let fav = dic.value(forKey: "is_fav") as! String
            
            let newDic = NSMutableDictionary.init(dictionary: dicData)
            newDic.setValue(topicName, forKey: "topicName")
            newDic.setValue(question, forKey: "question")
            newDic.setValue(fav, forKey: "is_fav")
            
            self.arrQuestions.add(newDic)
        }
        for index in 0..<arrWrongInCorrect!.count{
            
            let dicData = arrWrongInCorrect?.object(at: index)  as! NSDictionary
            
            let que_id = dicData.value(forKey: "que_id") as! String
            
            let array = DBHandler.getDataFromTable("SELECT * FROM question WHERE id = '\(que_id)'")
            
            let dic = array?.firstObject as! NSDictionary
            
            let topicName = dic.value(forKey: "topicName") as! String
            let question = dic.value(forKey: "question") as! String
            let fav = dic.value(forKey: "is_fav") as! String
            
            let newDic = NSMutableDictionary.init(dictionary: dicData)
            newDic.setValue(topicName, forKey: "topicName")
            newDic.setValue(question, forKey: "question")
            newDic.setValue(fav, forKey: "is_fav")
            
            self.arrQuestions.add(newDic)
        }
        
        let arrAllInCorrectQue = DBHandler.getDataFromTable("SELECT * FROM question WHERE is_wrong = '1'")
        self.arrQuestions.addObjects(from: arrAllInCorrectQue as! [Any])
        
        if arrAllInCorrectQue!.count > 0 || arrQuickInCorrect!.count > 0 || arrWrongInCorrect!.count > 0 || arrCustomInCorrect!.count > 0{
            
            noDataView.isHidden = true
            dataView.isHidden = false
            
        } else {
            
            noDataView.isHidden = false
            dataView.isHidden = true
            
        }
    }
    
    //MARK: - Tableview Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrQuestions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblView.dequeueReusableCell(withIdentifier: "ReviewTableViewCell", for: indexPath) as! ReviewTableViewCell
        
        let dicData = arrQuestions.object(at: indexPath.row) as! NSDictionary
        
        if dicData.value(forKey: "is_fav") as? String == "0"{
            cell.imgView2.image = UIImage(named: "ic_favourite")
            cell.imgView2.isHidden = false
        } else {
            cell.imgView2.isHidden = true
        }
        
        cell.lblTitle.text = dicData.value(forKey: "topicName") as? String
        cell.lblQue.text = dicData.value(forKey: "question") as? String
        cell.imgView1.image = UIImage(named: "ic_cross")
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    
}
