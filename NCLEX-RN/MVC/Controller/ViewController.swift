//
//  ViewController.swift
//  NCLEX-RN
//
//  Created by Kevin M1 on 28/03/22.
//

import UIKit
import AMTabView

class ViewController: AMTabsViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        setTabsControllers()

        selectedTabIndex = 0
        
    }
    private func setTabsControllers() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let FirstVC = storyboard.instantiateViewController(withIdentifier: "FirstVC")
        let SecondVC = storyboard.instantiateViewController(withIdentifier: "SwipeQueVC")
        let ThirdVC = storyboard.instantiateViewController(withIdentifier: "ReviewVC")
        let FourthVC = storyboard.instantiateViewController(withIdentifier: "SettingVC")
        
        viewControllers = [
            FirstVC,
            SecondVC,
            ThirdVC,
            FourthVC
        ]
    }
    
    override func tabDidSelectAt(index: Int) {
      super.tabDidSelectAt(index: index)
      
    }

}
