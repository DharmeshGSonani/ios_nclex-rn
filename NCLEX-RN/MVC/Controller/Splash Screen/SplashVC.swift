//
//  SplashVC.swift
//  NCLEX-RN
//
//  Created by Kevin M1 on 04/03/22.
//

import UIKit
import StoreKit
import AMTabView

class SplashVC: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var lblTitle: UILabel!
    
    //MARK: - Variables
    var products: [SKProduct] = []
    var exDate : Date = Date()
    
    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let attributedString1 = NSMutableAttributedString(string: "NCLEX-", attributes: [
            NSAttributedString.Key.font: UIFont(name: "Montserrat-Bold", size: 30)!,
            .foregroundColor: UIColor(named: "textColor") as Any])
        
        let attributedString2 = NSMutableAttributedString(string: "RN", attributes: [
            NSAttributedString.Key.font: UIFont(name: "Montserrat-Bold", size: 30)!,
            .foregroundColor: UIColor(named: "lightBlue") as Any])
        
        let priceNSMutableAttributedString = NSMutableAttributedString()
        priceNSMutableAttributedString.append(attributedString1)
        priceNSMutableAttributedString.append(attributedString2)
        
        self.lblTitle.attributedText = priceNSMutableAttributedString
        
        self.receiptValidation()
    }

    //MARK: - In AppPurchase ReceiptValidation
    
    func receiptValidation() {
        let receiptFileURL = Bundle.main.appStoreReceiptURL
        let receiptData = try? Data(contentsOf: receiptFileURL!)
        let recieptString = receiptData?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue:0))
        if recieptString != nil
        {
            let jsonDict: [String: AnyObject] = ["receipt-data" : recieptString! as AnyObject, "password" :secretKey as AnyObject]
            
            do {
                let requestData = try JSONSerialization.data(withJSONObject: jsonDict, options:JSONSerialization.WritingOptions.prettyPrinted)
                let storeURL = URL(string: verifyReceiptURL)!
                var storeRequest = URLRequest(url: storeURL)
                storeRequest.httpMethod = "POST"
                storeRequest.httpBody = requestData
                
                let session = URLSession(configuration: URLSessionConfiguration.default)
                let task = session.dataTask(with: storeRequest, completionHandler: { [weak self] (data,response, error) in
                    do {
                        if data != nil
                        {
                            let jsonResponse = try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.mutableContainers)
                            if let datee = self?.getExpirationDateFromResponse(jsonResponse as! NSDictionary) {
                                print("=======>>>>>>",datee)
                                print("=======>>>>>>",Date())
                                
                                self?.exDate = datee
                                DispatchQueue.main.async {
                                    self?.setvc()
                                }
                            }
                            else
                            {
                                DispatchQueue.main.async {
                                    self?.setvc()
                                }
                            }
                        }
                        else
                        {
                            DispatchQueue.main.async {
                                self?.setvc()
                            }
                        }
                    } catch let parseError {
                        print(parseError)
                        DispatchQueue.main.async {
                            self?.setvc()
                        }
                    }
                })
                task.resume()
            } catch let parseError {
                print(parseError)
                self.setvc()
            }
        }
        else
        {
            self.setvc()
        }
    }
    
    func getExpirationDateFromResponse(_ jsonResponse: NSDictionary) -> Date? {
        print(jsonResponse)
        if let receiptInfo: NSArray = jsonResponse["latest_receipt_info"] as? NSArray {
            
            let lastReceipt = receiptInfo.lastObject as! NSDictionary
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss VV"
            
            if let expiresDate = lastReceipt["expires_date"] as? String {
                return formatter.date(from: expiresDate)
            }
            
            return nil
        }
        else {
            return nil
        }
    }
    
    func setvc()
    {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss VV"
        let date1 : String = formatter.string(from: Date())
        let dt : Date = formatter.date(from: date1)!
        switch exDate.compare(dt)
        {
        case .orderedAscending     :
            print("\(exDate) < \(dt)")
            isProductPurchased = false
        case .orderedDescending    :
            print("\(exDate) > \(dt)")
            isProductPurchased = true
            break
        case .orderedSame          :
            print("Date()")
            isProductPurchased = false
        }
                
        isProductPurchased = false
        
        if isProductPurchased == true
        {
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! AMTabsViewController
            let navigationController = UINavigationController(rootViewController: nextViewController)
            navigationController.isNavigationBarHidden = true
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = navigationController
        }
        
        else
        {
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "WelcomeScreenVC") as! WelcomeScreenVC
            let navigationController = UINavigationController(rootViewController: nextViewController)
            navigationController.isNavigationBarHidden = true
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = navigationController
        }
    }
}
