//
//  WelcomeScreenVC.swift
//  NCLEX-RN
//
//  Created by Kevin M1 on 05/03/22.
//

import UIKit

protocol WelcomeScreenDelegate: class{
    func changeProgress(numberView : Int)
}

class WelcomeScreenVC: UIViewController, WelcomeScreenDelegate
{
    //MARK: - IBOutlets
    @IBOutlet weak var viewIntro: Hola!
    @IBOutlet weak var viewIntro1: UIView!
    @IBOutlet weak var viewIntro2: UIView!
    @IBOutlet weak var viewIntro3: UIView!
    @IBOutlet weak var viewIntro4: UIView!
    @IBOutlet weak var introImgView: UIImageView!
    @IBOutlet weak var imgPageControl: UIImageView!
    
    @IBOutlet weak var viewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var buttonContinueTop: NSLayoutConstraint!
    @IBOutlet weak var buttonContinueBottom: NSLayoutConstraint!
    
    @IBOutlet weak var view1IntroImgTop: NSLayoutConstraint!
    @IBOutlet weak var view1TitleTop: NSLayoutConstraint!
    @IBOutlet weak var view1SubTitleTop: NSLayoutConstraint!
    
    @IBOutlet weak var view2IntroImgTop: NSLayoutConstraint!
    @IBOutlet weak var view2TitleTop: NSLayoutConstraint!
    @IBOutlet weak var view2SubTitleTop: NSLayoutConstraint!
    
    @IBOutlet weak var view3IntroImgTop: NSLayoutConstraint!
    @IBOutlet weak var view3TitleTop: NSLayoutConstraint!
    @IBOutlet weak var view3SubTitleTop: NSLayoutConstraint!
    
    @IBOutlet weak var view4IntroImgTop: NSLayoutConstraint!
    @IBOutlet weak var view4TitleTop: NSLayoutConstraint!
    @IBOutlet weak var view4SubTitleTop: NSLayoutConstraint!
    
    var viewArray : [UIView]?

    var arrIntroImg = ["ic_intro1", "ic_intro2", "ic_intro3", "ic_intro4"]
    var arrTitle = ["Highly Acclaimed Test Prep App", "Save Your Time and Save Money", "Study Anytime and Anywhere", "Highly Acclaimed Test Prep App"]
    var arrSubtitle = ["Thousands of people come here every month hoping to achieve their dreams, and we do everything we can to help them reach their goals every time!", "Boost your study efficiency and make you productive after every practice session to help you get a higher score on the final exam!", "Whether you're on the subway, on the bus, or in the mall, you can complete a professional practice session in just a few minutes.","Thousands of people come here every month hoping to achieve their dreams, and we do everything we can to help them reach their goals every time!"]
    
    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupSlider()
        self.knowDeviceType()

    }
    
    func knowDeviceType(){
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136, 1334, 1920, 2208: // iPhone 5 or 5S or 5C
                
                viewBottomConstraint.constant = 10
                view1IntroImgTop.constant = 20
                view1TitleTop.constant = 10
                view1SubTitleTop.constant = 0
                
                view2IntroImgTop.constant = 20
                view2TitleTop.constant = 10
                view2SubTitleTop.constant = 0
                
                view3IntroImgTop.constant = 20
                view3TitleTop.constant = 10
                view3SubTitleTop.constant = 0
                
                view4IntroImgTop.constant = 20
                view4TitleTop.constant = 10
                view4SubTitleTop.constant = 0
                
                buttonContinueTop.constant = 0
                buttonContinueBottom.constant = 65
                
                break
                
            case 1792, 2340, 2436, 2532, 2688, 2778: // iPhone XR/ 11 iphone 12 mini iPhone X/XS/11 Pro iphone 12 pro iPhone XS Max/11 Pro Max iphone 12 pro max
                
                viewBottomConstraint.constant = 20
                view1IntroImgTop.constant = 60
                view1TitleTop.constant = 25
                view1SubTitleTop.constant = 15
                
                view2IntroImgTop.constant = 60
                view2TitleTop.constant = 25
                view2SubTitleTop.constant = 15
                
                view3IntroImgTop.constant = 60
                view3TitleTop.constant = 25
                view3SubTitleTop.constant = 15
                
                view4IntroImgTop.constant = 60
                view4TitleTop.constant = 25
                view4SubTitleTop.constant = 15
                
                buttonContinueTop.constant = 0
                buttonContinueBottom.constant = -25
                
                break
                
            default:
                print("Unknown")
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.isHidden = true
    }

    //MARK: - PageController
    
    func changeProgress(numberView: Int) {
        let imageArray = ["ic_page1","ic_page2","ic_page3","ic_page4"]
        imgPageControl.image = UIImage(named: imageArray[numberView])
    }
    
    func setupSlider(){

        DispatchQueue.main.async {
            self.viewIntro1.frame = CGRect(x: self.viewIntro.bounds.origin.x, y: self.viewIntro.bounds.origin.y, width: self.view.bounds.width, height: self.viewIntro.bounds.height)
            self.viewIntro2.frame = CGRect(x: self.viewIntro.bounds.origin.x, y: self.viewIntro.bounds.origin.y, width: self.view.bounds.width, height: self.viewIntro.bounds.height)
            self.viewIntro3.frame = CGRect(x: self.viewIntro.bounds.origin.x, y: self.viewIntro.bounds.origin.y, width: self.view.bounds.width, height: self.viewIntro.bounds.height)
            self.viewIntro4.frame = CGRect(x: self.viewIntro.bounds.origin.x, y: self.viewIntro.bounds.origin.y, width: self.view.bounds.width, height: self.viewIntro.bounds.height)

            self.viewArray = [self.viewIntro1, self.viewIntro2, self.viewIntro3, self.viewIntro4]

            self.viewIntro.prepareViewsForStoryboard(viewArray: self.viewArray!, rotation: .horizontal)

            self.viewIntro.pageControl.isHidden = true
            self.viewIntro.delegate = self
        }

    }
    
    //MARK: - IBAction
    @IBAction func btnContinueTapped(_ sender: Any) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "SubscriptionVC") as! SubscriptionVC
        let navigationController = UINavigationController(rootViewController: nextViewController)
        navigationController.isNavigationBarHidden = true
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = navigationController
        
    }
}

