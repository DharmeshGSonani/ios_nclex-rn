//
//  SubscriptionVC.swift
//  NCLEX-RN
//
//  Created by Kevin M1 on 05/03/22.
//

import UIKit
import SwiftUI
import StoreKit
import SVProgressHUD
import AMTabView

class SubscriptionVC: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var btnWeek: UIButton!
    @IBOutlet weak var btnLifeTime: UIButton!
    @IBOutlet weak var imgView1: UIImageView!
    @IBOutlet weak var imgView2: UIImageView!
    @IBOutlet weak var lblStrike: UILabel!
    @IBOutlet weak var lblWeekPrice: UILabel!
    @IBOutlet weak var lblLifeTimePrice: UILabel!
    @IBOutlet weak var lblEveryweek: UILabel!
    @IBOutlet weak var imgDiscount: UIImageView!
    
    //MARK: - Variables
    var product_id: String?
    var products: [SKProduct] = []
    var selectedIDX : Int = Int()
    var isfail : Bool = false
    
    var isSubSelected = false
    
    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnWeek.setBackgroundImage(UIImage(named: "ic_selected"), for: .normal)
        imgView1.image = UIImage(named: "ic_check")
        
        let attributeString: NSMutableAttributedString = NSMutableAttributedString(string: "$10.99")
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 1, range: NSRange(location: 0, length: attributeString.length))
        
        lblStrike.attributedText = attributeString
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handlePurchaseNotification(_:)),
                                               name: .IAPHelperPurchaseNotification,
                                               object: nil)
       
        selectedIDX = 1
        
        self.displayProductPrice()
        
//        self.knowDeviceType()
    }
    
    func displayProductPrice()
    {
        let products = InAppPurchaseManager.sharedManager.availableProducts()

        let locale = Locale.current // currency symbol from current location
        let currencySymbol = locale.currencySymbol!

        if products != nil{
            
            let product2 = products?.first
            let price2 = product2?.price as! NSDecimalNumber
            self.lblLifeTimePrice.text = "\(currencySymbol)\(price2)"
            
            let product1 = products?.last
            let price1 = product1?.price as! NSDecimalNumber
            self.lblWeekPrice.text = "\(currencySymbol)\(price1)"
            
            self.lblEveryweek.text = "Billed \(currencySymbol)\(price1) every week"
        }
    }
    
    func buyProduct()
    {
        SVProgressHUD.show()
      
        guard let product = InAppPurchaseManager.sharedManager.availableProducts()?[selectedIDX] else {
            return
        }
        InAppPurchaseManager.sharedManager.purchaseProduct(product) { (receipts, error) in

            if let error = error {
                print(String(describing: error))
            } else if let receipts = receipts {
                print("Congrats! Here are you receipts for your purchases: \(receipts)")
            }
        }
    }

    @objc func handlePurchaseNotification(_ notification: Notification)
    {
        isfail = true
        if let identifier : String = notification.object as? String
        {
            if identifier.prefix(4) == "Fail"
            {
                // failed
                SVProgressHUD.show()
//                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "IAPFailedVC") as! IAPFailedVC
//                let vc = UINavigationController(rootViewController: nextViewController)
//                vc.isNavigationBarHidden = true
//                vc.modalPresentationStyle = .overFullScreen
//                vc.modalTransitionStyle   = .crossDissolve
//                self.present(vc, animated: true, completion: nil)
                
            }
            else
            {
                isProductPurchased = true
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! AMTabsViewController
                let navigationController = UINavigationController(rootViewController: nextViewController)
                navigationController.isNavigationBarHidden = true

                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = navigationController
            }
        }
    }
    
    func knowDeviceType(){
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136, 1334, 1920, 2208: // iPhone 5 or 5S or 5C
                
                tblView.isScrollEnabled = true
                break
                
            case 1792, 2340, 2436, 2532, 2688, 2778: // iPhone XR/ 11 iphone 12 mini iPhone X/XS/11 Pro iphone 12 pro iPhone XS Max/11 Pro Max iphone 12 pro max
                
                tblView.isScrollEnabled = false
                break
                
            default:
                print("Unknown")
            }
        }
    }
    
    //MARK: - IBAction
    @IBAction func btnSubcriptionWeekClicked(_ sender: Any) {
        
        btnWeek.setBackgroundImage(UIImage(named: "ic_selected"), for: .normal)
        btnLifeTime.setBackgroundImage(nil, for: .normal)
        
        imgView1.image = UIImage(named: "ic_check")
        
        imgView2.image = UIImage(named: "ic_uncheck")
        imgDiscount.layer.opacity = 1.0
        
        isSubSelected = true
        
        selectedIDX = 1
        
        let products = InAppPurchaseManager.sharedManager.availableProducts()

        let locale = Locale.current // currency symbol from current location
        let currencySymbol = locale.currencySymbol!

        if products != nil{
            
            let product2 = products?.first
            let price2 = product2?.price as! NSDecimalNumber
            self.lblLifeTimePrice.text = "\(currencySymbol)\(price2)"
            
            let product1 = products?.last
            let price1 = product1?.price as! NSDecimalNumber
            self.lblWeekPrice.text = "\(currencySymbol)\(price1)"
            self.lblEveryweek.text = "Billed \(currencySymbol)\(price1) every week"
        }
        
    }
    
    @IBAction func btnSubcriptionLifeTimeClicked(_ sender: Any) {
        
        btnLifeTime.setBackgroundImage(UIImage(named: "ic_selected"), for: .normal)
        btnWeek.setBackgroundImage(nil, for: .normal)
       
        self.imgView2.image = UIImage(named: "ic_check")
        
        self.imgView1.image = UIImage(named: "ic_uncheck")
        self.imgDiscount.layer.opacity = 0.8
        isSubSelected = false
        
        selectedIDX = 0
        
        let products = InAppPurchaseManager.sharedManager.availableProducts()

        let locale = Locale.current // currency symbol from current location
        let currencySymbol = locale.currencySymbol!

        if products != nil{
            
            let product2 = products?.first
            let price2 = product2?.price as! NSDecimalNumber
            self.lblLifeTimePrice.text = "\(currencySymbol)\(price2)"
            self.lblEveryweek.text = "Billed \(currencySymbol)\(price2) Lifetime"
            
            let product1 = products?.last
            let price1 = product1?.price as! NSDecimalNumber
            self.lblWeekPrice.text = "\(currencySymbol)\(price1)"
        }
            
    }
    
    @IBAction func btnTermsTapped(_ sender: Any) {

        let url = URL(string: "https://sites.google.com/view/termlikny")!
        if UIApplication.shared.canOpenURL(url)
        {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
            //If you want handle the completion block than
            UIApplication.shared.open(url, options: [:], completionHandler: { (success) in
                print("Open url : \(success)")
            })
        }
        
    }
    
    @IBAction func btnPolicyTapped(_ sender: Any) {

        let url = URL(string: "https://sites.google.com/view/likny")!
        if UIApplication.shared.canOpenURL(url)
        {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
            //If you want handle the completion block than
            UIApplication.shared.open(url, options: [:], completionHandler: { (success) in
                print("Open url : \(success)")
            })
        }
        
    }
    
    @IBAction func bttnRestoreClicked(_ sender: Any) {
        InAppPurchaseManager.sharedManager.restoringPurchases()
    }
    
    @IBAction func btnCancelClicked(_ sender: Any) {
        
        if self.presentingViewController != nil {
            
            self.dismiss(animated: true, completion: nil)
        }
        else{
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! AMTabsViewController
            let navigationController = UINavigationController(rootViewController: nextViewController)
            navigationController.isNavigationBarHidden = true

            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = navigationController
        }
        
    }
    
    @IBAction func btnContinueClicked(_ sender: Any) {
        
        SVProgressHUD.show()

        self.buyProduct()
    }
}

