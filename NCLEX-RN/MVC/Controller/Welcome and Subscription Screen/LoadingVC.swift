//
//  LoadingVC.swift
//  NCLEX-RN
//
//  Created by Kevin M1 on 07/03/22.
//

import UIKit
import StoreKit

class LoadingVC: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblInstruction: UILabel!
    
    //MARK: - Variables
    var product_id: String?
    var products: [SKProduct] = []
    var selectedIDX : Int = Int()
    var isfail : Bool = false

    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }

}
