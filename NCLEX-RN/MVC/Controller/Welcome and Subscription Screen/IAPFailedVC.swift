//
//  IAPFailedVC.swift
//  NCLEX-RN
//
//  Created by Kevin M1 on 25/03/22.
//

import UIKit

class IAPFailedVC: UIViewController {

    //MARK: - IBOutlets
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblInstruction: UILabel!
    
    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    //MARK: - IBAction
    @IBAction func btnClosedTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
