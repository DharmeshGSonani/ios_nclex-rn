//
//  QuitTestPopUpVC.swift
//  NCLEX-RN
//
//  Created by Kevin M1 on 21/03/22.
//

import UIKit
import AMTabView

class QuitTestPopUpVC: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var lblInstruction: UILabel!
    
    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()

        let viewCount = UserDefaults.standard.value(forKey: "viewCount")
        let partName = UserDefaults.standard.value(forKey: "partName")
        let totalArrQuestion = UserDefaults.standard.value(forKey: "totalArrQuestion")
        UserDefaults.standard.synchronize()
        
        let attributedString1 = NSMutableAttributedString(string: "You've answered \(viewCount ?? "") out of ", attributes: [
            NSAttributedString.Key.font:UIFont(name: "Montserrat-Regular", size: 15)!,
            .foregroundColor: UIColor.init(red: 52/255, green: 57/255, blue: 101/255, alpha: 0.9)
            //            .kern: -0.29
        ])
        
        let attributedString2 = NSMutableAttributedString(string: "\(totalArrQuestion ?? "") question on this \(partName ?? "")", attributes: [
            NSAttributedString.Key.font:UIFont(name: "Montserrat-Regular", size: 15)!,
            .foregroundColor: UIColor.init(red: 52/255, green: 57/255, blue: 101/255, alpha: 0.9)
            //.kern: -0.29
        ])
        
        let priceNSMutableAttributedString = NSMutableAttributedString()
        priceNSMutableAttributedString.append(attributedString1)
        priceNSMutableAttributedString.append(attributedString2)
        
        self.lblInstruction.attributedText = priceNSMutableAttributedString
        
    }
    
    //MARK: - IBAction
    @IBAction func btnQuitTest(_ sender: Any) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! AMTabsViewController
        let navigationController = UINavigationController(rootViewController: nextViewController)
        navigationController.isNavigationBarHidden = true
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        appDelegate.isFromCheckIn = true
        appDelegate.window?.rootViewController = navigationController
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func btnSubmitTest(_ sender: Any) {
        
        let resultVC = self.storyboard?.instantiateViewController(withIdentifier: "ResultVC") as! ResultVC
        self.navigationController?.pushViewController(resultVC, animated: true)
        
    }
    
    @IBAction func btnContinueTest(_ sender: Any) {
       
        self.dismiss(animated: true, completion: nil)
    }
    
}
