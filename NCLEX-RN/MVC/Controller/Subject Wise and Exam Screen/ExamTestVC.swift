//
//  ExamTestVC.swift
//  NCLEX-RN
//
//  Created by Kevin M1 on 09/03/22.
//

import UIKit
import CryptoKit
import AMTabView
import SwiftUI

protocol submitQuestion{
    func questionSubmit(str:String)
}

class ExamTestVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //MARK: - IBOutlets
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var explanationView: UIView!
    @IBOutlet weak var lblQuestion: UITextView!
    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblExplanation: UILabel!
    @IBOutlet weak var lblExplanData: UITextView!
    @IBOutlet weak var imgExplan: UIImageView!
    @IBOutlet weak var btnFav: UIButton!
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var btn3: UIButton!
    @IBOutlet weak var lbl1: UILabel!
    @IBOutlet weak var lbl2: UILabel!
    @IBOutlet weak var lbl3: UILabel!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var lineBottomView: UIView!
    
    //MARK: - Variables
    var currentQuestionIndex : Int = 0
    var arrQuestions = NSMutableArray()
    var arrOptions = NSMutableArray()
    var arrAnswer = NSArray()
    
    var mainTitle = ""
    
    var isAnswerSelected = false
    var isButtonSelected = false
    
    var dicResult = NSMutableDictionary()
    var totalCorrectAnswer : Int = 0
    var totalInCorrectAnswer : Int = 0
    var totalQuestion : Int = 0
    
    var isFromDailyQuestion = false
    var isFromTopicVC = false
    var isFromQuickTest = false
    var isFromWrongQue = false
    var isFromCustomized = false
    var isFromDailyCheckIn = false
    
    var viewCount = 0
    var arrCount = 0
    var totalPartId = ""
    var topic_id = ""
    var partQueCount : Int = 0
    
    var timer = Timer()
    var second = 00
    var istimerrunning = false
    
    var delegateSumbit : submitQuestion!
    var arrOption1 = ["A", "B", "C", "D", "E"]
    
    var isSelected = false
    
    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblView.delegate = self
        tblView.dataSource = self
        
        lblTitle.text = mainTitle
        explanationView.isHidden = true
        
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)

        self.view2.isHidden = true
        self.view3.isHidden = true
        lineBottomView.isHidden = true
        
        if isFromTopicVC == true
        {
            let arrQu : NSArray = DBHandler.getDataFromTable("SELECT * FROM question WHERE topicId = '\(topic_id)' AND part_id = '\(totalPartId)'")
            
            arrCount = arrQu.count
            initiateDB(arrQu)
            
            if arrQuestions.count == 0 {
                
                for i in 0..<arrQu.count {
                    
                    let dic = arrQu.object(at: i) as! NSDictionary
                    let questionid = dic.value(forKey: "id") as! String
                    
                    let updateQuery : String = "UPDATE question SET read_count = '0' WHERE id = \(questionid) AND part_id = '\(totalPartId)'"
                    let success = DBHandler.genericQuery(updateQuery)
                    print(success)
                }
                
                let arrQu : NSArray = DBHandler.getDataFromTable("SELECT * FROM question WHERE topicId = '\(topic_id)' AND part_id = '\(totalPartId)'")
                initiateDB(arrQu)
            }
            
            self.dicResult.setValue("\(self.arrQuestions.count)", forKey: "totalQuestion")
            print(arrQuestions.count)
            
            self.view2.isHidden = false
            self.view3.isHidden = false
            
        }
        
        else if isFromDailyQuestion == true || isFromDailyCheckIn == true
        {
     
            arrQuestions = DBHandler.getDataFromTable("SELECT * FROM question ORDER BY random() LIMIT 3")
            self.dicResult.setValue("\(self.arrQuestions.count)", forKey: "totalQuestion")
            print(arrQuestions.count)
            
            self.view2.isHidden = false
            self.view3.isHidden = false
        }
        
        else if isFromQuickTest == true
        {
       
            arrQuestions = DBHandler.getDataFromTable("SELECT * FROM question ORDER BY random() LIMIT 10")
            self.dicResult.setValue("\(arrQuestions.count)", forKey: "totalQuestion")
            self.view2.isHidden = false
            self.view3.isHidden = false
            
        }
        else if isFromCustomized == true{
            
            if arrQuestions.count != 0{
                self.setQuestionAndAnswer()
                self.dicResult.setValue("\(arrQuestions.count)", forKey: "totalQuestion")
                self.view2.isHidden = false
                self.view3.isHidden = false
            }
            
        } else if isFromWrongQue == true{

            if arrQuestions.count != 0{
                self.setQuestionAndAnswer()
                self.dicResult.setValue("\(arrQuestions.count)", forKey: "totalQuestion")
                self.view2.isHidden = false
                self.view3.isHidden = false
            }
        }
        else {
            
            self.dicResult.setValue("\(arrQuestions.count)", forKey: "totalQuestion")
            self.setQuestionAndAnswer()
            self.view2.isHidden = false
            self.view3.isHidden = false
        }
        self.setQuestionAndAnswer()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tblView.reloadData()
    }
    
    @objc func updateTimer(){
        second += 1
    }
    
    func timeString(time: TimeInterval) -> String{
       
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        
        return String(format: " %02d:%02d ", minutes, seconds)
    }
    
    func setQuestionAndAnswer(){
        
        if self.arrQuestions.count > 0{
            
            let dicQuestion = self.arrQuestions.object(at: self.currentQuestionIndex) as! NSDictionary
            
            let currentQNo = self.currentQuestionIndex + 1
            self.lbl1.text = "\(currentQNo)"
            
            if currentQNo >=  3 {
                
                let second = currentQNo - 1
                self.lbl2.text = "\(second)"
                self.lbl2.textColor = UIColor.white
                
                let third = currentQNo - 2
                self.lbl3.text = "\(third)"
                self.lbl3.textColor = UIColor.white
                
            } else if currentQNo >= 2 {
                
                let second = currentQNo - 1
                self.lbl2.text = "\(second)"
                self.lbl3.text = ""
                self.lbl2.textColor = UIColor.white
                
            } else {
                
                self.lbl2.text = ""
                self.lbl3.text = ""
                
            }
            
            let strQuestionTitle = dicQuestion.value(forKey: "question") as! String
            lblQuestion.text = strQuestionTitle
            
            let strExplanation = dicQuestion.value(forKey: "explanation") as! String
            lblExplanData.text = strExplanation
            
            let strAnswer = dicQuestion.value(forKey: "answers") as! String
            var strFinalAnswer = strAnswer.replacingOccurrences(of: "[\"", with: "").replacingOccurrences(of: "\"]", with: "")
            strFinalAnswer = strFinalAnswer.replacingOccurrences(of: "\\", with: "")
            let strAnswerFinal = strFinalAnswer.replacingOccurrences(of: "\"", with: "")
            
            arrAnswer = strFinalAnswer.components(separatedBy: ";") as NSArray
            print(arrAnswer)
            
            let strOption = dicQuestion.value(forKey: "choices") as! String
            var strFinalOption = strOption.replacingOccurrences(of: "[\"", with: "").replacingOccurrences(of: "\"]", with: "")
            strFinalOption = strFinalOption.replacingOccurrences(of: "\\", with: "")
            let strOptionFinal = strFinalOption.replacingOccurrences(of: "\"", with: "").replacingOccurrences(of: "\"", with: "")
            
            var arrOption : [String] = []
            
            arrOption = strOptionFinal.components(separatedBy: "$")
            let count1 = arrOption.count
            arrOption.insert(strAnswerFinal, at: count1)
            
            self.arrOptions.removeAllObjects()
            
            for index in 0..<arrOption.count
            {
                let id = arrOption[index]
                self.arrOptions.add(id)
            }
            
            self.arrOptions = (arrOption.shuffled() as NSArray).mutableCopy() as! NSMutableArray
            
            let attributedString1 = NSMutableAttributedString(string: "\(self.currentQuestionIndex+1)/", attributes: [
                NSAttributedString.Key.font: UIFont(name: "Montserrat-Regular", size: 10)!,
                .foregroundColor: UIColor.init(red: 74/255, green: 92/255, blue: 208/255, alpha: 1)])
            
            let count = self.arrQuestions.count
            
            let attributedString2 = NSMutableAttributedString(string: "\(count)", attributes: [
                NSAttributedString.Key.font: UIFont(name: "Montserrat-Regular", size: 10)!,
                .foregroundColor: UIColor.init(red: 74/255, green: 92/255, blue: 208/255, alpha: 0.60)])
            
            let priceNSMutableAttributedString = NSMutableAttributedString()
            priceNSMutableAttributedString.append(attributedString1)
            priceNSMutableAttributedString.append(attributedString2)
            
            self.lblCount.attributedText = priceNSMutableAttributedString
            
            let que_id = dicQuestion.value(forKey: "id") as? String
            
            let favQue = DBHandler.getDataFromTable("SELECT * FROM question WHERE is_fav = '0'")
            
            if favQue != nil{
                
                let array = favQue!
                
                let predicate = NSPredicate.init(format: "SELF = %@", que_id!)
                let filtered = array.filtered(using: predicate) as NSArray
                
                if filtered.count > 0{
                    self.btnFav.setImage(UIImage(named: "ic_favourite"), for: .normal)
                    self.isButtonSelected = true
                }
                else{
                    self.btnFav.setImage(UIImage(named: "ic_unfavourite"), for: .normal)
                    self.isButtonSelected = false
                }
            }
            
            self.view1.backgroundColor = UIColor(named: "grayBlue")
            
            self.tblView.reloadData()
        }
    }
    
    //MARK: - TableView Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblView.dequeueReusableCell(withIdentifier: "ExamTableViewCell", for: indexPath) as! ExamTableViewCell
        
        cell.lblOption.text = arrOptions[indexPath.row] as? String
        
        cell.lblOption1.text = arrOption1[indexPath.row]
        
        cell.bgView.backgroundColor = UIColor(red: 236/255, green: 242/255, blue: 255/255, alpha: 1.0)
//        cell.blankView.isHidden = false
//        cell.blankView.backgroundColor = UIColor(named: "cellbgview")
        cell.imgView1.isHidden = true
        
        if isFromTopicVC == true{
            
            self.btnNext.layer.opacity = 0.8
            
            let dicQuestion = arrQuestions.object(at: self.currentQuestionIndex) as! NSDictionary
            let questionId = dicQuestion.value(forKey: "id") as! String
            let part_id = dicQuestion.value(forKey: "part_id") as! String
            
            let array = DBHandler.getDataFromTable("SELECT * FROM question WHERE id = '\(questionId)' AND part_id = '\(part_id)'")
            
            if array!.count > 0{
                
                let dicObject = array?.firstObject as! NSDictionary
                
                if dicObject.value(forKey: "is_wrong") as! String != ""{
                    
                    let strSelected = self.arrOptions.object(at: indexPath.row) as! String
                    let correct = dicObject.value(forKey: "correct_option") as! String
                    let incorrect = dicObject.value(forKey: "wrong_option") as! String
                    
                    if dicObject.value(forKey: "is_wrong") as! String == "1"{
                        
                        if strSelected == correct{
                            
//                            cell.blankView.isHidden = true
//                            cell.imgView.image = UIImage(named: "ic_right")
                            
                            cell.bgView.backgroundColor = UIColor(named: "green")
                            cell.imgView1.image = UIImage(named: "ic_mark")
                            cell.imgView1.isHidden = false
                        }
                        else if strSelected == incorrect{
                            
//                            cell.blankView.isHidden = true
//                            cell.imgView.image = UIImage(named: "ic_wrong1")
                            cell.bgView.backgroundColor = UIColor(named: "red")
                            cell.imgView1.image = UIImage(named: "ic_cross")
                            cell.imgView1.isHidden = false
                        }
                        else{
                            cell.bgView.backgroundColor = UIColor(red: 236/255, green: 242/255, blue: 255/255, alpha: 1.0)
//                            cell.imgView1.isHidden = true
//                            cell.blankView.isHidden = false
                        }
                    }
                    else{
                        if strSelected == correct{
                            
//                            cell.blankView.isHidden = true
//                            cell.imgView.image = UIImage(named: "ic_right")
                            cell.bgView.backgroundColor = UIColor(named: "green")
                            cell.imgView1.image = UIImage(named: "ic_mark")
                            cell.imgView1.isHidden = false
                        }
                        else{
                            
                            cell.bgView.backgroundColor = UIColor(red: 236/255, green: 242/255, blue: 255/255, alpha: 1.0)
                            cell.imgView1.isHidden = true
//                            cell.blankView.isHidden = false
                        }
                    }
                }else {
                    cell.bgView.backgroundColor = UIColor(red: 236/255, green: 242/255, blue: 255/255, alpha: 1.0)
                    cell.imgView1.isHidden = true
                    cell.blankView.isHidden = true
                }
            }else {
                cell.bgView.backgroundColor = UIColor(red: 236/255, green: 242/255, blue: 255/255, alpha: 1.0)
                cell.imgView1.isHidden = true
                cell.blankView.isHidden = true
            }
        } else {
            
            var array = NSArray()
            
            self.btnNext.layer.opacity = 0.8
            let dicQuestion = arrQuestions.object(at: self.currentQuestionIndex) as! NSDictionary
            let questionId = dicQuestion.value(forKey: "id") as! String
            
            if isFromDailyCheckIn == true{
            
                array = DBHandler.getDataFromTable("SELECT * FROM daily_checkin WHERE que_id = '\(questionId)'")
                
            } else if isFromDailyQuestion == true{
                
                array = DBHandler.getDataFromTable("SELECT * FROM daily_test WHERE que_id = '\(questionId)'")

            } else if isFromQuickTest == true{
                
                array = DBHandler.getDataFromTable("SELECT * FROM quick_test WHERE que_id = '\(questionId)'")

            } else if isFromWrongQue == true{
                
                array = DBHandler.getDataFromTable("SELECT * FROM wrong_test WHERE que_id = '\(questionId)'")

            } else if isFromCustomized == true{
                
                array = DBHandler.getDataFromTable("SELECT * FROM customized_test WHERE que_id = '\(questionId)'")

            }
            
            if array.count > 0{
                
                let dicObject = array.firstObject as! NSDictionary
                
                if dicObject.value(forKey: "is_wrong") as! String != ""{
                    
                    let strSelected = self.arrOptions.object(at: indexPath.row) as! String
                    let correct = dicObject.value(forKey: "correct_option") as! String
                    let incorrect = dicObject.value(forKey: "wrong_option") as! String
                    
                    if dicObject.value(forKey: "is_wrong") as! String == "1"{
                        
                        if strSelected == correct{
                            
//                            cell.blankView.isHidden = true
//                            cell.imgView.image = UIImage(named: "ic_right")
                            cell.bgView.backgroundColor = UIColor(named: "green")
                            cell.imgView1.image = UIImage(named: "ic_mark")
                            cell.imgView1.isHidden = false
                        }
                        else if strSelected == incorrect{
                            
//                            cell.blankView.isHidden = true
//                            cell.imgView.image = UIImage(named: "ic_wrong1")
                            cell.bgView.backgroundColor = UIColor(named: "red")
                            cell.imgView1.image = UIImage(named: "ic_cross")
                            cell.imgView1.isHidden = false
                        }
                        else{
                            cell.bgView.backgroundColor = UIColor(red: 236/255, green: 242/255, blue: 255/255, alpha: 1.0)
//                            cell.imgView1.isHidden = true
//                            cell.blankView.isHidden = false
                        }
                    }
                    else{
                        if strSelected == correct{
                            
//                            cell.blankView.isHidden = true
//                            cell.imgView.image = UIImage(named: "ic_right")
                            cell.bgView.backgroundColor = UIColor(named: "green")
                            cell.imgView1.image = UIImage(named: "ic_mark")
                            cell.imgView1.isHidden = false
                        }
                        else{
                            
                            cell.bgView.backgroundColor = UIColor(red: 236/255, green: 242/255, blue: 255/255, alpha: 1.0)
                            cell.imgView1.isHidden = true
//                            cell.blankView.isHidden = false
                        }
                    }
                } else {
                    cell.bgView.backgroundColor = UIColor(red: 236/255, green: 242/255, blue: 255/255, alpha: 1.0)
                    cell.imgView1.isHidden = true
                    cell.blankView.isHidden = true
                }
            } else {
                cell.bgView.backgroundColor = UIColor(red: 236/255, green: 242/255, blue: 255/255, alpha: 1.0)
                cell.imgView1.isHidden = true
                cell.blankView.isHidden = true
            }
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if isAnswerSelected{
            return
        }
        
        isAnswerSelected = true
        
        let cell = tblView.cellForRow(at: indexPath) as! ExamTableViewCell
        
        if isFromTopicVC == true{
            
            let dicQuestion = arrQuestions.object(at: self.currentQuestionIndex) as! NSDictionary
            let questionId = dicQuestion.value(forKey: "id") as! String
            let part_id = dicQuestion.value(forKey: "part_id") as! String
            
            let array = DBHandler.getDataFromTable("SELECT * FROM question WHERE id = '\(questionId)' AND part_id='\(part_id)'")
            
            if array!.count > 0{
                
                let dicObject = array?.firstObject as! NSDictionary
                
                if dicObject.value(forKey: "is_wrong") as! String == ""{
                    
                    let strAnswer = dicQuestion.value(forKey: "answers") as! String
                    var strFinalAnswer = strAnswer.replacingOccurrences(of: "[\"", with: "").replacingOccurrences(of: "\"]", with: "")
                    strFinalAnswer = strFinalAnswer.replacingOccurrences(of: "\\", with: "")
                    strFinalAnswer = strFinalAnswer.replacingOccurrences(of: "\"", with: "")
                    
                    var strSelected = self.arrOptions.object(at: indexPath.row) as! String
                    
                    if strSelected == strFinalAnswer {
                        
                        strSelected = strSelected.setReplaceValue()
                        strFinalAnswer = strFinalAnswer.setReplaceValue()
                        
                        let success = DBHandler.genericQuery("UPDATE question SET is_wrong = '0', correct_option = '\(strSelected)' WHERE id = '\(questionId)' AND part_id='\(part_id)'")
                        
                        if success{
                            print("Update Successfully")
                            self.tblView.reloadData()
                        }
                        
                        self.totalCorrectAnswer = self.totalCorrectAnswer + 1
                        
                        let qNo1 = Int(self.lbl1.text ?? "")
                        
                        let currentQno = self.currentQuestionIndex + 1
                        
                        if qNo1 == currentQno {
                            self.view1.backgroundColor = UIColor(named: "darkgreen")
//                            self.lbl1.textColor = UIColor.white
                        }
//                        cell.imgView.image = UIImage(named: "ic_right")
                        cell.bgView.backgroundColor = UIColor(named: "green")
//                        cell.blankView.isHidden = false
//                        cell.blankView.backgroundColor = UIColor(named: "cellbgview")
                        cell.imgView1.isHidden = false
                        cell.imgView1.image = UIImage(named: "ic_mark")
                        lblExplanation.textColor = UIColor(named: "darkgreen")
                        imgExplan.image = UIImage(named: "ic_right")
                        explanationView.isHidden = false
                        lineBottomView.isHidden = false
                        
                        self.dicResult.setValue("\(self.totalCorrectAnswer)", forKey: "correctAnswer")
                        
                        self.btnNext.layer.opacity = 1.0
                        
                    } else {
                        
                        strSelected = strSelected.setReplaceValue()
                        strFinalAnswer = strFinalAnswer.setReplaceValue()
                        
                        let success = DBHandler.genericQuery("UPDATE question SET is_wrong = '1', wrong_option = '\(strSelected)', correct_option = '\(strFinalAnswer)' WHERE id = '\(questionId)' AND part_id='\(part_id)'")
                        
                        if success{
                            print("Update Successfully")
                            self.tblView.reloadData()
                        }
                        
                        self.totalInCorrectAnswer = self.totalInCorrectAnswer + 1
                        
                        let qNo1 = Int(self.lbl1.text ?? "")
                        
                        let currentQno = self.currentQuestionIndex + 1
                        
                        if qNo1 == currentQno {
                            self.view1.backgroundColor = UIColor(named: "darkred")
//                            self.lbl1.textColor = UIColor.white
                        }
                        
//                        cell.imgView.image = UIImage(named: "ic_wrong1")
                        cell.bgView.backgroundColor = UIColor(named: "red")
//                        cell.blankView.isHidden = false
//                        cell.blankView.backgroundColor = UIColor(named: "cellbgview")
                        cell.imgView1.isHidden = false
                        cell.imgView1.image = UIImage(named: "ic_cross")
                        lblExplanation.textColor = UIColor(named: "darkred")
                        imgExplan.image = UIImage(named: "ic_wrong1")
                        explanationView.isHidden = false
                        lineBottomView.isHidden = false
                        
                        for index in 0..<arrOptions.count
                        {
                            let option = arrOptions[index]
                            
                            if option as! String == strFinalAnswer{
                                
                                let cellRightAnswer = tableView.cellForRow(at: IndexPath.init(row: index, section: 0)) as! ExamTableViewCell
                                
//                                cellRightAnswer.imgView.image = UIImage(named: "ic_right")
                                cellRightAnswer.bgView.backgroundColor = UIColor(named: "green")
//                                cellRightAnswer.blankView.isHidden = false
//                                cellRightAnswer.blankView.backgroundColor = UIColor(named: "cellbgview")
                                cellRightAnswer.imgView1.isHidden = false
                                cellRightAnswer.imgView1.image = UIImage(named: "ic_mark")
                                
                            }
                            self.dicResult.setValue("\(self.totalInCorrectAnswer)", forKey: "IncorrectAnswer")
                        }
                        self.btnNext.layer.opacity = 1.0
                    }
                }
            }
        } else {
            
            let dicQuestion = arrQuestions.object(at: self.currentQuestionIndex) as! NSDictionary
            var strQuestion = dicQuestion.value(forKey: "question") as! String
            let questionId = dicQuestion.value(forKey: "id") as! String
            
            let arrQuick = DBHandler.getDataFromTable("SELECT * FROM question WHERE id = '\(questionId)'")
            
            if arrQuick!.count > 0{
                
                let dicObject = arrQuick?.firstObject as! NSDictionary
                
                if dicObject.value(forKey: "is_wrong") as! String == ""{
                    
                    let strAnswer = dicQuestion.value(forKey: "answers") as! String
                    var strFinalAnswer = strAnswer.replacingOccurrences(of: "[\"", with: "").replacingOccurrences(of: "\"]", with: "")
                    strFinalAnswer = strFinalAnswer.replacingOccurrences(of: "\\", with: "")
                    strFinalAnswer = strFinalAnswer.replacingOccurrences(of: "\"", with: "")
                    
                    let strOption = dicQuestion.value(forKey: "choices") as! String
                    var strFinal = strOption.replacingOccurrences(of: "[\"", with: "").replacingOccurrences(of: "\"]", with: "")
                    strFinal = strFinal.replacingOccurrences(of: "\\", with: "")
                    strFinal = strFinal.replacingOccurrences(of: "\"", with: "").replacingOccurrences(of: "\"", with: "")
                    
                    var strSelected = self.arrOptions.object(at: indexPath.row) as! String
                    
                    if strSelected == strFinalAnswer {
                        
                        if isFromQuickTest == true{
                            
                            strSelected = strSelected.setReplaceValue()
                            strFinal = strFinal.setReplaceValue()

                            let insertSuccess = DBHandler.addQuickTest(questionId, is_wrong: "0", correct_option: strSelected, wrong_option: "")
                            
                            print(insertSuccess)
                            
                        }
                        else if isFromWrongQue == true{
                            
                            let insertSuccess = DBHandler.addWrongQue(questionId, is_wrong: "0", correct_option: strSelected, wrong_option: "")
                            
                            print(insertSuccess)
                            
//                            let success = DBHandler.genericQuery("UPDATE question SET is_wrong = '0', correct_option = '\(strSelected)' WHERE id = '\(questionId)'")
//
//                            if success{
//                                print("Update Successfully")
//                            }
                            
                        }
                        else if isFromCustomized == true{
                            
                            let insertSuccess = DBHandler.addCustomized(questionId, is_wrong: "0", correct_option: strSelected, wrong_option: "")
                            
                            print(insertSuccess)
                            
                        } else if isFromDailyQuestion == true{
                            
                            let insertSuccess = DBHandler.addDaily(questionId, is_wrong: "0", correct_option: strSelected, wrong_option: "")
                            
                            print(insertSuccess)
                            
                        } else if isFromDailyCheckIn == true{
                            
                            let insertSuccess = DBHandler.addDailyCheckin(questionId, is_wrong: "0", correct_option: strSelected, wrong_option: "")
                            
                            print(insertSuccess)
                        }
                        
                        self.totalCorrectAnswer = self.totalCorrectAnswer + 1
                        
                        let qNo1 = Int(self.lbl1.text ?? "")
                        
                        let currentQno = self.currentQuestionIndex + 1
                        
                        if qNo1 == currentQno {
                            self.view1.backgroundColor = UIColor(named: "darkgreen")
//                            self.lbl1.textColor = UIColor.white
                        }
                        
                        //                    cell.imgView.image = UIImage(named: "ic_right")
                        cell.bgView.backgroundColor = UIColor(named: "green")
                        //                    cell.blankView.isHidden = false
                        //                    cell.blankView.backgroundColor = UIColor(named: "green")
                        cell.imgView1.isHidden = false
                        cell.imgView1.image = UIImage(named: "ic_mark")
                        lblExplanation.textColor = UIColor(named: "darkgreen")
                        imgExplan.image = UIImage(named: "ic_right")
                        explanationView.isHidden = false
                        lineBottomView.isHidden = false
                        
                        self.dicResult.setValue("\(self.totalCorrectAnswer)", forKey: "correctAnswer")
                        
                        self.btnNext.layer.opacity = 1.0
                        
                    } else {
                        
                        if isFromQuickTest == true{
                            
                            strSelected = strSelected.setReplaceValue()
                            strQuestion = strQuestion.setReplaceValue()
                            strFinal = strFinal.setReplaceValue()
                            
                            let insertSuccess = DBHandler.addQuickTest(questionId, is_wrong: "1", correct_option: strFinalAnswer, wrong_option: strSelected)
                            
                            print(insertSuccess)
                            
                        } else if isFromWrongQue == true{
                            
                            let insertSuccess = DBHandler.addWrongQue(questionId, is_wrong: "1", correct_option: strFinalAnswer, wrong_option: strSelected)
                            print(insertSuccess)
                            
//                            let success = DBHandler.genericQuery("UPDATE question SET is_wrong = '1', wrong_option = '\(strSelected)', correct_option = '\(strFinalAnswer)' WHERE id = '\(questionId)'")
//
//                            if success{
//                                print("Update Successfully")
//                            }
                            
                        } else if isFromCustomized == true{
                            
                            let insertSuccess = DBHandler.addCustomized(questionId, is_wrong: "1", correct_option: strFinalAnswer, wrong_option: strSelected)
                            print(insertSuccess)
                            
                        } else if isFromDailyQuestion == true{
                            
                            let insertSuccess = DBHandler.addDaily(questionId, is_wrong: "1", correct_option: strFinalAnswer, wrong_option: strSelected)
                            print(insertSuccess)
                            
                        } else if isFromDailyCheckIn == true{
                            let insertSuccess = DBHandler.addDailyCheckin(questionId, is_wrong: "1", correct_option: strFinalAnswer, wrong_option: strSelected)
                            print(insertSuccess)
                        }
                        
                        self.totalInCorrectAnswer = self.totalInCorrectAnswer + 1
                        
                        let qNo1 = Int(self.lbl1.text ?? "")
                        
                        let currentQno = self.currentQuestionIndex + 1
                        
                        if qNo1 == currentQno {
                            self.view1.backgroundColor = UIColor(named: "darkred")
//                            self.lbl1.textColor = UIColor.white
                        }
                        
                        //                    cell.imgView.image = UIImage(named: "ic_wrong1")
                        cell.bgView.backgroundColor = UIColor(named: "red")
                        //                    cell.blankView.isHidden = false
                        //                    cell.blankView.backgroundColor = UIColor(named: "red")
                        cell.imgView1.isHidden = false
                        cell.imgView1.image = UIImage(named: "ic_cross")
                        lblExplanation.textColor = UIColor(named: "darkred")
                        imgExplan.image = UIImage(named: "ic_wrong1")
                        explanationView.isHidden = false
                        lineBottomView.isHidden = false
                        
                        for index in 0..<arrOptions.count
                        {
                            let option = arrOptions[index]
                            
                            if option as! String == strFinalAnswer{
                                
                                let cellRightAnswer = tableView.cellForRow(at: IndexPath.init(row: index, section: 0)) as! ExamTableViewCell
                                
                                //                            cellRightAnswer.imgView.image = UIImage(named: "ic_right")
                                cellRightAnswer.bgView.backgroundColor = UIColor(named: "green")
                                //                            cellRightAnswer.blankView.isHidden = false
                                //                            cellRightAnswer.blankView.backgroundColor = UIColor(named: "green")
                                cellRightAnswer.imgView1.isHidden = false
                                cellRightAnswer.imgView1.image = UIImage(named: "ic_mark")
                                
                            }
                            self.dicResult.setValue("\(self.totalInCorrectAnswer)", forKey: "IncorrectAnswer")
                        }
                        self.btnNext.layer.opacity = 1.0
                    }
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func initiateDB(_ arrQu: NSArray)
    {
        for i in 0..<arrQu.count {
            if let dicQuestion = arrQu.object(at: i) as? NSDictionary {
                if let read_count : String = dicQuestion.value(forKey: "read_count") as? String {
                    print("read_count==> ", read_count)
                    if read_count == "0" || read_count == ""{
                        arrQuestions.add(dicQuestion)
                    }
                } else {
                    arrQuestions.add(dicQuestion)
                }
            }
        }
    }
    
    //MARK: - IBAction
    @IBAction func btnBackTapped(_ sender: Any) {
            
         if isFromTopicVC == true  {
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "QuitTestPopUpVC") as! QuitTestPopUpVC
            let vc = UINavigationController(rootViewController: nextViewController)
            vc.isNavigationBarHidden = true
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle   = .crossDissolve
            self.present(vc, animated: true, completion: nil)
            
        } else {
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! AMTabsViewController
            let vc = UINavigationController(rootViewController: nextViewController)
            vc.isNavigationBarHidden = true
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle   = .crossDissolve
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnNextTapped(_ sender: Any) {
        
        isAnswerSelected = false
        
        if isFromTopicVC == true{
            
            let dicQuestion = arrQuestions.object(at: self.currentQuestionIndex) as! NSDictionary
            let questionId = dicQuestion.value(forKey: "id") as! String
            let part_id = dicQuestion.value(forKey: "part_id") as! String
            
            let array = DBHandler.getDataFromTable("SELECT * FROM question WHERE id = '\(questionId)' AND part_id = '\(part_id)'")
            
            if array!.count > 0{
                
                let dicObject = array?.firstObject as! NSDictionary
                
                if dicObject.value(forKey: "is_wrong") as! String != ""{
                    
                    self.perform(#selector(self.displayNextQuestion), with: nil, afterDelay: 0.5)
                    
                }
            }
        }
        else {

            let dicQuestion = arrQuestions.object(at: self.currentQuestionIndex) as! NSDictionary
            let questionId = dicQuestion.value(forKey: "id") as! String
            
            if isFromQuickTest == true{
                
                let array = DBHandler.getDataFromTable("SELECT * FROM quick_test WHERE que_id = '\(questionId)'")
                
                if array!.count > 0{
                    
                    let dicObject = array?.firstObject as! NSDictionary
                    
                    if dicObject.value(forKey: "is_wrong") as! String != ""{
                        
                        self.perform(#selector(self.displayNextQuestion), with: nil, afterDelay: 0.5)
                    } else {
                        self.view.makeToast("Please Select Atleast one Option.")
                    }
                }
            } else if isFromCustomized == true{
                let array = DBHandler.getDataFromTable("SELECT * FROM customized_test WHERE que_id = '\(questionId)'")
                
                if array!.count > 0{
                    
                    let dicObject = array?.firstObject as! NSDictionary
                    
                    if dicObject.value(forKey: "is_wrong") as! String != ""{
                        
                        self.perform(#selector(self.displayNextQuestion), with: nil, afterDelay: 0.5)
                    } else {
                        self.view.makeToast("Please Select Atleast one Option.")
                    }
                }
                
            } else if isFromWrongQue == true{
                let array = DBHandler.getDataFromTable("SELECT * FROM wrong_test WHERE que_id = '\(questionId)'")
                
                if array!.count > 0{
                    
                    let dicObject = array?.firstObject as! NSDictionary
                    
                    if dicObject.value(forKey: "is_wrong") as! String != ""{
                        
                        self.perform(#selector(self.displayNextQuestion), with: nil, afterDelay: 0.5)
                    } else {
                        self.view.makeToast("Please Select Atleast one Option.")
                    }
                }
                
            } else if isFromDailyQuestion == true{
                let array = DBHandler.getDataFromTable("SELECT * FROM daily_test WHERE que_id = '\(questionId)'")
                
                if array!.count > 0{
                    
                    let dicObject = array?.firstObject as! NSDictionary
                    
                    if dicObject.value(forKey: "is_wrong") as! String != ""{
                        
                        self.perform(#selector(self.displayNextQuestion), with: nil, afterDelay: 0.5)
                    } else {
                        self.view.makeToast("Please Select Atleast one Option.")
                    }
                }
                
            } else if isFromDailyCheckIn == true{
                
                let array = DBHandler.getDataFromTable("SELECT * FROM daily_checkin WHERE que_id = '\(questionId)'")
                
                if array!.count > 0{
                    
                    let dicObject = array?.firstObject as! NSDictionary
                    
                    if dicObject.value(forKey: "is_wrong") as! String != ""{
                        
                        self.perform(#selector(self.displayNextQuestion), with: nil, afterDelay: 0.5)
                    } else {
                        self.view.makeToast("Please Select Atleast one Option.")
                    }
                }
            }
        }
    }
    
    @objc func displayNextQuestion(){
        
        if self.currentQuestionIndex < self.arrQuestions.count - 1 {
            
            if self.isFromTopicVC == true{
                self.setupViewQuestion()
            }
            
            self.currentQuestionIndex = self.currentQuestionIndex + 1
            self.explanationView.isHidden = true
            lineBottomView.isHidden = true
            
            self.setQuestionAndAnswer()
            self.showStatusofQuestion()
            
            if isFromDailyCheckIn == true{
                
                if isSelected == false{
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.isFromCheckIn = true
                    appDelegate.totalQuestionShow = appDelegate.totalQuestionShow + 1
                }
            }
            
        } else {
            
            if isFromDailyCheckIn == true{
                
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.isFromCheckIn = true
                appDelegate.totalQuestionShow = appDelegate.totalQuestionShow + 1
                
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "SubmitTestPopUpVc") as! SubmitTestPopUpVc
                nextViewController.isFromDailyCheckIn = true
                nextViewController.time = timeString(time: TimeInterval(second))
                nextViewController.dicResult = self.dicResult
                let vc = UINavigationController(rootViewController: nextViewController)
                vc.isNavigationBarHidden = true
                
                vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle   = .crossDissolve
                self.present(vc, animated: true, completion: nil)
                
                
            } else if isFromTopicVC == true{
                
                let dic = self.arrQuestions.object(at: self.currentQuestionIndex) as! NSDictionary
                let questionid = dic.value(forKey: "id") as! String
                let part_id = dic.value(forKey: "part_id") as! String
                
                let updateQuery : String = "UPDATE question SET read_count = '1' WHERE id = '\(questionid)' AND part_id='\(part_id)'"
                let success = DBHandler.genericQuery(updateQuery)
                print(success)
                
                let topic_id = dic.value(forKey: "topicId") as! String
                
                viewCount = viewCount + 1
                
                let readRecord = DBHandler.getDataFromTable("SELECT * FROM read_question WHERE topic_id = '\(topic_id)' AND part_id = '\(part_id)'")
                
                if readRecord!.count > 0{
                    
                    let query = "UPDATE read_question SET viewcount = '\(viewCount)', topic_id = '\(topic_id)', part_id = '\(part_id)' WHERE topic_id = '\(topic_id)' AND part_id = '\(part_id)'"
                    let updateSuccess = DBHandler.genericQuery(query)
                    print(updateSuccess)
                }
                else{
                    
                    let insertSuccess = DBHandler.addReadQue(topic_id, totalPages: self.arrQuestions.count, viewCount: viewCount, partid: part_id)
                    
                    print(insertSuccess)
                }
                
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "SubmitTestPopUpVc") as! SubmitTestPopUpVc
                nextViewController.isFromTopic = true
                nextViewController.dicResult = self.dicResult
                nextViewController.time = timeString(time: TimeInterval(second))
                nextViewController.part_id = totalPartId
                let vc = UINavigationController(rootViewController: nextViewController)
                vc.isNavigationBarHidden = true
                
                vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle   = .crossDissolve
                self.present(vc, animated: true, completion: nil)
                
            }
            else if isFromQuickTest == true {
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "SubmitTestPopUpVc") as! SubmitTestPopUpVc
                nextViewController.isFromQuickTest = true
                nextViewController.time = timeString(time: TimeInterval(second))
                nextViewController.dicResult = self.dicResult
                let vc = UINavigationController(rootViewController: nextViewController)
                vc.isNavigationBarHidden = true
                
                vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle   = .crossDissolve
                self.present(vc, animated: true, completion: nil)
            }
            else if isFromCustomized == true {
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "SubmitTestPopUpVc") as! SubmitTestPopUpVc
                nextViewController.isFromCustomized = true
                nextViewController.dicResult = self.dicResult
                let vc = UINavigationController(rootViewController: nextViewController)
                vc.isNavigationBarHidden = true
                
                vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle   = .crossDissolve
                self.present(vc, animated: true, completion: nil)
            }
            else if isFromWrongQue == true {
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "SubmitTestPopUpVc") as! SubmitTestPopUpVc
                nextViewController.isFromCustomized = true
                nextViewController.dicResult = self.dicResult
                nextViewController.time = timeString(time: TimeInterval(second))
                let vc = UINavigationController(rootViewController: nextViewController)
                vc.isNavigationBarHidden = true
                
                vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle   = .crossDissolve
                self.present(vc, animated: true, completion: nil)
                
            } else if isFromDailyQuestion == true {
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "SubmitTestPopUpVc") as! SubmitTestPopUpVc
                nextViewController.dicResult = self.dicResult
                nextViewController.time = timeString(time: TimeInterval(second))
                nextViewController.isFromDailyTest = true
                let vc = UINavigationController(rootViewController: nextViewController)
                vc.isNavigationBarHidden = true
                
                vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle   = .crossDissolve
                self.present(vc, animated: true, completion: nil)
                
            } else {
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "SubmitTestPopUpVc") as! SubmitTestPopUpVc
                nextViewController.dicResult = self.dicResult
                nextViewController.time = timeString(time: TimeInterval(second))
                let vc = UINavigationController(rootViewController: nextViewController)
                vc.isNavigationBarHidden = true
                
                vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle   = .crossDissolve
                self.present(vc, animated: true, completion: nil)
            }
        }
    }
    
    func showStatusofQuestion()
    {
        if isFromTopicVC == true{
            
            if self.lbl3.text != ""{
                
                view3.isHidden = false
                
                let index = (Int)(self.lbl3.text!)
                
                let questionIndex = index! - 1
                
                let dicQuestion = self.arrQuestions.object(at: questionIndex) as! NSDictionary
                let questionId = dicQuestion.value(forKey: "id") as! String
                let part_id = dicQuestion.value(forKey: "part_id") as! String
                
                let array = DBHandler.getDataFromTable("SELECT * FROM question WHERE id = '\(questionId)' AND part_id = '\(part_id)'")
                
                if array!.count > 0{
                    
                    let dicObject =  array?.firstObject as! NSDictionary
                    
                    if dicObject.value(forKey: "is_wrong") as! String == "1"{
                        
                        self.view3.backgroundColor = UIColor(named: "darkred")
                    }
                    else if dicObject.value(forKey: "is_wrong") as! String == "0"{
                        
                        self.view3.backgroundColor = UIColor(named: "darkgreen")
                    }
                }
            }
            else {
                self.view3.backgroundColor = UIColor.clear
            }
            
            if self.lbl2.text != ""{
                
                view2.isHidden = false
                
                let index = (Int)(self.lbl2.text!)
                
                let questionIndex = index! - 1
                
                let dicQuestion = self.arrQuestions.object(at: questionIndex) as! NSDictionary
                let questionId = dicQuestion.value(forKey: "id") as! String
                let part_id = dicQuestion.value(forKey: "part_id") as! String
                
                let array = DBHandler.getDataFromTable("SELECT * FROM question WHERE id = '\(questionId)' AND part_id = '\(part_id)'")
                
                if array!.count > 0{
                    
                    let dicObject = array?.firstObject as! NSDictionary
                    
                    if dicObject.value(forKey: "is_wrong") as! String == "1"{
                        
                        self.view2.backgroundColor = UIColor(named: "darkred")
                    }
                    else if dicObject.value(forKey: "is_wrong") as! String == "0"{
                        
                        self.view2.backgroundColor = UIColor(named: "darkgreen")
                    }
                }
            }
            else {
                self.view2.backgroundColor = UIColor.clear
            }
            
            if self.lbl1.text != ""{
                
                let index = (Int)(self.lbl1.text!)
                
                let questionIndex = index! - 1
                
                let dicQuestion = self.arrQuestions.object(at: questionIndex) as! NSDictionary
                let questionId = dicQuestion.value(forKey: "id") as! String
                let part_id = dicQuestion.value(forKey: "part_id") as! String
                
                let array = DBHandler.getDataFromTable("SELECT * FROM question WHERE id = '\(questionId)' AND part_id = '\(part_id)'")
                
                if array!.count > 0{
                    
                    
                    let dicObject =  array?.firstObject as! NSDictionary
                    
                    if dicObject.value(forKey: "is_wrong") as! String == "1"{
                        
                        self.view1.backgroundColor = UIColor(named: "darkred")
                    }
                    else if dicObject.value(forKey: "is_wrong") as! String == "0"{
                        
                        self.view1.backgroundColor = UIColor(named: "darkgreen")
                    }
                }
            }
            else {
                self.view1.backgroundColor = UIColor.clear
            }
        }
        else {
            
            var array = NSArray()
            if self.lbl3.text != ""{
                
                view3.isHidden = false
                
                let index = (Int)(self.lbl3.text!)
                
                let questionIndex = index! - 1
                
                let dicQuestion = self.arrQuestions.object(at: questionIndex) as! NSDictionary
                let questionId = dicQuestion.value(forKey: "id") as! String
                
                if isFromDailyCheckIn == true{
                
                array = DBHandler.getDataFromTable("SELECT * FROM daily_checkin WHERE que_id = '\(questionId)'")
                    
                } else if isFromDailyQuestion == true{
                array = DBHandler.getDataFromTable("SELECT * FROM daily_test WHERE que_id = '\(questionId)'")
                    

                } else if isFromQuickTest == true{
                array = DBHandler.getDataFromTable("SELECT * FROM quick_test WHERE que_id = '\(questionId)'")
                    

                } else if isFromWrongQue == true{
                array = DBHandler.getDataFromTable("SELECT * FROM wrong_test WHERE que_id = '\(questionId)'")
                    

                } else if isFromCustomized == true{
                array = DBHandler.getDataFromTable("SELECT * FROM customized_test WHERE que_id = '\(questionId)'")

                }
                
                if array.count > 0{
                    
                    let dicObject =  array.firstObject as! NSDictionary
                    
                    if dicObject.value(forKey: "is_wrong") as! String == "1"{
                        
                        self.view3.backgroundColor = UIColor(named: "darkred")
                    }
                    else if dicObject.value(forKey: "is_wrong") as! String == "0"{
                        
                        self.view3.backgroundColor = UIColor(named: "darkgreen")
                    }
                }
            } else{
                self.view3.backgroundColor = UIColor.clear
            }
            
            if self.lbl2.text != ""{
                
                view2.isHidden = false
                
                let index = (Int)(self.lbl2.text!)
                
                let questionIndex = index! - 1
                
                let dicQuestion = self.arrQuestions.object(at: questionIndex) as! NSDictionary
                let questionId = dicQuestion.value(forKey: "id") as! String
                

                if isFromDailyCheckIn == true{
                
                array = DBHandler.getDataFromTable("SELECT * FROM daily_checkin WHERE que_id = '\(questionId)'")
                    
                } else if isFromDailyQuestion == true{
                array = DBHandler.getDataFromTable("SELECT * FROM daily_test WHERE que_id = '\(questionId)'")
                    

                } else if isFromQuickTest == true{
                array = DBHandler.getDataFromTable("SELECT * FROM quick_test WHERE que_id = '\(questionId)'")
                    

                } else if isFromWrongQue == true{
                array = DBHandler.getDataFromTable("SELECT * FROM wrong_test WHERE que_id = '\(questionId)'")
                    

                } else if isFromCustomized == true{
                array = DBHandler.getDataFromTable("SELECT * FROM customized_test WHERE que_id = '\(questionId)'")

                }
                
                if array.count > 0{
                    
                    let dicObject =  array.firstObject as! NSDictionary
                    
                    if dicObject.value(forKey: "is_wrong") as! String == "1"{
                        
                        self.view2.backgroundColor = UIColor(named: "darkred")
                    }
                    else if dicObject.value(forKey: "is_wrong") as! String == "0"{
                        
                        self.view2.backgroundColor = UIColor(named: "darkgreen")
                    }
                }
            } else{
                self.view2.backgroundColor = UIColor.clear
            }
            
            if self.lbl1.text != ""{
                
                view1.isHidden = false
                
                let index = (Int)(self.lbl1.text!)
                
                let questionIndex = index! - 1
                
                let dicQuestion = self.arrQuestions.object(at: questionIndex) as! NSDictionary
                let questionId = dicQuestion.value(forKey: "id") as! String
                
                if isFromDailyCheckIn == true{
                
                array = DBHandler.getDataFromTable("SELECT * FROM daily_checkin WHERE que_id = '\(questionId)'")
                    
                } else if isFromDailyQuestion == true{
                array = DBHandler.getDataFromTable("SELECT * FROM daily_test WHERE que_id = '\(questionId)'")
                    

                } else if isFromQuickTest == true{
                array = DBHandler.getDataFromTable("SELECT * FROM quick_test WHERE que_id = '\(questionId)'")
                    

                } else if isFromWrongQue == true{
                array = DBHandler.getDataFromTable("SELECT * FROM wrong_test WHERE que_id = '\(questionId)'")
                    

                } else if isFromCustomized == true{
                array = DBHandler.getDataFromTable("SELECT * FROM customized_test WHERE que_id = '\(questionId)'")

                }
                
                if array.count > 0{
                    
                    let dicObject =  array.firstObject as! NSDictionary
                    
                    if dicObject.value(forKey: "is_wrong") as! String == "1"{
                        
                        self.view1.backgroundColor = UIColor(named: "darkred")
                    }
                    else if dicObject.value(forKey: "is_wrong") as! String == "0"{
                        
                        self.view1.backgroundColor = UIColor(named: "darkgreen")
                    }
                }
            } else{
                self.view1.backgroundColor = UIColor.clear
            }
            
        }
    }
    
    func setupViewQuestion()
    {
        let dic = self.arrQuestions.object(at: self.currentQuestionIndex) as! NSDictionary
        let questionid = dic.value(forKey: "id") as! String
        let part_id = dic.value(forKey: "part_id") as! String
        
        let updateQuery : String = "UPDATE question SET read_count = '1' WHERE id = '\(questionid)' AND part_id = '\(part_id)'"
        let success = DBHandler.genericQuery(updateQuery)
        print(success)
        
        let topic_id = dic.value(forKey: "topicId") as! String
        
        let index = self.currentQuestionIndex + 1
        viewCount = self.arrCount - (self.arrQuestions.count - index)
        
        let readRecord = DBHandler.getDataFromTable("SELECT * FROM read_question WHERE topic_id = '\(topic_id)' AND part_id = '\(part_id)'")
                
        if readRecord!.count > 0{
            
            let query = "UPDATE read_question SET viewcount = '\(viewCount)', topic_id = '\(topic_id)', part_id = '\(part_id)' WHERE topic_id = '\(topic_id)' AND part_id = '\(part_id)'"
            let updateSuccess = DBHandler.genericQuery(query)
            print(updateSuccess)
        }
        else{
            
            let insertSuccess = DBHandler.addReadQue(topic_id, totalPages: self.arrQuestions.count, viewCount: viewCount, partid: part_id)
            
            print(insertSuccess)
        }
        
        UserDefaults.standard.set(viewCount, forKey: "viewCount")
        UserDefaults.standard.set(mainTitle, forKey: "partName")
        UserDefaults.standard.set(self.arrCount, forKey: "totalArrQuestion")
        UserDefaults.standard.synchronize()
        
        
    }
    
    @IBAction func btnFavTapped(_ sender: UIButton) {
        
        let dicQuestion = self.arrQuestions.object(at: self.currentQuestionIndex) as! NSDictionary
        let que_id = dicQuestion.value(forKey: "id") as? String
        
        if isButtonSelected == true{
            
            let success = DBHandler.genericQuery("UPDATE question SET is_fav = '1' WHERE id = '\(que_id ?? "")'")
            
            if success
            {
                print("Remove Successfully")
            }
            
            self.btnFav.setImage(UIImage(named: "ic_unfavourite"), for: .normal)
            
            isButtonSelected = false
            
        } else {
            
            let success = DBHandler.genericQuery("UPDATE question SET is_fav = '0' WHERE id = '\(que_id ?? "")'")
            
            if success
            {
                print("Add Successfully")
            }
            
            self.btnFav.setImage(UIImage(named: "ic_favourite"), for: .normal)
            
            isButtonSelected = true
        }
    }
    
    @IBAction func tappedOnBtn2(_ sender: Any) {
        
        isSelected = true
        
        if self.lbl2.text != ""{
            
            self.currentQuestionIndex = self.currentQuestionIndex - 1
            print(self.currentQuestionIndex)
            
            if self.currentQuestionIndex <= -1{
                self.tblView.reloadData()
                return
            } else {
                self.explanationView.isHidden = false
                self.lineBottomView.isHidden = false
                self.setQuestionAndAnswer()
                self.showStatusofQuestion()
                self.tblView.reloadData()
            }
            
        }
    }
    
    @IBAction func tappedOnBtn3(_ sender: Any) {
        
        isSelected = true
        
        if self.lbl3.text != ""{
            self.currentQuestionIndex = self.currentQuestionIndex - 2
            print(self.currentQuestionIndex)
            
            if self.currentQuestionIndex <= -2{
                self.tblView.reloadData()
                return
            } else {
                self.explanationView.isHidden = false
                self.lineBottomView.isHidden = false
                self.setQuestionAndAnswer()
                self.showStatusofQuestion()
                self.tblView.reloadData()
            }
        }
    }
}
extension String {
    func escapeString() -> String {
        
        var newString = self.replacingOccurrences(of: "\"", with: "\"\"")
        
       if  newString.contains(",") ||  newString.contains("\n")
        {
           newString = String(format: "\"%@\"", newString)
        }

        return newString
    }
    
    func setReplaceValue()->String
    {
        let str = self.replacingOccurrences(of: "'", with: "%20")
        return str
    }
    
    func getReplaceValue()->String
    {
        let str = self.replacingOccurrences(of: "%20", with: "'")
        return str
    }
}

