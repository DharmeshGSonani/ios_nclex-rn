//
//  SubmitTestPopUpVc.swift
//  NCLEX-RN
//
//  Created by Kevin M1 on 11/03/22.
//

import UIKit

class SubmitTestPopUpVc: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblInstruction: UILabel!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var btnContinue: UIButton!
    
    //MARK: - Variables
    var dicResult = NSMutableDictionary()
    
    var isFromTopic = false
    var isFromQuickTest = false
    var isFromWrong = false
    var isFromCustomized = false
    var isFromDailyTest = false
    var isFromDailyCheckIn = false
    
    var part_id = ""
    var time = ""
    
    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
    
        let correctAns = (Int)((dicResult.value(forKey: "correctAnswer") as? String) ?? "")
        let totalQuestion = (Int)((dicResult.value(forKey: "totalQuestion") as? String) ?? "")
        
        if correctAns != nil{
            
            let count = correctAns!
            let count1 = totalQuestion!
            
            let attributedString1 = NSMutableAttributedString(string: "You've answered \(count) out of", attributes: [
                NSAttributedString.Key.font:UIFont(name: "Montserrat-Regular", size: 15.0)!,
                .foregroundColor: UIColor.init(red: 52/255, green: 57/255, blue: 101/255, alpha: 0.9)
                //            .kern: -0.29
            ])
            
            let attributedString2 = NSMutableAttributedString(string: " \(count1) question. If you submit, You'll only be scored on the 1 question you answered.", attributes: [
                NSAttributedString.Key.font:UIFont(name: "Montserrat-Regular", size: 15.0)!,
                .foregroundColor: UIColor.init(red: 52/255, green: 57/255, blue: 101/255, alpha: 0.9)
                //.kern: -0.29
            ])
            
            let priceNSMutableAttributedString = NSMutableAttributedString()
            priceNSMutableAttributedString.append(attributedString1)
            priceNSMutableAttributedString.append(attributedString2)
            
            self.lblInstruction.attributedText = priceNSMutableAttributedString
        }
        
    }

    //MARK: - IBAction
    @IBAction func btSubmitTestTapped(_ sender: Any) {
        
        if isFromTopic == true{
            
            let resultVC = self.storyboard?.instantiateViewController(withIdentifier: "ResultVC") as! ResultVC
            resultVC.dicResult = self.dicResult
            resultVC.time = time
            resultVC.part_id = part_id
            resultVC.isFromTopicVC = true
            self.navigationController?.pushViewController(resultVC, animated: true)
            
        } else if isFromCustomized == true{
            let resultVC = self.storyboard?.instantiateViewController(withIdentifier: "ResultTopVC") as! ResultTopVC
            resultVC.dicResult = self.dicResult
            resultVC.isFromCustomized = true
            self.navigationController?.pushViewController(resultVC, animated: true)
            
        } else if isFromQuickTest == true{
            let resultVC = self.storyboard?.instantiateViewController(withIdentifier: "ResultTopVC") as! ResultTopVC
            resultVC.dicResult = self.dicResult
            resultVC.time = time
            resultVC.isFromQuickTest = true
            self.navigationController?.pushViewController(resultVC, animated: true)
            
        }
//        else if isFromWrong == true{
//            let resultVC = self.storyboard?.instantiateViewController(withIdentifier: "ResultTopVC") as! ResultTopVC
//            resultVC.dicResult = self.dicResult
//            resultVC.time = time
//            resultVC.isFromWrong = true
//            self.navigationController?.pushViewController(resultVC, animated: true)
//
//        }
        else if isFromDailyTest == true {
            let resultVC = self.storyboard?.instantiateViewController(withIdentifier: "ResultTopVC") as! ResultTopVC
            resultVC.isFromDaily = true
            resultVC.time = time
            resultVC.dicResult = self.dicResult
            self.navigationController?.pushViewController(resultVC, animated: true)
            
        } else if isFromDailyCheckIn == true {
            let resultVC = self.storyboard?.instantiateViewController(withIdentifier: "ResultTopVC") as! ResultTopVC
            resultVC.isFromDailyCheckin = true
            resultVC.time = time
            resultVC.dicResult = self.dicResult
            self.navigationController?.pushViewController(resultVC, animated: true)
            
        }
        else if isFromWrong == true{
            let resultVC = self.storyboard?.instantiateViewController(withIdentifier: "ResultVC") as! ResultVC
            resultVC.dicResult = self.dicResult
            resultVC.time = time
            resultVC.isFromWrong = true
            self.navigationController?.pushViewController(resultVC, animated: true)
            
        }
        else {
            let resultVC = self.storyboard?.instantiateViewController(withIdentifier: "ResultVC") as! ResultVC
            resultVC.dicResult = self.dicResult
            resultVC.time = time
            self.navigationController?.pushViewController(resultVC, animated: true)
        }
        
    }
    
    @IBAction func btnContinueTest(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
