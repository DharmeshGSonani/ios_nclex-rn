//
//  SubjectWiseVC.swift
//  NCLEX-RN
//
//  Created by Kevin M1 on 09/03/22.
//

import UIKit

class SubjectWiseVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    //MARK: - IBOutlets
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblTopicName: UILabel!
    
    //MARK: - Variables
//    var arrTitle = ["Management of care 1", "Management of care 2" , "Management of care 3", "Management of care 4"]
    var imageData = UIImage()
    
    var arrayPartId : [String] = []
    
    var lblMainTopic = ""
    var mainTitle = ""
    
    var currentQuestionIndex : Int = 0
    var arrQuestions = NSMutableArray()
    var arrAnswer = NSArray()
    var arrOptions = NSMutableArray()
    var dicSelectedCategory = NSDictionary()
    
    var arrTopicExit = NSMutableArray()
    
    var arrPharma = NSMutableArray()
    var arrManagement = NSMutableArray()
    var arrHealth = NSMutableArray()
    var arrIntegrity = NSMutableArray()
    var arrAdoption = NSMutableArray()
    var arrSafety = NSMutableArray()
    var arrBasicAndCare = NSMutableArray()
    var arrReduction = NSMutableArray()
    
    var topic_id = ""
    
    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lblTopicName.text = self.lblMainTopic
        
        tblView.delegate = self
        tblView.dataSource = self
        
        let categoryTest = AppConstant.shared.CategoryTest
        
        if categoryTest == "Pharmacological and Parenteral Therapies"{
            arrQuestions = DBHandler.getDataFromTable("SELECT * FROM question WHERE topicName = 'Pharmacological and Parenteral Therapies'")

        }
        else if categoryTest == "Management of Care"{
            arrQuestions = DBHandler.getDataFromTable("SELECT * FROM question WHERE topicName = 'Management of Care'")
            
        }
        else if categoryTest == "Health Promotion and Maintenance"{
            arrQuestions = DBHandler.getDataFromTable("SELECT * FROM question WHERE topicName = 'Health Promotion and Maintenance'")

        }
        else if categoryTest == "Psychosocial Integrity"{
            arrQuestions = DBHandler.getDataFromTable("SELECT * FROM question WHERE topicName = 'Psychosocial Integrity'")
            
        }
        else if categoryTest == "Physiological Adaptation"{
            arrQuestions = DBHandler.getDataFromTable("SELECT * FROM question WHERE topicName = 'Physiological Adaptation'")

        }
        else if categoryTest == "Safety and Infection Control"{
            arrQuestions = DBHandler.getDataFromTable("SELECT * FROM question WHERE topicName = 'Safety and Infection Control'")

        }
        else if categoryTest == "Basic Care and Comfort"{
            arrQuestions = DBHandler.getDataFromTable("SELECT * FROM question WHERE topicName = 'Basic Care and Comfort'")

        }
        else if categoryTest == "Reduction of Risk Potential"{
            arrQuestions = DBHandler.getDataFromTable("SELECT * FROM question WHERE topicName = 'Reduction of Risk Potential'")
    
        }
        
        var array = NSMutableArray()
        var rowCount : Int = 0
        
        for index in 0..<arrQuestions.count{
            
            let object = arrQuestions.object(at: index) as! NSDictionary
            array.add(object)
            
            if array.count == 20{
                
                self.arrayPartId.append("\(rowCount)")
                
                for index in 0..<array.count{
                    
                    let obj = array.object(at: index) as! NSDictionary
                    let id = obj.value(forKey: "id") as? String
                    
                    let updateQuery : String = "UPDATE question SET part_id = '\(rowCount)' WHERE id = '\(id ?? "")'"
                    let success = DBHandler.genericQuery(updateQuery)
                    print(success)
                }
                
                array = NSMutableArray()
                rowCount = rowCount + 1
                
            } else {
                
            }
        }
    }
    
    //MARK: - TableView Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.arrayPartId.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblView.dequeueReusableCell(withIdentifier: "SubjectWiseCell", for: indexPath) as! SubjectWiseCell
        
        let index = indexPath.row + 1
        cell.lblTitle.text = "\(self.mainTitle) \(index)"
        cell.imgView.image = imageData
                
        let count = (arrQuestions.count / self.arrayPartId.count)
        
        let attributedString1 = NSMutableAttributedString(string: "\(count)", attributes: [
            NSAttributedString.Key.font: UIFont(name: "Montserrat-Regular", size: 10)!,
            .foregroundColor: UIColor(named: "textColorLight") as Any])

        let attributedString2 = NSMutableAttributedString(string: " Exam Questions", attributes: [
            NSAttributedString.Key.font: UIFont(name: "Montserrat-Regular", size: 10)!,
            .foregroundColor: UIColor(named: "textColorLight") as Any])

        let priceNSMutableAttributedString = NSMutableAttributedString()
        priceNSMutableAttributedString.append(attributedString1)
        priceNSMutableAttributedString.append(attributedString2)

        cell.lblExamQuestion.attributedText = priceNSMutableAttributedString
            
        let part_id = arrayPartId[indexPath.row]

        let allReadRecord = DBHandler.getDataFromTable("SELECT * FROM read_question WHERE part_id = '\(part_id)' AND topic_id = '\(topic_id)'")!
        let predicate = NSPredicate.init(format: "part_id = %@ AND topic_id = %@", part_id, topic_id)
        let filteredArray = allReadRecord.filtered(using: predicate)

        if filteredArray.count > 0{
            
            let dicObject = filteredArray.first as! NSDictionary
            let viewCardCount = (Int)(dicObject.value(forKey: "viewcount") as! String)!
            let totalQuestion = (Int)(dicObject.value(forKey: "total_pages") as! String)!
            let percentage = ((CGFloat)(viewCardCount * 100)/(CGFloat)(totalQuestion))
            cell.progressView.progress = Float(percentage/100)
            
            let per  = (CGFloat(percentage))
            let toFloat = (String(format:"%.f", per))
            cell.lblCount.text = "\(toFloat)%"
            
        } else {
            cell.progressView.progress = 0
            cell.lblCount.text = "0%"
        }
        
        let arrQu : NSArray = DBHandler.getDataFromTable("SELECT * FROM question WHERE part_id = \(part_id) AND topicId = '\(topic_id)'")
        
        let arrViewQue : NSArray = DBHandler.getDataFromTable("SELECT * FROM read_question WHERE part_id = \(part_id) AND topic_id = \(topic_id)")
        
        for index in 0..<arrViewQue.count{
            
            let viewCount = (arrViewQue[index] as AnyObject).value(forKey: "viewcount") as? String
            
            if arrQu.count == Int(viewCount!){
                cell.progressView.progressTintColor = UIColor(named: "progressColor")
                cell.imgView1.isHidden = false
                cell.imgView1.image = UIImage(named: "ic_complete1")
                cell.imgDotView.isHidden = true
                cell.lblCount.isHidden = true
                cell.isUserInteractionEnabled = false
            } else {
                if #available(iOS 15.0, *) {
                    cell.progressView.progressTintColor = UIColor.tintColor
                } else {
                    // Fallback on earlier versions
                }
                cell.imgView1.isHidden = true
                cell.lblCount.isHidden = false
                cell.isUserInteractionEnabled = true

            }
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tblView.cellForRow(at: indexPath) as! SubjectWiseCell
        let count = (arrQuestions.count / self.arrayPartId.count)
        
        let topicId = (arrQuestions.value(forKey: "topicId") as? NSArray)?.firstObject as! String

        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let examTestVC = mainStoryboard.instantiateViewController(withIdentifier: "ExamTestVC") as! ExamTestVC
        examTestVC.tabBarController?.tabBar.isHidden = true
        examTestVC.mainTitle = cell.lblTitle.text ?? ""
        examTestVC.partQueCount = count
        examTestVC.topic_id = topicId
        examTestVC.totalPartId = self.arrayPartId[indexPath.row]
        examTestVC.isFromTopicVC = true
        self.navigationController?.pushViewController(examTestVC, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 101
    }
    
    //MARK: - IBAction
    @IBAction func btnBackTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
