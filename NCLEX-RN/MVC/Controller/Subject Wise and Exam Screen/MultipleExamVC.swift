//
//  MultipleExamVC.swift
//  NCLEX-RN
//
//  Created by Kevin M1 on 12/04/22.
//

import UIKit

class MultipleExamVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    //MARK: - IBOutlets
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet weak var lblQuestion: UILabel!
    @IBOutlet weak var lblExplaination: UILabel!
    @IBOutlet weak var lblExplanData: UILabel!
    @IBOutlet weak var imgExplain: UIImageView!
    @IBOutlet weak var explainationView: UIView!
    @IBOutlet weak var btnCheckAnswer: UIButton!
    @IBOutlet weak var btnFav : UIButton!
    @IBOutlet weak var btn1 : UIButton!
    @IBOutlet weak var btn2 : UIButton!
    @IBOutlet weak var btn3 : UIButton!
    @IBOutlet weak var lbl1 : UILabel!
    @IBOutlet weak var lbl2 : UILabel!
    @IBOutlet weak var lbl3 : UILabel!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view3: UIView!
    
    //MARK: - Variables
    var currentQuestionIndex : Int = 0
    var arrQuestions = NSMutableArray()
    var arrAnswer = NSArray()
    var arrOptions = NSMutableArray()
    
    var mainTitle = ""
    
    var isAnswerSelected = false
    var isButtonSelected = false
    
    var dicResult = NSMutableDictionary()
    var totalCorrectAnswer : Int = 0
    var totalInCorrectAnswer : Int = 0
    var totalQuestion : Int = 0
    
    var isSelected = false
    
    var isFromDailyCheckin = false
    var isFromTopicVC = false
    var isFromQuickTest = false
    var isFromWrongQue = false
    var viewCount = 0
    var arrCount = 0
    var totalQueCount = 0
    
    var isSelect = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblView.delegate = self
        tblView.dataSource = self
        
        lblTitle.text = mainTitle
        explainationView.isHidden = true
        
        let categoryTest = AppConstant.shared.CategoryTest
        
        if isFromTopicVC == true
        {
//            arrQuestions = DBHandler.getDataFromTable("SELECT * FROM question WHERE topicName = '\(categoryTest)' ORDER BY random() LIMIT 20")
//            print(arrQuestions.count)
            let arrQu : NSArray = DBHandler.getDataFromTable("SELECT * FROM question WHERE topicName = '\(categoryTest)' ORDER BY random() LIMIT '\(totalQueCount)'")
            arrCount = arrQu.count
            initiateDB(arrQu)
            
            if arrQuestions.count == 0 {

                for i in 0..<arrQu.count {

                    let dic = arrQu.object(at: i) as! NSDictionary
                    let questionid = dic.value(forKey: "id") as! String

                    let updateQuery : String = "UPDATE question SET read_count = '0' WHERE id = \(questionid)"
                    let success = DBHandler.genericQuery(updateQuery)
                    print(success)
                }

                let arrQu : NSArray = DBHandler.getDataFromTable("SELECT * FROM question WHERE topicName = '\(categoryTest)' ORDER BY random() LIMIT '\(totalQueCount)'")
                initiateDB(arrQu)
            }
            
            self.dicResult.setValue("\(self.arrQuestions.count)", forKey: "totalQuestion")
            print(arrQuestions.count)
            
        }
        self.setQuestionAndAnswer()
    }
    
    func setQuestionAndAnswer(){
        
        if self.arrQuestions.count > 0{
                        
            let dicQuestion = arrQuestions.object(at: self.currentQuestionIndex) as! NSDictionary
            
            let currentQNo = self.currentQuestionIndex + 1
            self.lbl1.text = "\(currentQNo)"
            
            if currentQNo >=  3 {
                
                let second = currentQNo - 1
                self.lbl2.text = "\(second)"
                
                let third = currentQNo - 2
                self.lbl3.text = "\(third)"
                
            } else if currentQNo >= 2 {
                
                let second = currentQNo - 1
                self.lbl2.text = "\(second)"
                
            } else {
                
                self.lbl2.text = ""
                self.lbl3.text = ""
                
            }
            
            if isSelected == true{
                
                let currentQNo = self.currentQuestionIndex + 1
                self.lbl1.text = "\(currentQNo)"
                
                if currentQNo >=  3 {
                    
                    let second = currentQNo - 1
                    self.lbl2.text = "\(second)"
                    
                    let third = currentQNo - 2
                    self.lbl3.text = "\(third)"
                    
                } else if currentQNo >= 2 {
                    
                    let second = currentQNo - 1
                    self.lbl2.text = "\(second)"
                    
                    self.lbl3.text = ""
                    
                } else {
                    
                    self.lbl2.text = ""
                    self.lbl3.text = ""
                    
                }
            }
            
            let strQuestionTitle = dicQuestion.value(forKey: "question") as! String
            lblQuestion.text = strQuestionTitle
            
            let strExplanation = dicQuestion.value(forKey: "explanation") as! String
            lblExplanData.text = strExplanation
            
            let strAnswer = dicQuestion.value(forKey: "answers") as! String
            let strFinalAnswer = strAnswer.replacingOccurrences(of: "[\"", with: "").replacingOccurrences(of: "\"]", with: "")
            
            let strOption = dicQuestion.value(forKey: "choices") as! String
            let strFinalOption = strOption.replacingOccurrences(of: "[\"", with: "").replacingOccurrences(of: "\"]", with: "")
            let strFinal = strFinalOption.replacingOccurrences(of: "\"", with: "").replacingOccurrences(of: "\"", with: "")
            
            var arrOption : [String] = []
            
            arrOption = strFinal.components(separatedBy: "$")
            let count1 = arrOption.count
            arrOption.insert(strFinalAnswer, at: count1)
            
            self.arrOptions.removeAllObjects()
            
            for index in 0..<arrOption.count
            {
                let id = arrOption[index]
                self.arrOptions.add(id)
            }
            
            self.arrOptions = (arrOption.shuffled() as NSArray).mutableCopy() as! NSMutableArray
            
            let attributedString1 = NSMutableAttributedString(string: "\(self.currentQuestionIndex+1)/", attributes: [
                NSAttributedString.Key.font: UIFont(name: "Montserrat-Regular", size: 10)!,
                .foregroundColor: UIColor.init(red: 74/255, green: 92/255, blue: 208/255, alpha: 1)])
            
            let count = self.arrQuestions.count
            
            let attributedString2 = NSMutableAttributedString(string: "\(count)", attributes: [
                NSAttributedString.Key.font: UIFont(name: "Montserrat-Regular", size: 10)!,
                .foregroundColor: UIColor.init(red: 74/255, green: 92/255, blue: 208/255, alpha: 0.60)])
            
            let priceNSMutableAttributedString = NSMutableAttributedString()
            priceNSMutableAttributedString.append(attributedString1)
            priceNSMutableAttributedString.append(attributedString2)
            
            self.lblCount.attributedText = priceNSMutableAttributedString
            
            let que_id = dicQuestion.value(forKey: "id") as? String
          
            let favQue = DBHandler.getDataFromTable("SELECT * FROM question WHERE is_fav = '0'")
            
            if favQue != nil{
                
                let array = favQue!
                
                let predicate = NSPredicate.init(format: "SELF = %@", que_id as! CVarArg)
                let filtered = array.filtered(using: predicate) as! NSArray
                
                if filtered.count > 0{
                    self.btnFav.setImage(UIImage(named: "ic_favourite"), for: .normal)
                    self.isButtonSelected = true
                }
                else{
                    self.btnFav.setImage(UIImage(named: "ic_unfavourite"), for: .normal)
                    self.isButtonSelected = false
                }
            }
            
            self.view1.backgroundColor = UIColor(named: "grayBlue")
            self.view2.backgroundColor = UIColor(named: "grayBlue")
            self.view3.backgroundColor = UIColor(named: "grayBlue")
            
            self.tblView.reloadData()
        }
    }

    //MARK: - TableView Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblView.dequeueReusableCell(withIdentifier: "ExamTableViewCell", for: indexPath) as! ExamTableViewCell
        
        cell.lblOption.text = arrOptions[indexPath.row] as? String
        
        cell.bgView.backgroundColor = UIColor(red: 236/255, green: 242/255, blue: 255/255, alpha: 1.0)
        cell.blankView.isHidden = false
        cell.imgView1.isHidden = true
    
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if isAnswerSelected{
            return
        }
        
        isAnswerSelected = true
        
        let cell = tblView.cellForRow(at: indexPath) as! ExamTableViewCell
        let dicQuestion = arrQuestions.object(at: self.currentQuestionIndex) as! NSDictionary
        let strAnswer = dicQuestion.value(forKey: "answers") as! String
        let strFinalAnswer = strAnswer.replacingOccurrences(of: "[\"", with: "").replacingOccurrences(of: "\"]", with: "")
        
        let strSelected = self.arrOptions.object(at: indexPath.row) as! String
        let questionId = dicQuestion.value(forKey: "id") as! String
        
        if strSelected == strFinalAnswer {
            
            let success = DBHandler.genericQuery("UPDATE question SET is_wrong = '0' WHERE id = '\(questionId)'")
            
            if success{
                print("Update Successfully")
            }
            
            self.totalCorrectAnswer = self.totalCorrectAnswer + 1
            
            let qNo1 = Int(self.lbl1.text ?? "")
//            let qNo2 = Int(self.lbl2.text ?? "")
//            let qNo3 = Int(self.lbl3.text ?? "")
            
            let currentQno = self.currentQuestionIndex + 1
            
            if qNo1 == currentQno {
                self.view1.backgroundColor = UIColor(named: "darkgreen")
            }
            
            cell.imgView.image = UIImage(named: "ic_right")
            cell.bgView.backgroundColor = UIColor(named: "green")
            cell.blankView.isHidden = true
            cell.imgView1.isHidden = false
            lblExplaination.textColor = UIColor(named: "darkgreen")
            imgExplain.image = UIImage(named: "ic_right")
            explainationView.isHidden = false
            
            self.dicResult.setValue("\(self.totalCorrectAnswer)", forKey: "correctAnswer")
            
        } else {
            
            let success = DBHandler.genericQuery("UPDATE question SET is_wrong = '1', wrong_option = '\(strSelected)', correct_option = '\(strFinalAnswer)' WHERE id = '\(questionId)'")
            
            if success{
                print("Update Successfully")
            }
            
            self.totalInCorrectAnswer = self.totalInCorrectAnswer + 1
            
            let qNo1 = Int(self.lbl1.text ?? "")
//            let qNo2 = Int(self.lbl2.text ?? "")
//            let qNo3 = Int(self.lbl3.text ?? "")
            
            let currentQno = self.currentQuestionIndex + 1
            
            if qNo1 == currentQno {
                self.view1.backgroundColor = UIColor(named: "darkred")
            }
            
            cell.imgView.image = UIImage(named: "ic_wrong1")
            cell.bgView.backgroundColor = UIColor(named: "red")
            cell.blankView.isHidden = true
            cell.imgView1.isHidden = true
            lblExplaination.textColor = UIColor(named: "darkred")
            imgExplain.image = UIImage(named: "ic_wrong1")
            explainationView.isHidden = false
            
            self.dicResult.setValue("\(self.totalInCorrectAnswer)", forKey: "IncorrectAnswer")
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func initiateDB(_ arrQu: NSArray)
    {
        for i in 0..<arrQu.count {
            if let dicQuestion = arrQu.object(at: i) as? NSDictionary {
                if let read_count : String = dicQuestion.value(forKey: "read_count") as? String {
                    print("read_count==> ", read_count)
                    if read_count == "0" || read_count == ""{
                        arrQuestions.add(dicQuestion)
                    }
                } else {
                    arrQuestions.add(dicQuestion)
                }
            }
        }
    }
    
    //MARK: - IBAction
    @IBAction func btnBackTapped(_ sender: Any) {
        
        if isFromDailyCheckin == true{
            UserDefaults.standard.setValue("false", forKey: "showQuestion")
            UserDefaults.standard.synchronize()
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "QuitTestPopUpVC") as! QuitTestPopUpVC
            let vc = UINavigationController(rootViewController: nextViewController)
            vc.isNavigationBarHidden = true
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle   = .crossDissolve
            self.present(vc, animated: true, completion: nil)
            
        } else {
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "QuitTestPopUpVC") as! QuitTestPopUpVC
            let vc = UINavigationController(rootViewController: nextViewController)
            vc.isNavigationBarHidden = true
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle   = .crossDissolve
            self.present(vc, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func btnNextTapped(_ sender: Any) {
        
        isAnswerSelected = false
        
        self.perform(#selector(self.displayNextQuestion), with: nil, afterDelay: 0.5)
        
    }
    
    @objc func displayNextQuestion(){

        if self.currentQuestionIndex < self.arrQuestions.count - 1 {

            self.currentQuestionIndex = self.currentQuestionIndex + 1
            self.explainationView.isHidden = true
            
            self.setQuestionAndAnswer()
            
            if self.isFromTopicVC == true{
                self.setupViewQuestion()
            } else if isFromDailyCheckin == true{
                self.setupViewQuestion()
            }
            

        } else {
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "SubmitTestPopUpVc") as! SubmitTestPopUpVc
            nextViewController.dicResult = self.dicResult
            let vc = UINavigationController(rootViewController: nextViewController)
            vc.isNavigationBarHidden = true

            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle   = .crossDissolve
            self.present(vc, animated: true, completion: nil)
            
        }
    }
    
    func setupViewQuestion()
    {
        let dic = self.arrQuestions.object(at: self.currentQuestionIndex) as! NSDictionary
        let questionid = dic.value(forKey: "id") as! String

        let updateQuery : String = "UPDATE question SET read_count = '1' WHERE id = '\(questionid)'"
        let success = DBHandler.genericQuery(updateQuery)
        print(success)

        let topic_id = dic.value(forKey: "topicId") as? String
        
        let cat_id = AppConstant.shared.CategoryTest

        viewCount = viewCount + 1

        let readRecord = DBHandler.getDataFromTable("SELECT * FROM read_question WHERE cat_id = '\(questionid)'")

        if readRecord!.count > 0{

            let query = "UPDATE read_question SET viewcount = '\(viewCount)' WHERE que_id = '\(questionid)'"
            let updateSuccess = DBHandler.genericQuery(query)
            print(updateSuccess)
        }
        else{

            let insertSuccess = DBHandler.addReadQue(questionid, totalPages: self.arrQuestions.count, viewCount: viewCount, partid: "")
            print(insertSuccess)
        }

    }
    
    @IBAction func btnFavTapped(_ sender: UIButton) {
    
        let dicQuestion = self.arrQuestions.object(at: self.currentQuestionIndex) as! NSDictionary
        let que_id = dicQuestion.value(forKey: "id") as? String
        
        if isButtonSelected == true{

            let success = DBHandler.genericQuery("UPDATE question SET is_fav = '1' WHERE id = '\(que_id ?? "")'")
            
            if success
            {
                print("Remove Successfully")
            }
            
            self.btnFav.setImage(UIImage(named: "ic_unfavourite"), for: .normal)
            
            isButtonSelected = false
            
        } else {
            
            let success = DBHandler.genericQuery("UPDATE question SET is_fav = '0' WHERE id = '\(que_id ?? "")'")
            
            if success
            {
                print("Add Successfully")
            }
            
            self.btnFav.setImage(UIImage(named: "ic_favourite"), for: .normal)

            isButtonSelected = true
        }
    }

    @IBAction func tappedOnBtn2(_ sender: Any) {
        
        isSelected = true
        self.currentQuestionIndex = self.currentQuestionIndex - 1
        print(self.currentQuestionIndex)
        
        self.setQuestionAndAnswer()
        
        
    }
    
    @IBAction func tappedOnBtn3(_ sender: Any) {
        
        isSelected = true
        self.currentQuestionIndex = self.currentQuestionIndex - 2
        print(self.currentQuestionIndex)
        
        self.setQuestionAndAnswer()
        
    }
}
