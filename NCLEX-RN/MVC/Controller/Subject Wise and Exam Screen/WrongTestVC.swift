//
//  WrongTestVC.swift
//  NCLEX-RN
//
//  Created by Kevin M1 on 29/04/22.
//

import UIKit
import AMTabView

class WrongTestVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var explanationView: UIView!
    @IBOutlet weak var lblQuestion: UITextView!
    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblExplanation: UILabel!
    @IBOutlet weak var lblExplanData: UITextView!
    @IBOutlet weak var imgExplan: UIImageView!
    @IBOutlet weak var btnFav: UIButton!
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var btn3: UIButton!
    @IBOutlet weak var lbl1: UILabel!
    @IBOutlet weak var lbl2: UILabel!
    @IBOutlet weak var lbl3: UILabel!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var lineBottomView: UIView!
    
    //MARK: - Variables
    var currentQuestionIndex : Int = 0
    var arrQuestions = NSMutableArray()
    var arrOptions = NSMutableArray()
    var arrAnswer = NSArray()
    
    var mainTitle = ""
    
    var isAnswerSelected = false
    var isButtonSelected = false
    
    var dicResult = NSMutableDictionary()
    var totalCorrectAnswer : Int = 0
    var totalInCorrectAnswer : Int = 0
    var totalQuestion : Int = 0
    
    var isFromWrongQue = false
    
    var viewCount = 0
    var arrCount = 0
    var totalPartId = ""
    var topic_id = ""
    var partQueCount : Int = 0
    
    var timer = Timer()
    var second = 00
    var istimerrunning = false
    
    var arrOption1 = ["A", "B", "C", "D", "E"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblView.delegate = self
        tblView.dataSource = self
        
        lblTitle.text = mainTitle
        explanationView.isHidden = true
        lineBottomView.isHidden = true
        
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
        
        if isFromWrongQue == true{
            self.view1.isHidden = true
            self.view2.isHidden = true
            self.view3.isHidden = true
            
            if arrQuestions.count != 0{
                self.setQuestionAndAnswer()
                self.dicResult.setValue("\(arrQuestions.count)", forKey: "totalQuestion")
            }
            
        }
        
    }
    
    @objc func updateTimer(){
        second += 1
    }
    
    func timeString(time: TimeInterval) -> String{
        
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        
        return String(format: " %02d:%02d ", minutes, seconds)
    }
    
    func setQuestionAndAnswer(){
        
        if self.arrQuestions.count > 0{
            
            let dicQuestion = self.arrQuestions.object(at: self.currentQuestionIndex) as! NSDictionary
            
            let currentQNo = self.currentQuestionIndex + 1
            self.lbl1.text = "\(currentQNo)"
            
            if currentQNo >=  3 {
                
                let second = currentQNo - 1
                self.lbl2.text = "\(second)"
                
                let third = currentQNo - 2
                self.lbl3.text = "\(third)"
                
            } else if currentQNo >= 2 {
                
                let second = currentQNo - 1
                self.lbl2.text = "\(second)"
                self.lbl3.text = ""
                
            } else {
                
                self.lbl2.text = ""
                self.lbl3.text = ""
                
            }
            
            let strQuestionTitle = dicQuestion.value(forKey: "question") as! String
            lblQuestion.text = strQuestionTitle
            
            let strExplanation = dicQuestion.value(forKey: "explanation") as! String
            lblExplanData.text = strExplanation
            
            let strAnswer = dicQuestion.value(forKey: "answers") as! String
            var strFinalAnswer = strAnswer.replacingOccurrences(of: "[\"", with: "").replacingOccurrences(of: "\"]", with: "")
            strFinalAnswer = strFinalAnswer.replacingOccurrences(of: "\\", with: "")
            let strAnswerFinal = strFinalAnswer.replacingOccurrences(of: "\"", with: "")
            
            arrAnswer = strFinalAnswer.components(separatedBy: ";") as NSArray
            print(arrAnswer)
            
            let strOption = dicQuestion.value(forKey: "choices") as! String
            var strFinalOption = strOption.replacingOccurrences(of: "[\"", with: "").replacingOccurrences(of: "\"]", with: "")
            strFinalOption = strFinalOption.replacingOccurrences(of: "\\", with: "")
            let strOptionFinal = strFinalOption.replacingOccurrences(of: "\"", with: "").replacingOccurrences(of: "\"", with: "")
            
            var arrOption : [String] = []
            
            arrOption = strOptionFinal.components(separatedBy: "$")
            let count1 = arrOption.count
            arrOption.insert(strAnswerFinal, at: count1)
            
            self.arrOptions.removeAllObjects()
            
            for index in 0..<arrOption.count
            {
                let id = arrOption[index]
                self.arrOptions.add(id)
            }
            
            self.arrOptions = (arrOption.shuffled() as NSArray).mutableCopy() as! NSMutableArray
            
            let attributedString1 = NSMutableAttributedString(string: "\(self.currentQuestionIndex+1)/", attributes: [
                NSAttributedString.Key.font: UIFont(name: "Montserrat-Regular", size: 10)!,
                .foregroundColor: UIColor.init(red: 74/255, green: 92/255, blue: 208/255, alpha: 1)])
            
            let count = self.arrQuestions.count
            
            let attributedString2 = NSMutableAttributedString(string: "\(count)", attributes: [
                NSAttributedString.Key.font: UIFont(name: "Montserrat-Regular", size: 10)!,
                .foregroundColor: UIColor.init(red: 74/255, green: 92/255, blue: 208/255, alpha: 0.60)])
            
            let priceNSMutableAttributedString = NSMutableAttributedString()
            priceNSMutableAttributedString.append(attributedString1)
            priceNSMutableAttributedString.append(attributedString2)
            
            self.lblCount.attributedText = priceNSMutableAttributedString
            
//            let que_id = dicQuestion.value(forKey: "que_id") as? String
//
//            let favQue = DBHandler.getDataFromTable("SELECT * FROM question WHERE is_fav = '0'")
//
//            if favQue != nil{
//
//                let array = favQue!
//
//                let predicate = NSPredicate.init(format: "SELF = %@", que_id as! CVarArg)
//                let filtered = array.filtered(using: predicate) as! NSArray
//
//                if filtered.count > 0{
//                    self.btnFav.setImage(UIImage(named: "ic_favourite"), for: .normal)
//                    self.isButtonSelected = true
//                }
//                else{
//                    self.btnFav.setImage(UIImage(named: "ic_unfavourite"), for: .normal)
//                    self.isButtonSelected = false
//                }
//
//            }
            
            self.view1.backgroundColor = UIColor(named: "grayBlue")
            
            self.tblView.reloadData()
        }
    }
    
    //MARK: - TableView Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblView.dequeueReusableCell(withIdentifier: "ExamTableViewCell", for: indexPath) as! ExamTableViewCell
        
        cell.lblOption.text = arrOptions[indexPath.row] as? String
        
        cell.lblOption1.text = arrOption1[indexPath.row]
        
        cell.bgView.backgroundColor = UIColor(red: 236/255, green: 242/255, blue: 255/255, alpha: 1.0)
        //        cell.blankView.isHidden = false
        //        cell.blankView.backgroundColor = UIColor(named: "cellbgview")
        cell.imgView1.isHidden = true
        self.btnNext.layer.opacity = 0.8
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tblView.cellForRow(at: indexPath) as! ExamTableViewCell
        let dicQuestion = arrQuestions.object(at: self.currentQuestionIndex) as! NSDictionary
//        var strQuestion = dicQuestion.value(forKey: "question") as! String
        let questionId = dicQuestion.value(forKey: "id") as! String
        
        let arrQuick = DBHandler.getDataFromTable("SELECT * FROM question WHERE id = '\(questionId)'")
        
        if arrQuick!.count > 0{
            
            let dicObject = arrQuick?.firstObject as! NSDictionary
            
            if dicObject.value(forKey: "is_wrong") as! String != ""{
                
                let strAnswer = dicQuestion.value(forKey: "answers") as! String
                var strFinalAnswer = strAnswer.replacingOccurrences(of: "[\"", with: "").replacingOccurrences(of: "\"]", with: "")
                strFinalAnswer = strFinalAnswer.replacingOccurrences(of: "\\", with: "")
                strFinalAnswer = strFinalAnswer.replacingOccurrences(of: "\"", with: "")
                
                let strOption = dicQuestion.value(forKey: "choices") as! String
                var strFinal = strOption.replacingOccurrences(of: "[\"", with: "").replacingOccurrences(of: "\"]", with: "")
                strFinal = strFinal.replacingOccurrences(of: "\\", with: "")
                strFinal = strFinal.replacingOccurrences(of: "\"", with: "").replacingOccurrences(of: "\"", with: "")
                
                let strSelected = self.arrOptions.object(at: indexPath.row) as! String
                
                if strSelected == strFinalAnswer {
                    
                    if isFromWrongQue == true{
                        
                        let success = DBHandler.genericQuery("UPDATE question SET is_wrong = '0', correct_option = '\(strSelected)' WHERE id = '\(questionId)'")
                        
                        if success{
                            print("Update Successfully")
                        }
                        
//                        let updateSuccess = DBHandler.genericQuery("UPDATE wrong_test SET is_wrong = '0',correct_option = '\(strSelected)' WHERE que_id = '\(questionId)'")
//
//                        if updateSuccess{
//                            print("Update Successfully")
//                        }
                    }
                    
                    self.totalCorrectAnswer = self.totalCorrectAnswer + 1
                    
                    let qNo1 = Int(self.lbl1.text ?? "")
                    
                    let currentQno = self.currentQuestionIndex + 1
                    
                    if qNo1 == currentQno {
                        self.view1.backgroundColor = UIColor(named: "darkgreen")
                    }
                    
                    //                    cell.imgView.image = UIImage(named: "ic_right")
                    cell.bgView.backgroundColor = UIColor(named: "green")
                    //                    cell.blankView.isHidden = false
                    //                    cell.blankView.backgroundColor = UIColor(named: "green")
                    cell.imgView1.isHidden = false
                    cell.imgView1.image = UIImage(named: "ic_mark")
                    lblExplanation.textColor = UIColor(named: "darkgreen")
                    imgExplan.image = UIImage(named: "ic_right")
                    explanationView.isHidden = false
                    lineBottomView.isHidden = false
                    
                    self.dicResult.setValue("\(self.totalCorrectAnswer)", forKey: "correctAnswer")
                    
                    self.btnNext.layer.opacity = 1.0
                    
                } else {
                    
                    if isFromWrongQue == true{
                        
                        let success = DBHandler.genericQuery("UPDATE question SET is_wrong = '1', wrong_option = '\(strSelected)', correct_option = '\(strFinalAnswer)' WHERE id = '\(questionId)'")
                        
                        if success{
                            print("Update Successfully")
                        }
                        
//                        let updateSuccess = DBHandler.genericQuery("UPDATE wrong_test SET is_wrong = '1',correct_option = '\(strSelected)' WHERE que_id = '\(questionId)'")
//
//                        if updateSuccess{
//                            print("Update Successfully")
//                        }
                        
                    }
                    
                    self.totalInCorrectAnswer = self.totalInCorrectAnswer + 1
                    
                    let qNo1 = Int(self.lbl1.text ?? "")
                    
                    let currentQno = self.currentQuestionIndex + 1
                    
                    if qNo1 == currentQno {
                        self.view1.backgroundColor = UIColor(named: "darkred")
                    }
                    
                    //                    cell.imgView.image = UIImage(named: "ic_wrong1")
                    cell.bgView.backgroundColor = UIColor(named: "red")
                    //                    cell.blankView.isHidden = false
                    //                    cell.blankView.backgroundColor = UIColor(named: "red")
                    cell.imgView1.isHidden = false
                    cell.imgView1.image = UIImage(named: "ic_cross")
                    lblExplanation.textColor = UIColor(named: "darkred")
                    imgExplan.image = UIImage(named: "ic_wrong1")
                    explanationView.isHidden = false
                    lineBottomView.isHidden = false
                    
                    for index in 0..<arrOptions.count
                    {
                        let option = arrOptions[index]
                        
                        if option as! String == strFinalAnswer{
                            
                            let cellRightAnswer = tableView.cellForRow(at: IndexPath.init(row: index, section: 0)) as! ExamTableViewCell
                            
                            //                            cellRightAnswer.imgView.image = UIImage(named: "ic_right")
                            cellRightAnswer.bgView.backgroundColor = UIColor(named: "green")
                            //                            cellRightAnswer.blankView.isHidden = false
                            //                            cellRightAnswer.blankView.backgroundColor = UIColor(named: "green")
                            cellRightAnswer.imgView1.isHidden = false
                            cellRightAnswer.imgView1.image = UIImage(named: "ic_mark")
                            
                        }
                        self.dicResult.setValue("\(self.totalInCorrectAnswer)", forKey: "IncorrectAnswer")
                    }
                    self.btnNext.layer.opacity = 1.0
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    //MARK: - IBAction
    @IBAction func btnBackTapped(_ sender: Any) {
        
        //         if isFromWrongQue == true  {
        //            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        //            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "QuitTestPopUpVC") as! QuitTestPopUpVC
        //            let vc = UINavigationController(rootViewController: nextViewController)
        //            vc.isNavigationBarHidden = true
        //            vc.modalPresentationStyle = .overFullScreen
        //            vc.modalTransitionStyle   = .crossDissolve
        //            self.present(vc, animated: true, completion: nil)
        //
        //        } else {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! AMTabsViewController
        let vc = UINavigationController(rootViewController: nextViewController)
        vc.isNavigationBarHidden = true
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle   = .crossDissolve
        self.present(vc, animated: true, completion: nil)
        //        }
    }
    
    @IBAction func btnNextTapped(_ sender: Any) {
        
        isAnswerSelected = false
        
        if isFromWrongQue == true{
            
            let dicQuestion = arrQuestions.object(at: self.currentQuestionIndex) as! NSDictionary
            let questionId = dicQuestion.value(forKey: "id") as! String
            
            let array = DBHandler.getDataFromTable("SELECT * FROM question WHERE id = '\(questionId)'")
            
            if array!.count > 0{
                
                let dicObject = array?.firstObject as! NSDictionary
                
                if dicObject.value(forKey: "is_wrong") as! String != ""{
                    
                    self.perform(#selector(self.displayNextQuestion), with: nil, afterDelay: 0.5)
                } else {
                    self.view.makeToast("Please Select Atleast one Option.")
                }
            }
            
        }
    }
    
    @objc func displayNextQuestion(){
        
        if self.currentQuestionIndex < self.arrQuestions.count - 1 {
            
            //            if self.isFromWrongQue == true{
            //                self.showStatusofQuestion()
            //            }
            
            self.currentQuestionIndex = self.currentQuestionIndex + 1
            self.explanationView.isHidden = true
            self.lineBottomView.isHidden = true
            
            self.setQuestionAndAnswer()
                        
        } else {
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "SubmitTestPopUpVc") as! SubmitTestPopUpVc
            nextViewController.isFromWrong = true
            nextViewController.time = timeString(time: TimeInterval(second))
            nextViewController.dicResult = self.dicResult
            let vc = UINavigationController(rootViewController: nextViewController)
            vc.isNavigationBarHidden = true
            
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle   = .crossDissolve
            self.present(vc, animated: true, completion: nil)
            
        }
    }
    
    @IBAction func btnFavTapped(_ sender: UIButton) {
        
        let dicQuestion = self.arrQuestions.object(at: self.currentQuestionIndex) as! NSDictionary
        let que_id = dicQuestion.value(forKey: "id") as? String
        
        if isButtonSelected == true{
            
            let success = DBHandler.genericQuery("UPDATE question SET is_fav = '1' WHERE id = '\(que_id ?? "")'")
            
            if success
            {
                print("Remove Successfully")
            }
            
            self.btnFav.setImage(UIImage(named: "ic_unfavourite"), for: .normal)
            
            isButtonSelected = false
            
        } else {
            
            let success = DBHandler.genericQuery("UPDATE question SET is_fav = '0' WHERE id = '\(que_id ?? "")'")
            
            if success
            {
                print("Add Successfully")
            }
            
            self.btnFav.setImage(UIImage(named: "ic_favourite"), for: .normal)
            
            isButtonSelected = true
        }
    }

}
