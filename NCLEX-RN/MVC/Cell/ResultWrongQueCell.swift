//
//  ResultWrongQueCell.swift
//  NCLEX-RN
//
//  Created by Kevin M1 on 12/03/22.
//

import UIKit

class ResultWrongQueCell: UITableViewCell {
   
    @IBOutlet weak var lblOption: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
