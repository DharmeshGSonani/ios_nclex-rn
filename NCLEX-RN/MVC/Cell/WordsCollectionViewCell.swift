//
//  WordsCollectionViewCell.swift
//  NCLEX-RN
//
//  Created by Kevin M1 on 08/03/22.
//

import UIKit

class WordsCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var lblSub: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        lblSub.layer.cornerRadius =  lblSub.frame.height/2
    }

//    func getInteresticSize(strText:String,cv:UICollectionView)-> CGSize{
//        let font = (Name:lblSub.font.fontName,Size:lblSub.font.pointSize)
//        let textSize = lblSub.textSize(font: UIFont(name: font.Name, size: font.Size)!, text: strText)
//
//        // 50 - Label Padding
//        if textSize.width + 50 >= cv.frame.size.width{
//            let height = lblSub.heightForView(text: strText, font: UIFont(name: font.Name, size: font.Size+1)!, width: cv.frame.size.width - 15)
//            return CGSize(width: cv.frame.size.width, height: height + 35)
//        }else{
//            return CGSize(width: textSize.width + 40 , height: textSize.height + 35)
//        }
//    }
    
}
