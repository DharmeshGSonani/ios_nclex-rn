//
//  ResultWrongQueHeaderCell.swift
//  NCLEX-RN
//
//  Created by Kevin M1 on 12/03/22.
//

import UIKit

class ResultWrongQueHeaderCell: UITableViewHeaderFooterView {

    @IBOutlet weak var lblQues: UILabel!
}
