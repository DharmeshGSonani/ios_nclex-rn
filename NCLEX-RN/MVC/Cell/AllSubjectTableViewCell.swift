//
//  AllSubjectTableViewCell.swift
//  NCLEX-RN
//
//  Created by Kevin M1 on 07/03/22.
//

import UIKit

class AllSubjectTableViewCell: UITableViewCell {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTotalque: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var imgView1: UIImageView!
    @IBOutlet weak var lblCount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
