//
//  ExamTableViewCell.swift
//  NCLEX-RN
//
//  Created by Kevin M1 on 09/03/22.
//

import UIKit

class ExamTableViewCell: UITableViewCell {

    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblOption: UILabel!
    @IBOutlet weak var blankView: UIView!
    @IBOutlet weak var imgView1: UIImageView!
    @IBOutlet weak var lblOption1: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
