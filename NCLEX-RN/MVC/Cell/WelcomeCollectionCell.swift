//
//  WelcomeCollectionCell.swift
//  NCLEX-RN
//
//  Created by Kevin M1 on 14/03/22.
//

import UIKit

class WelcomeCollectionCell: UICollectionViewCell {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lbl1: UILabel!
    @IBOutlet weak var lbl2: UILabel!
    @IBOutlet weak var imgPageControl: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
