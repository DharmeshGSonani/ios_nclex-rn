//
//  SubjectWiseCell.swift
//  NCLEX-RN
//
//  Created by Kevin M1 on 09/03/22.
//

import UIKit

class SubjectWiseCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet weak var imgView1: UIImageView!
    @IBOutlet weak var lblExamQuestion: UILabel!
    @IBOutlet weak var imgDotView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
